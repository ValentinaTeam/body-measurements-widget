JavaScript widget for editing individual measurements.

Goals:

* consumes and produces Valentina individal measurement files
* asks the user to enter measurements
* configurable in terms of what measurements the widget asks the user to enter
* hooks and interface for developers to customize and integrate the widget in their apps
* minimal dependencies
* can render into an empty `div`
* can be easily wrapped into a React (or other frontend framework) component

* Measurement list from Valentina should be imported into this repo by a script - so that there is a single source of truth on measurements (i.e. the Valentina repository)



A vague, interactive sketch of the kind of measurements app this widget can be used in can be found at: https://grumpi.bitbucket.io/
