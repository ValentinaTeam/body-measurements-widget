var serializer = require('./../../src/serializer');
var Person = require('./person');

module.exports = function () {
    
    // mockup only. This is where we later access the file system to retrieve measurement profiles and measurement sets
    
    var thomasTester = new Person({'personal': {
        'given-name': 'Thomas',
        'family-name': 'Tester',
        'gender': 'male',
        'email': 'Thomas.tester@onlytesting.test',
    },
    'notes': 'He really needs a better jacket.',
    'unit': 'cm',
    'pm_system': '998',
    'body-measurements': {
        'head_circ': "55.5",
        'bust_circ': "95.0",
        'waist_circ': "75.0",
        'hip_circ': "80.0",
    }
    });
    
    window.localStorage.setItem('person-files', JSON.stringify(['Tester-Thomas.vit']));
    window.localStorage.setItem('Tester-Thomas.vit', serializer.stringify(thomasTester, knownMeasurements));
    
    var muellerUndSohn = new Person({'personal': {
        'given-name': '',
        'family-name': 'Mueller und Sohn Measurements',
        'gender': 'unknown',
        'email': '',
    },
    'notes': 'All measurements.',
    'unit': 'cm',
    'pm_system': '998', // here should be mueller und sohn value
    'body-measurements': {
        // example just contains basic measurements
        'bust_circ': "95.0",
        'waist_circ': "75.0",
        'hip_circ': "80.0",
    }
    });
    
    window.localStorage.setItem('measurement-set-files', JSON.stringify(['Mueller-und-Sohn.vit']));
    window.localStorage.setItem('Mueller-und-Sohn.vit', serializer.stringify(muellerUndSohn, knownMeasurements));
    
    return {
        'readFile': function (path) {
            console.log(['readFile', path, window.localStorage.getItem(path)]);
            return window.localStorage.getItem(path);
        },
        'deleteFile': function (path) {
            window.localStorage.removeItem(path);
            window.localStorage.setItem('person-files', JSON.stringify(JSON.parse(window.localStorage.getItem('person-files')).filter(function (item) { return item !== path; })));
            window.localStorage.setItem('measurement-set-files', JSON.stringify(JSON.parse(window.localStorage.getItem('measurement-set-files')).filter(function (item) { return item !== path; })));
        },
        
        
        'writePersonFile': function (path, content) {
            window.localStorage.setItem(path, content);
            window.localStorage.setItem('person-files', JSON.stringify(JSON.parse(window.localStorage.getItem('person-files')).concat([path])));
            console.log(['writeFile', path, window.localStorage.getItem(path), content]);
        },
        'writeMeasurementSetFile': function (path, content) {
            window.localStorage.setItem(path, content);
            window.localStorage.setItem('measurement-set-files', JSON.stringify(JSON.parse(window.localStorage.getItem('measurement-set-files')).concat([path])));
            console.log(['writeFile', path, window.localStorage.getItem(path), content]);
        },
        
        'listPersonFiles': function (prefix) {
            return JSON.parse(window.localStorage.getItem('person-files'));
        },
        'listMeasurementSetFiles': function () {
            return JSON.parse(window.localStorage.getItem('measurement-set-files'));
        }
    };
};