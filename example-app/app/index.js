var v = require('./views');
var serializer = require('./../../src/serializer');
var Person = require('./person');

function App() {
    var self = this;

    this.w = domvm.watch(function(e) {
		if (typeof document !== 'undefined') {
            console.log(["domvm.watch triggered redraw", e]);
            self.view.redraw();
        }
	});
    
    self.translations = {
        'en_US': {
            knownMeasurements: require('./../../translations/measurements_en_US'),
            uiText: require('./../translations/en_US'),
        },
        'de_DE': {
            knownMeasurements: require('./../../translations/measurements_de_DE'),
            uiText: require('./../translations/de_DE'),
        },
        /*
        'ru_RU': {
            knownMeasurements: require('./../../translations/measurements_ru_RU'),
        },
        'nl_NL': {
            knownMeasurements: require('./../../translations/measurements_nl_NL'),
        },
        'fr_FR': {
            knownMeasurements: require('./../../translations/measurements_fr_FR'),
        },
        'ro_RO': {
            knownMeasurements: require('./../../translations/measurements_ro_RO'),
        },
        
        'en_CA': {
            knownMeasurements: require('./../../translations/measurements_en_CA'),
        },
        */
    };
    
    self.currentLanguage = 'en_US';
    
    self.knownMeasurements = function () {
        return self.translations[self.currentLanguage].knownMeasurements;
    }

    self.uiText = function (item, ignoreMissing) {
        function hasTranslation(language) {
            return Object.keys(self.translations).indexOf(language) !== -1 && typeof self.translations[language].uiText !== 'undefined';
        }
        
        function getTranslation(language, item) {
            var uiText = self.translations[language].uiText[item];
            
            if (typeof uiText !== 'undefined') {
                return uiText;
            }
            return null;
        }
        
        var result = "";
        
        if (hasTranslation(self.currentLanguage)) {
            result = getTranslation(self.currentLanguage, item);
            if (!result) {
                result = getTranslation('en_US', item);
                if (!result) {
                    result = "(missing text: "+ item +")";
                } else {
                    if (!ignoreMissing) {
                        result += "*";
                    }
                }
            }
        } else {
            result = getTranslation('en_US', item);
            if (!result) {
                result = "(missing text: "+ item +")";
            } else {
                if (!ignoreMissing) {
                    result += "*";
                }
            }
        }
        return result;
    }
}

var Routes = {
    home: {
        path: '/',
        context: function (router, app, segs) {
            var ctx = {};
            ctx.title = app.uiText('MEASUREMENTS');

            
            ctx.people = domvm.watch().prop(app.filesystem.listPersonFiles().map(function (file) {
                var person = new Person(serializer.parse(app.filesystem.readFile(file)));
                person.file = file;
                return person;
            }));
            
            console.log(ctx.people());
            
            return ctx;
        },
    },
    
    settings: {
        path: '/settings/',
        context: function (router, app, segs) {
            var ctx = {};
            ctx.title = app.uiText('SETTINGS');
            
            return ctx;
        },
    },
    
    languages: {
        path: '/languages/',
        context: function (router, app, segs) {
            var ctx = {};
            ctx.title = app.uiText('SELECT_LANGUAGE');
            
            return ctx;
        },
    },
    
    measurementSets: {
        path: '/measurement-sets/',
        context: function (router, app, segs) {
            var ctx = {};
            ctx.title = app.uiText('MEASUREMENT_SETS');

            ctx.measurementSets = domvm.watch().prop(app.filesystem.listMeasurementSetFiles().map(function (file) {
                var person = new Person(serializer.parse(app.filesystem.readFile(file)));
                person.file = file;
                return person;
            }));
            
            console.log(ctx.measurementSets());
            
            return ctx;
        },
    },
    
    editMeasurementSet: {
        path: '/measurement-set/:file/',
        context: function (router, app, segs) {
            var ctx = {};
            
            var content = app.filesystem.readFile(segs.file);
            
            ctx.title = app.uiText("EDIT_MEASUREMENT_SET");
            if (content) {
                ctx.measurementSet = new Person(serializer.parse(content));
                ctx.measurementSet.file = segs.file;
                console.log(['measurementSet', ctx.measurementSet]);
                ctx.title = ctx.measurementSet.personal['family-name'];
            } else {
                alert("File not found: " + segs.file);
                //router.goto('home', {});
            }
            
            return ctx;
        },
    },
    
    search: {
        path: '/search/',
        context: function (router, app, segs) {
            var ctx = {};
            ctx.title = app.uiText('SEARCH');
            
            return ctx;
        },
    },
    
    createPerson: {
        path: '/create-person/',
        context: function (router, app, segs) {
            var ctx = {};
            ctx.title = app.uiText('NEW_PERSON');
            
            ctx.person = {
                'personal': {
                    'given-name': domvm.watch().prop(''),
                    'family-name': domvm.watch().prop(''),
                    'gender': domvm.watch().prop('unknown'),
                    'birth-date': domvm.watch().prop('1800-01-01'),
                    'email': domvm.watch().prop(''),
                }
            };
                            
            return ctx;
        },
    },
    
    person: {
        path: '/person/:file/',
        context: function (router, app, segs) {
            var ctx = {};
            
            var content = app.filesystem.readFile(segs.file);
            
            ctx.title = app.uiText("PERSON");
            if (content) {
                ctx.person = new Person(serializer.parse(content));
                ctx.person.file = segs.file;
                console.log(['person', ctx.person]);
                ctx.title = ctx.person.personal['family-name'] + ', ' + ctx.person.personal['given-name'];
            } else {
                alert("Person not found: " + segs.file);
                //router.goto('home', {});
            }
            
            return ctx;
        },
    },
    
    addMeasurements: {
        path: '/person/:file/add-measurements/',
        context: function (router, app, segs) {
            var ctx = {};
            ctx.title = "Add Measurement";
            
            ctx.person = {
                'file': segs.file
            };
                        
            return ctx;
        },
    },

}

function makeOnenter(router, app, context, route) {
    function wrappedOnenter(segs) {
        console.log(['onenter runs for', route]);
        app.context = context(router, app, segs);
        document.title = app.context.title;
        app.view && app.view.redraw();
    }
    return wrappedOnenter;
}

function makeDomvmRoutes(router, app, routes) {
    var domvmRoutes = {};
    for (var route in routes) {
        if (routes.hasOwnProperty(route)) {
            domvmRoutes[route] = {};
            domvmRoutes[route].path = routes[route].path;
            domvmRoutes[route].onenter = makeOnenter(router, app, routes[route].context, route);
            domvmRoutes[route].onexit = routes[route].onexit;
        }
    }
    
    return domvmRoutes;
}

function Router(router, app) {
    return makeDomvmRoutes(router, app, Routes);
}

module.exports = {
    App: App,
    Router: Router,
    View: v.View,
};
