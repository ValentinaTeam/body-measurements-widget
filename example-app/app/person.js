function checkValue(variable, key, defaultValue) {
    if (typeof variable[key] === 'undefined') {
        variable[key] = defaultValue;
    }
}

function Person (data) {
    var keys=Object.keys(data);
    var l=keys.length;
    for (var i=0;i<l;i++) {
        this[keys[i]] = data[keys[i]];
    }
    
    checkValue(this, 'personal', {});
    checkValue(this.personal, 'family-name', '');
    checkValue(this.personal, 'given-name', '');
    checkValue(this.personal, 'gender', 'unknown');
    checkValue(this.personal, 'birth-date', '1800-01-01');
    checkValue(this.personal, 'email', '');
    checkValue(this, 'notes', '');
    
    checkValue(this, 'pm_system', '998');
    checkValue(this, 'unit', 'cm');
    checkValue(this, 'body-measurements', {});
    
    this.hasMeasurements = function () {
        return Object.keys(this['body-measurements']).length;
    }
}

module.exports = Person;