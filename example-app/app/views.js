var w = require('./widgets');   

function View(vm, deps) {
    return function() {
        var route = deps.router.location();
        var href_ = deps.router.href;
        var uiText = deps.app.uiText;
        var result;
        
        var ctx = deps.app.context;
        if (!ctx) return ['div', 'Loading...'];
        
        switch (route.name) {
            case 'home':
                result = ['div', 
                    w.renderNavBar(ctx.title, [['a', {href: href_('settings', {})}, uiText('SETTINGS')]], [['a', {href: href_('search', {})}, uiText('SEARCH')]]),
                    ['div.container',
                        ['div.card-panel.teal.white-text', "Note: This is not functional yet. Just a very rough sketch."],
                        [w.People, {router: deps.router, people: ctx.people(), app: deps.app}]
                    ],
                    ];
                break;
                
            case 'settings':
                result = ['div',
                    w.renderNavBar(ctx.title, [['a', {href: href_('home', {})}, "Home"]]),
                    ['div.container',
                    ['div.collection',
                        ['a.collection-item', {href: href_('languages', {})}, uiText('CURRENT_LANGUAGE').replace("%1", uiText(deps.app.currentLanguage, true))],
                        ['a.collection-item', {href: href_('measurementSets', {})}, uiText('MEASUREMENT_SETS')],
                    ]
                    ]
                    ];
                break;

            case 'languages':
                function selectLanguage (lang) {
                    return function () {
                        deps.app.currentLanguage = lang;
                        deps.router.goto('settings');
                        return false;
                    }
                }
                
                console.log(Object.keys(deps.app.translations));
            
                result = ['div',
                    w.renderNavBar(ctx.title, [['a', {href: href_('settings', {})}, uiText("BACK")]]),
                    ['div.container',
                    ['div.collection',
                        Object.keys(deps.app.translations).map(function (lang) {
                            return ['a.collection-item', {onclick:selectLanguage(lang)}, uiText(lang, true)]; 
                        }),
                    ]
                    ]
                ];
                break;

                
            case 'measurementSets':
                result = ['div',
                    w.renderNavBar(ctx.title, [['a', {href: href_('settings', {})}, uiText('BACK')]]),
                    
                    ['div.container',
                    ['div.collection',
                    ctx.measurementSets().map(function (set) {
                        return ['a.collection-item', {href: href_('editMeasurementSet', {file: set.file})}, set.file]
                    }),
                    ],
                    ],
                ];
                break;
                
            case 'editMeasurementSet':
            
                function toggleMeasurement(m_name) {
                    return function () {
                        console.log(["toggleMeasurement", m_name]);
                    }
                }
                
                function renderMeasurement(m_name) {
                    m = deps.app.knownMeasurements()[m_name];
                            return ['div.col.s6', {
                                    onclick: toggleMeasurement(m_name),
                                    style: (Object.keys(ctx.measurementSet['body-measurements']).indexOf(m_name) !== -1 ? 'background:yellow; border: 1px solid green' : null),
                                },
                                ['div.card-image',
                                    ['img', {src: '/images/'+m_name+'.svg', style: 'max-width:100%;display:block;max-height:300px;max-height:30vh'}],
                                ],
                                ['div',
                                    ['b', m.number], '. ', m.full_name, ' (', ['small', m.description], ')'
                                ],
                            ];
                }
            
                result = ['div',
                    w.renderNavBar(ctx.title, [['a', {href: href_('measurementSets', {})}, uiText('BACK')]]),
                    
                    ['div.container',
                    ['p', ctx.measurementSet.notes],
                    
                    ['div.row',
                        Object.keys(deps.app.knownMeasurements()).map(renderMeasurement),
                    ],
                    ],
                ];
                break;
                
            case 'search':
                result = ['div',
                    w.renderNavBar(ctx.title, [['a', {href: href_('home', {})}, uiText('BACK')]]),
                    ['div.container',
                        ['input', {type: 'search'}]
                    ]
                    ];
                break;
                
            case 'createPerson':
                result = ['div',
                    w.renderNavBar(ctx.title, [['a', {href: href_('home', {})}, uiText('BACK')]]),
                    ['div.container',
                        [w.PersonForm, {router: deps.router, person: ctx.person, filesystem: deps.app.filesystem, app: deps.app}]
                    ]
                    ];
                break;
                
            case 'person':
                result = ['div',
                    w.renderNavBar(ctx.title, [['a', {href: href_('home', {})}, uiText('HOME')]]),
                    ['div.container',
                        [w.Person, {app: deps.app, person: ctx.person}],
                        ['a.btn', {href: href_('person', {file: ctx.person.file})}, uiText('SHARE')],
                        ['a.btn', {href: href_('addMeasurements', {file: ctx.person.file})}, uiText('UPDATE_MEASUREMENTS')],
                    ]
                    ];
                break;                
                
            case 'addMeasurements':
                result = ['div',
                    w.renderNavBar(ctx.title, [['a', {href: href_('person', {file: ctx.person.file})}, uiText('BACK')]]),
                    ['div.container',
                        ['div', {style:'height:300px;background: gainsboro'}, "Illustration"],
                        ['h4', 'Waist'],
                        ['input'],
                        ['a.btn', {href: href_('person', {})}, uiText('SAVE')],
                        ['a.btn', {href: href_('addMeasurements', {file: ctx.person.file})}, uiText('NEXT')],
                    ],
                    ];
                break;                
            default:
                result = ["span", "There be dragons here. Let's ", ["a", {href: href_("home", {})}, 
                    "go home"], "."];
                break;
        }
        
        return ['div#domvm',
            result];
    };
}

module.exports = {
    View: View,
};
