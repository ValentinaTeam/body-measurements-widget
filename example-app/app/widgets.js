var t = require('./templates');
var serializer = require('./../../src/serializer');
var Person = require('./person');

function translateGender(uiText, value) {
    switch (value) {
        case 'male':
            return uiText('MALE');
            break;
        case 'female':
            return uiText('FEMALE');
            break;
        case 'unknown':
        default:
            return uiText('UNKNOWN_GENDER');
            break;
    }
}

module.exports = {
    BoundInput: function (vm, deps) {
        function refresh(property) {
            return function (e, node) {
                property(node.el.value);
            }
        }
        
        return function () {
            deps.attrs.value = deps.property;
            deps.attrs.oninput = refresh(deps.property);
            return ["input", deps.attrs];
        }
    },
    
    renderNavBar: function (title, links, links_right) {
        return ['nav',
            ['div.nav-wrapper',
                ['span.brand-logo.center', title],
                links ? ['ul.left'].concat(links.map(function (l) {return ['li', l];})) : null,
                links_right ? ['ul.right'].concat(links_right.map(function (l) {return ['li', l];})) : null,
            ]
        ];
    },
    
    BoundSelect: function (vm, deps) {
        function refresh(property) {
            return function (e, node) {
                property(node.el.value);
            }
        }
        
        function renderOption(opt) {
            return ['option', deps.attrs.value == opt.value ? {'selected': true} : null, opt.display];
        }
        
        return function () {
            deps.attrs.value = deps.property;
            deps.attrs.oninput = refresh(deps.property);
            return ["select", deps.attrs, deps.options.map(renderOption)];
        }
    },
    
    PersonForm: function (vm, deps) {
        function save() {
            var person = {
                'personal': {},
                'body-measurements': {}
            };
            
            for (var key in deps.person.personal) {
                person.personal[key] = deps.person.personal[key]();
            }
            
            person = new Person(person);
            
            console.log(person);
            var filename = person.personal['family-name'] + '-' + person.personal['given-name'] + '-' + Date.now() + '.vit';
            
            deps.filesystem.writePersonFile(filename, serializer.stringify(person));
            
            deps.router.goto('person', {file: filename});
        }
        
        var GenderOptions = [{value: 'unknown', display: deps.app.uiText('UNKNOWN_GENDER')}, {value: 'male', display: deps.app.uiText('MALE')}, {value: 'female', display: deps.app.uiText('FEMALE')}];
        
        return function () {
            return ['div',
                ['div',
                    ['label', deps.app.uiText('FIRST_NAME'),
                    [module.exports.BoundInput, {property: deps.person.personal['given-name'], attrs: {}}]],
                ],
                ['div',
                    ['label', deps.app.uiText('LAST_NAME'),
                    [module.exports.BoundInput, {property: deps.person.personal['family-name'], attrs: {}}]],
                ],                
                ['div',
                    ['label', deps.app.uiText('GENDER'),
                    [module.exports.BoundSelect, {property: deps.person.personal['gender'], attrs: {class: 'browser-default'}, options: GenderOptions}]],
                ],
                ['div',
                    ['label', deps.app.uiText('BIRTH_DATE'),
                    [module.exports.BoundInput, {property: deps.person.personal['birth-date'], attrs: {type:'date'}}]],
                ],                
                ['div',
                    ['label', deps.app.uiText('EMAIL'),
                    [module.exports.BoundInput, {property: deps.person.personal['email'], attrs: {}}]],
                ],                
                
                ['button.btn', {onclick: save}, deps.app.uiText('SAVE')],
            ];
        }
    },
    
    Person: function (vm, deps) {
        var expandedMeasurement = null;
        
        function expandMeasurement(measurement) {
            return function () {
                if (expandedMeasurement === measurement) {
                    expandedMeasurement = null;
                    vm.redraw();
                } else {
                    expandedMeasurement = measurement;
                    vm.redraw();
                }
            }
        }
        
        function renderMeasurements(person) {
                var measurements = [];
                for (var measurement in person['body-measurements']) {
                    measurements.push(['tr', {onclick: expandMeasurement(measurement), title: deps.app.knownMeasurements()[measurement].description}, ['th', deps.app.knownMeasurements()[measurement].number, '. ', deps.app.knownMeasurements()[measurement].full_name], ['td', person['body-measurements'][measurement], ' ', person['unit']]]);
                    if (expandedMeasurement === measurement) {
                        measurements.push(['tr', ['td', {colspan: 2}, ['small', deps.app.knownMeasurements()[measurement].description],
                        ['img', {src: '/images/'+measurement+'.svg', style: 'max-width:100%;display:block;max-height:300px;max-height:30vh'}],]]);
                    }
                }
                
                return ['table', ['tbody'].concat(measurements)];
        }
        
        var textFile = null,
        makeTextFile = function (text) {
            var data = new Blob([text], {type: 'text/xml'});

            // If we are replacing a previously generated file we need to
            // manually revoke the object URL to avoid memory leaks.
            if (textFile !== null) {
                window.URL.revokeObjectURL(textFile);
            }

            textFile = window.URL.createObjectURL(data);

            // returns a URL you can use as a href
            return textFile;
        };
            
        var saveURL = makeTextFile(serializer.stringify(deps.person, deps.app.knownMeasurements()));
        
        return function () {
            return ['div',
                ['div.circle', {style:'width:100px;height:100px; margin: 20px auto;background: gainsboro'}],
                ['table.responsive-table', 
                ['thead',
                    ['tr', ['th', deps.app.uiText('FIRST_NAME')], ['th', deps.app.uiText('LAST_NAME')], ['th', deps.app.uiText('GENDER')], ['th', deps.app.uiText('BIRTH_DATE')], ['th', deps.app.uiText('EMAIL')]]
                ],
                ['tbody',
                    ['tr', 
                        ['td', deps.person['personal']['given-name']], 
                        ['td', deps.person['personal']['family-name']], 
                        ['td', translateGender(deps.app.uiText, deps.person['personal']['gender'])],
                        ['td', deps.person['personal']['birth-date']],
                        ['td', deps.person['personal']['email']],
                    ],
                ]],
                
                ['p', deps.person.notes],
                
                ['h4', deps.app.uiText('MEASUREMENTS')],
                // TODO: insert date here
                //['p', '20.04.2016'],
                
                deps.person.hasMeasurements() ? 
                    renderMeasurements(deps.person)
                    : 'No measurements recorded yet.'
                ,
                
                ['a', {href: saveURL, download: deps.person.file}, "Save to .vit file"],
            ];
        };
    },
    
    People: function (vm, deps) {
        function renderPerson (person) {
            // TODO: select person by file
            return ['a.collection-item', {href: deps.router.href('person', {file: person.file})},
                ['img.circle', {src: ''}],
                ['b', person['personal']['given-name'], ' ', person['personal']['family-name']],
                ['br'],
                translateGender(deps.app.uiText, person['personal']['gender'])
            ];
        }
        
        return function () {
            return ['div', 
                ['ul.collection'].concat(deps.people.map(renderPerson)),
                ['a.waves-effect.waves-light.btn', {href: deps.router.href('createPerson', {})}, '+ ', deps.app.uiText('ADD_NEW')],
            ];
        }
    },
};