require('domvm/dist/polyfills.min');

var filesystem = require('./app/filesystem');
var domvm = require('domvm');
var example_app = require('./app');

var app = null; 

document.addEventListener("DOMContentLoaded", function() {
    app = new example_app.App();
    knownMeasurements = app.knownMeasurements();
    
    app.filesystem = filesystem();
    
    var router = domvm.route(example_app.Router, app);
    app.view = domvm.view(example_app.View, {app: app, router: router});
    
    app.view.mount(document.body); 
    router.refresh();
    app.view.redraw();
});
