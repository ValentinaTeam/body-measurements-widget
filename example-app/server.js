var express = require('express');
var app = express();
var browserify = require('browserify-middleware');

app.get('/js/client.js', browserify('./client.js'));
app.use(express.static('static'));
app.use('/images', express.static('../images/'));

app.get('/favicon.ico', function (req, res) {
    res.status(404).send("Not found");
});
    
app.get('/', function (req, res) {

    var result = '<!doctype html><html>'
    + '<head>'
    + '<title>Example Measurements App</title>'
    + '<link href="/css/materialize.min.css" rel="stylesheet">'
    + '</head>'
    + '<body>'
    + '<script src="/js/client.js"></script>'
    + '</body>'
    + '</html>';
    res.send(result);
});

app.listen(8000, function () {
    console.log('Example app listening on port 8000!');
});


