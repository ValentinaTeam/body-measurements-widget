module.exports = {
    SETTINGS: "Einstellungen",
    MEASUREMENTS: "Maße",
    NEW_PERSON: "Neue Person",
    PERSON: "Person",

    SEARCH: "Suchen",
    HOME: "Home",
    BACK: "Zurück",

    CURRENT_LANGUAGE: "Sprache: %1",
    SELECT_LANGUAGE: "Sprache auswählen",
    MEASUREMENT_SETS: "Maßgruppen",

    FIRST_NAME: "Vorname",
    LAST_NAME: "Nachname",
    GENDER: "Geschlecht",
    MALE: "männlich",
    FEMALE: "weiblich",
    UNKNOWN_GENDER: "-",
    BIRTH_DATE: "Geburtsdatum",
    EMAIL: "e-Mail",

    ADD_NEW: "Hinzufügen",
    SHARE: "Teilen",
    UPDATE_MEASUREMENTS: "Maße aktualisieren",
    SAVE: "Speichern",
};
