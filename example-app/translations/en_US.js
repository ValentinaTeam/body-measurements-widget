module.exports = {
    MEASUREMENTS: "Measurements",
    SETTINGS: "Settings",
    NEW_PERSON: "New Person",
    PERSON: "Person",
    
    SEARCH: "Search",
    HOME: "Home",
    BACK: "Back",

    CURRENT_LANGUAGE: "Language: %1",
    SELECT_LANGUAGE: "Select Language",
    
    // Language names are ONLY defined in this file. We are NOT in the business of translating language names, that's why we just list all the language names in native language, respectively.
    en_US: 'English (United States)',
    en_CA: 'English (Canada)',
    de_DE: 'Deutsch',
    ru_RU: 'русский',
    nl_NL: 'Nederlands',
    fr_FR: 'Français',
    ro_RO: 'лимба ромынэ',
    
    MEASUREMENT_SETS: "Measurement Sets",

    FIRST_NAME: "First name",
    LAST_NAME: "Last name",
    GENDER: "Gender",
    MALE: "male",
    FEMALE: "female",
    UNKNOWN_GENDER: "-",
    BIRTH_DATE: "Birth date",
    EMAIL: "email",

    ADD_NEW: "Add New",
    SHARE: "Share",
    UPDATE_MEASUREMENTS: "Update Measurements",
    SAVE: "Save",
};
