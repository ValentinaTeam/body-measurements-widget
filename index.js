var serializer = require('./src/serializer');


var s = serializer.stringify({'personal': {'family-name': 'Test', 'given-name': 'Given', 'sex': 'male'}, 'body-measurements': {'ankle_height': 23, 'unit': 'cm'}});
console.log(s);
