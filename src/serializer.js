function node(name, attrs, parentEl) {
    var el = document.createElement(name);
    if (parentEl) {
        parentEl.appendChild(el);
    }
    if (attrs) {
        for (var attr in attrs) {
            el.setAttribute(attr, attrs[attr]);
        }
    }
    return el;
}

function textNode(name, text, parentEl) {
    var t = document.createElement(name);
    parentEl.appendChild(t);
    t.appendChild(document.createTextNode(text));
    return t;
}

module.exports = {
    parse: function (str) {
        
        var parser = new DOMParser();
        var dom = parser.parseFromString(str, "text/xml");
        
        console.log(['dom', dom]);
        
        if (dom.documentElement.nodeName == "parsererror") {
            throw (new Error("Error while parsing .vit file!"));
            return {};
        }
        
        var person = {
            'personal': {},
            'body-measurements': {}
        };
        
        var vit = dom.documentElement; //.childNodes[0];
        console.log([vit.nodeName, vit]);
        
        var l = vit.childNodes.length;
        for (var i=0; i<l; i++) {
            var node = vit.childNodes[i];
            console.log([node.nodeName, node]);
            
            if (node.nodeName == 'personal') {
                var l2 = node.childNodes.length;
                for (var j=0; j<l2; j++) {
                    var node2 = node.childNodes[j];
                    console.log([node2.nodeName, node2]);
                    person.personal[node2.nodeName] = node2.innerHTML;
                }
            }
           
            if (node.nodeName == 'body-measurements') {
                var l2 = node.childNodes.length;
                for (var j=0; j<l2; j++) {
                    var node2 = node.childNodes[j];
                    console.log([node2.nodeName, node2]);
                    person['body-measurements'][node2.getAttribute('name')] = node2.getAttribute('value');
                }
            }
            
            if (['unit', 'pm_system', 'notes', 'read-only'].indexOf(node.nodeName) !== -1) {
                person[node.nodeName] = node.innerHTML;
            }
        }
        
        console.log(['parse', str, person]);
        return person;
    },

    stringify: function (person, knownMeasurements) {
        var vit = node('vit', null);

        var version = textNode('version', '0.3.3', vit);
        
        textNode('read-only', 'false', vit);
        textNode('notes', person['notes'], vit);
        
        var unit = textNode('unit', person.unit, vit);
        var pm_system = textNode('pm_system', person.pm_system, vit);

        var personal = node('personal', null, vit);
        ['family-name','given-name','birth-date','gender','email'].forEach(function (key) {
            textNode(key, person.personal[key], personal);
        });
        
        var measurements = node('body-measurements', null, vit);
        Object.keys(person['body-measurements']).forEach(function (key) {
            console.log([key, knownMeasurements[key]]);
            node('m', {
                    name: key,
                    value: person['body-measurements'][key],
                    full_name: knownMeasurements[key].full_name,
                    description: knownMeasurements[key].description,
            }, measurements);
        });

        var data = "<?xml version='1.0' encoding='UTF-8'?>\n" + (new XMLSerializer()).serializeToString(vit);
        
        
        // this is a horrible hack to get rid of the xmlns attribute on the vit tag
        data = data.replace(/\ xmlns\=\"[\w\/\:\.]+\"/g,'');
        
        console.log(['stringify', person]);
        console.log(data);
        
        return data;
    },
};
