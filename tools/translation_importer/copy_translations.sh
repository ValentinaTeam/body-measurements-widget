for f in *.json; do 
	echo "Processing $f file.."; 
	{ echo -n 'module.exports = '; cat $f; } > ${f%.*}.js
done
cp *.js ../../translations
