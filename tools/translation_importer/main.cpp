#include "def.h"
#include "vtranslatevars.h"

#include <iostream>

#include <QCoreApplication>
#include <QTranslator>
#include <QFile>
#include <QFileInfo>

#include <QVariantMap>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>


VTranslateVars *trMs = nullptr;
QTranslator *translator = nullptr;

QString TranslationsPath()
{
    return QStringLiteral("/home/vagrant/valentina/share/translations");
}

void initTranslations() {
    if (trMs != nullptr)
    {
        delete trMs;
        trMs = new VTranslateVars();
    }
    else
    {
        trMs = new VTranslateVars();
    }
}

int loadMeasurements(const QString &checkedSystem, const QString &checkedLocale)
{
    const QString path = TranslationsPath();
    const QString file = QString("measurements_%1_%2.qm").arg(checkedSystem).arg(checkedLocale);

    if (QFileInfo(path+QLatin1Literal("/")+file).size() <= 34)
    {
        QString message = QString("Translation for system = %1 and locale = %2 is empty. \nFull path: %3/%4")
                .arg(checkedSystem)
                .arg(checkedLocale)
                .arg(path)
                .arg(file);
        std::cout << message.toStdString() << '\n';
        return -1;
    }
    
    if (translator != nullptr) 
    {
        QCoreApplication::removeTranslator(translator);
        delete translator;
    }
    translator = new QTranslator();

    if (not translator->load(file, path)) {
        QString message = QString("Can't load translation for system = %1 and locale = %2. \nFull path: %3/%4")
                .arg(checkedSystem)
                .arg(checkedLocale)
                .arg(path)
                .arg(file);
        std::cout << message.toStdString();
 
        delete translator;

        return -1;
    }

    if (not QCoreApplication::installTranslator(translator))
    {
        QString message = QString("Can't install translation for system = %1 and locale = %2. \nFull path: %3/%4")
                .arg(checkedSystem)
                .arg(checkedLocale)
                .arg(path)
                .arg(file);
        std::cout << message.toStdString();

        delete translator;

        return -1;
    }

    return 0;
}


int loadTranslation(const QString &checkedSystem, const QString &checkedLocale)
{
    int state = loadMeasurements(checkedSystem, checkedLocale);
    if (state != 0)
    {
        return state;
    }

    initTranslations();//Very important do this after loading QM files.

    return 0;
}



int main (int argc, char **argv) {
    
    QCoreApplication a(argc, argv);
    
   
    
    const QStringList locales = SupportedLocales();
    for(int l = 0, sz = locales.size(); l < sz; ++l)
    {
    // TODO: iterate over pattern systems?
        const QString locale = locales.at(l);
        int status = loadTranslation("p0", locale);
    
        if (status != 0) continue;
        /*
        const QStringList listSystems = ListPMSystems();
        QMap<QString, QString> systems;
        for (int i = 0; i < listSystems.size()-1; ++i)
        {
            std::cout << trMs->PMSystemName(listSystems.at(i)).toStdString() << " (" << listSystems.at(i).toStdString() << ")" << listSystems.at(i).toStdString() << std::endl;
        }
        */
    
    
        QFile saveFile(QString("measurements_%1.json").arg(locale));
        
        if (!saveFile.open(QIODevice::WriteOnly)) 
        {
            std::cout << "Couldn't open save file.\n";
            return false;
        }
        
        // generate Measurements JSON Object for current Language
        const QStringList originalNames = AllGroupNames();
        
        QVariantMap map;
        
        foreach(const QString &originalName, originalNames) 
        {
            QVariantMap measurement;
            
            const QString number = trMs->MNumber(originalName);
            const QString full_name = trMs->GuiText(originalName);
            const QString description = trMs->Description(originalName);
            std::cout << number.toStdString() << std::endl;
            const QString diagram = MapDiagrams(trMs, number);
            
            measurement.insert("number", number);
            measurement.insert("full_name", full_name);
            measurement.insert("description", description);
            measurement.insert("diagram", diagram);
            //std::cout << originalName.toStdString() << std::endl;
            
            map.insert(originalName, measurement);
        }
      
        QJsonObject json = QJsonObject::fromVariantMap(map);
        QJsonDocument doc(json);
        
        saveFile.write(doc.toJson());
    }
    
    return 0;
}
