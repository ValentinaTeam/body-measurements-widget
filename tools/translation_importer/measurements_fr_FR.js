module.exports = {
    "across_back_b": {
        "description": "Largeur d'un coté à l'autre des emmanchures, à l'endroit le plus étroit, dos",
        "diagram": "Ip3",
        "full_name": "Carrure dos",
        "number": "I08"
    },
    "across_back_center_to_armfold_front_to_across_back_center": {
        "description": "From center of Across Back, over Shoulder, under Arm, and return to start.",
        "diagram": "Pp7",
        "full_name": "Across Back Center, circled around Shoulder",
        "number": "P07"
    },
    "across_back_half_b": {
        "description": "Moitié de la carrure dos ('carrure dos' / 2).",
        "diagram": "Ip4",
        "full_name": "Demie-carrure dos",
        "number": "I11"
    },
    "across_back_to_waist_b": {
        "description": "Distance du milieu de la carrure dos à la ligne de taille dos en vertical",
        "diagram": "Hp13",
        "full_name": "Carrure dos à la taille dos",
        "number": "H42"
    },
    "across_chest_f": {
        "description": "Largeur d'un coté à l'autre des emmanchure, à lendroit le plus étroit.",
        "diagram": "Ip1",
        "full_name": "Carrure devant",
        "number": "I03"
    },
    "across_chest_half_f": {
        "description": "Moitié de la ligne de carrure, devant. ('carrure devant' / 2).",
        "diagram": "Ip2",
        "full_name": "Demie carrure, devant",
        "number": "I06"
    },
    "arm_above_elbow_circ": {
        "description": "Arm circumference at Bicep level.",
        "diagram": "Lp4",
        "full_name": "Arm: Above Elbow circumference",
        "number": "L12"
    },
    "arm_across_back_center_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Middle of Back to Elbow Tip.",
        "diagram": "Op10",
        "full_name": "Arm: Across Back Center to Elbow, high bend",
        "number": "O12"
    },
    "arm_across_back_center_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Middle of Back to Elbow Tip to Wrist bone.",
        "diagram": "Op10",
        "full_name": "Arm: Across Back Center to Wrist, high bend",
        "number": "O13"
    },
    "arm_armpit_to_elbow": {
        "description": "From Armpit to inner Elbow, arm straight.",
        "diagram": "Lp3",
        "full_name": "Arm: Armpit to Elbow, inside",
        "number": "L09"
    },
    "arm_armpit_to_wrist": {
        "description": "From Armpit to ulna Wrist bone, arm straight.",
        "diagram": "Lp3",
        "full_name": "Arm: Armpit to Wrist, inside",
        "number": "L08"
    },
    "arm_armscye_back_center_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Armscye Back to Elbow Tip.",
        "diagram": "Op11",
        "full_name": "Arm: Armscye Back Center to Wrist, high bend",
        "number": "O14"
    },
    "arm_elbow_circ": {
        "description": "Elbow circumference, arm straight.",
        "diagram": "Lp4",
        "full_name": "Arm: Elbow circumference",
        "number": "L13"
    },
    "arm_elbow_circ_bent": {
        "description": "Elbow circumference, arm is bent.",
        "diagram": "Lp1",
        "full_name": "Arm: Elbow circumference, bent",
        "number": "L04"
    },
    "arm_elbow_to_wrist": {
        "description": "From Elbow to Wrist, arm straight. ('Arm: Shoulder Tip to Wrist' - 'Arm: Shoulder Tip to Elbow').",
        "diagram": "Lp2",
        "full_name": "Arm: Elbow to Wrist",
        "number": "L07"
    },
    "arm_elbow_to_wrist_bent": {
        "description": "Elbow tip to wrist. ('Arm: Shoulder Tip to Wrist, bent' - 'Arm: Shoulder Tip to Elbow, bent').",
        "diagram": "Lp1",
        "full_name": "Arm: Elbow to Wrist, bent",
        "number": "L03"
    },
    "arm_elbow_to_wrist_inside": {
        "description": "From inside Elbow to Wrist. ('Arm: Armpit to Wrist, inside' - 'Arm: Armpit to Elbow, inside').",
        "diagram": "Lp3",
        "full_name": "Arm: Elbow to Wrist, inside",
        "number": "L10"
    },
    "arm_lower_circ": {
        "description": "Arm circumference where lower arm is widest.",
        "diagram": "Lp4",
        "full_name": "Arm: Lower Arm circumference",
        "number": "L14"
    },
    "arm_neck_back_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Back to Elbow Tip.",
        "diagram": "Op8",
        "full_name": "Arm: Neck Back to Elbow, high bend",
        "number": "O08"
    },
    "arm_neck_back_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Back to Elbow Tip to Wrist bone.",
        "diagram": "Op8",
        "full_name": "Arm: Neck Back to Wrist, high bend",
        "number": "O09"
    },
    "arm_neck_side_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Side to Elbow Tip.",
        "diagram": "Op9",
        "full_name": "Arm: Neck Side to Elbow, high bend",
        "number": "O10"
    },
    "arm_neck_side_to_finger_tip": {
        "description": "From Neck Side down arm to tip of middle finger. ('Shoulder Length' + 'Arm: Shoulder Tip to Wrist' + 'Hand: Length').",
        "diagram": "Lp7",
        "full_name": "Arm: Neck Side to Finger Tip",
        "number": "L18"
    },
    "arm_neck_side_to_outer_elbow": {
        "description": "From Neck Side over Shoulder Tip down to Elbow. (Shoulder length + Arm: Shoulder Tip to Elbow).",
        "diagram": "Lp10",
        "full_name": "Arm: Neck side to Elbow",
        "number": "L22"
    },
    "arm_neck_side_to_wrist": {
        "description": "From Neck Side to Wrist. ('Shoulder Length' + 'Arm: Shoulder Tip to Wrist').",
        "diagram": "Lp6",
        "full_name": "Arm: Neck Side to Wrist",
        "number": "L17"
    },
    "arm_neck_side_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Side to Elbow Tip to Wrist bone.",
        "diagram": "Op9",
        "full_name": "Arm: Neck Side to Wrist, high bend",
        "number": "O11"
    },
    "arm_shoulder_tip_to_armfold_line": {
        "description": "From Shoulder Tip down to Armpit level.",
        "diagram": "Lp5",
        "full_name": "Arm: Shoulder Tip to Armfold line",
        "number": "L16"
    },
    "arm_shoulder_tip_to_elbow": {
        "description": "From Shoulder tip to Elbow Tip, arm straight.",
        "diagram": "Lp2",
        "full_name": "Arm: Shoulder Tip to Elbow",
        "number": "L06"
    },
    "arm_shoulder_tip_to_elbow_bent": {
        "description": "Bend Arm, measure from Shoulder Tip to Elbow Tip.",
        "diagram": "Lp1",
        "full_name": "Arm: Shoulder Tip to Elbow, bent",
        "number": "L02"
    },
    "arm_shoulder_tip_to_wrist": {
        "description": "From Shoulder Tip to Wrist bone, arm straight.",
        "diagram": "Lp2",
        "full_name": "Arm: Shoulder Tip to Wrist",
        "number": "L05"
    },
    "arm_shoulder_tip_to_wrist_bent": {
        "description": "Bend Arm, measure from Shoulder Tip around Elbow to radial Wrist bone.",
        "diagram": "Lp1",
        "full_name": "Arm: Shoulder Tip to Wrist, bent",
        "number": "L01"
    },
    "arm_upper_circ": {
        "description": "Arm circumference at Armpit level.",
        "diagram": "Lp4",
        "full_name": "Arm: Upper Arm circumference",
        "number": "L11"
    },
    "arm_wrist_circ": {
        "description": "Wrist circumference.",
        "diagram": "Lp4",
        "full_name": "Arm: Wrist circumference",
        "number": "L15"
    },
    "armfold_to_armfold_b": {
        "description": "Distance du pli du bars dos à l'autre, le long de la carrure.",
        "diagram": "Ip3",
        "full_name": "Du pli du bras à l'autre, dos.",
        "number": "I09"
    },
    "armfold_to_armfold_bust": {
        "description": "Measure in a curve from Armfold Left Front through Bust Front curved back up to Armfold Right Front.",
        "diagram": "Pp9",
        "full_name": "Armfold to Armfold, front, curved through Bust Front",
        "number": "P09"
    },
    "armfold_to_armfold_f": {
        "description": "La plus courte distance, d'un coté à l'autre des plis du bras, devant, non parallèle au sol.",
        "diagram": "Ip1",
        "full_name": "D'un coté à l'autre des plis du bras, devant",
        "number": "I04"
    },
    "armfold_to_bust_front": {
        "description": "Measure from Armfold Front to Bust Front, shortest distance between the two, as straight as possible.",
        "diagram": "Pp10",
        "full_name": "Armfold to Bust Front",
        "number": "P10"
    },
    "armpit_to_waist_side": {
        "description": "Distance de sous l'aisselle à la taille en latéral.",
        "diagram": "Hp3",
        "full_name": "Aisselle à la taille en latéral",
        "number": "H03"
    },
    "armscye_arc": {
        "description": "From Armscye at Across Chest over ShoulderTip  to Armscye at Across Back.",
        "diagram": "Pp12",
        "full_name": "Armscye: Arc",
        "number": "P12"
    },
    "armscye_circ": {
        "description": "Let arm hang at side. Measure Armscye circumference through Shoulder Tip and Armpit.",
        "diagram": "Lp8",
        "full_name": "Armscye: Circumference",
        "number": "L19"
    },
    "armscye_length": {
        "description": "Vertical distance from Shoulder Tip to Armpit.",
        "diagram": "Lp8",
        "full_name": "Armscye: Length",
        "number": "L20"
    },
    "armscye_width": {
        "description": "Horizontal distance between Armscye Front and Armscye Back.",
        "diagram": "Lp9",
        "full_name": "Armscye: Width",
        "number": "L21"
    },
    "body_armfold_circ": {
        "description": "Mesure du torse en incluant les bras au niveau de la pliure du bras.",
        "diagram": "Gp7",
        "full_name": "Tour des épaules au niveau de la pliure du bras",
        "number": "G43"
    },
    "body_bust_circ": {
        "description": "Mesure prise autour des bras et du torse au niveau des seins.",
        "diagram": "Gp7",
        "full_name": "Tour du corps au niveau de la poirtrine",
        "number": "G44"
    },
    "body_torso_circ": {
        "description": "Tour de corps passant par le milieu de l'épaule et l'entrejambe.",
        "diagram": "Gp8",
        "full_name": "Tour de corps à la verticale",
        "number": "G45"
    },
    "bust_arc_b": {
        "description": "D'un coté à l'autre de la poitrine, par la carrure dos ('Tour de poitrine' - 'Demi-tour de poirtrine, devant').",
        "diagram": "Gp4",
        "full_name": "Demi-tour de poitrine, dos",
        "number": "G28"
    },
    "bust_arc_f": {
        "description": "D'un coté à l'autre de la poitrine, par la carrure devant.",
        "diagram": "Gp2",
        "full_name": "Arc de poitrine, devant.",
        "number": "G12"
    },
    "bust_arc_half_b": {
        "description": "Moitié d''arc de poitrine, dos.' ('Arc de poitrine, dos' / 2).",
        "diagram": "Gp5",
        "full_name": "Demi-arc de poitrine, dos",
        "number": "G36"
    },
    "bust_arc_half_f": {
        "description": "Moitié d''arc de poitrine, devant.' ('Arc de poitrine, devant' / 2).",
        "diagram": "Gp3",
        "full_name": "Demi-arc de poitrine, devant.",
        "number": "G20"
    },
    "bust_circ": {
        "description": "Circonférence autour du buste,passant par les pointes de sein, parallèle au sol",
        "diagram": "Gp1",
        "full_name": "Tour de poitrine",
        "number": "G04"
    },
    "bust_to_waist_b": {
        "description": "Distance de la ligne de poitrine, dos à la ligne de poitrine, dos ('Encolure dos à taille dos' - 'Encolure dos àpoitrine dos').",
        "diagram": "Hp7",
        "full_name": "poitrine dos à taille dos",
        "number": "H24"
    },
    "bust_to_waist_f": {
        "description": "Distance de la ligne de poitrine devant à la taille. ('Distance entre l'encolure et la taille , devant' - 'Encolure devant à ligne  de poitrine, devant').",
        "diagram": "Hp4",
        "full_name": "Ligne de poitrine devant à taille devant",
        "number": "H10"
    },
    "bustpoint_neck_side_to_waist": {
        "description": "De l'encolure latéral à la taille en passant par la pointe de sein. ('De la pointe de sein à l'encolure, latéral' + 'De la pointe de sein à la taille').",
        "diagram": "Jp3",
        "full_name": "Pointe de sein, encolure latéral à la taille ",
        "number": "J06"
    },
    "bustpoint_to_bustpoint": {
        "description": "Distance entre les pointes de sein",
        "diagram": "Jp1",
        "full_name": "D'une pointe de sein à l'autre",
        "number": "J01"
    },
    "bustpoint_to_bustpoint_half": {
        "description": "Moitié 'D'une pointe de sein à l'autre' ('D'une pointe de sein à l'autre' / 2').",
        "diagram": "Jp2",
        "full_name": "Demi-écart entre pointes de sein",
        "number": "J05"
    },
    "bustpoint_to_bustpoint_halter": {
        "description": "From Bustpoint around Neck Back down to other Bustpoint.",
        "diagram": "Jp5",
        "full_name": "Bustpoint to Bustpoint Halter",
        "number": "J09"
    },
    "bustpoint_to_lowbust": {
        "description": "Mesure de la courbe allant de la pointe de sein à la ligne sous-mammaire.",
        "diagram": "Jp1",
        "full_name": "De la pointe de sein à ligne sous-mammaire",
        "number": "J03"
    },
    "bustpoint_to_neck_side": {
        "description": "De point d'encolure latéral à la pointe de sein.",
        "diagram": "Jp1",
        "full_name": "De la pointe de sein à l'encolure, latéral",
        "number": "J02"
    },
    "bustpoint_to_shoulder_center": {
        "description": "From center of Shoulder to Bustpoint.",
        "diagram": "Jp6",
        "full_name": "Bustpoint to Shoulder Center",
        "number": "J10"
    },
    "bustpoint_to_shoulder_tip": {
        "description": "From Bustpoint to Shoulder tip.",
        "diagram": "Jp4",
        "full_name": "Bustpoint to Shoulder Tip",
        "number": "J07"
    },
    "bustpoint_to_waist": {
        "description": "Distance allant de la pointe de sein vers la ligne de taille, en ligne droite (sans suivre le corps).",
        "diagram": "Jp1",
        "full_name": "De la pointe de sein à la taille",
        "number": "J04"
    },
    "bustpoint_to_waist_front": {
        "description": "From Bustpoint to Waist Front, in a straight line, not following the curves of the body.",
        "diagram": "Jp4",
        "full_name": "Bustpoint to Waist Front",
        "number": "J08"
    },
    "crotch_length": {
        "description": "Put tape across gap between buttocks at Hip level. Measure from Waist Front down betwen legs and up to Waist Back.",
        "diagram": "Np1",
        "full_name": "Longueur de l'entrejambe",
        "number": "N01"
    },
    "crotch_length_b": {
        "description": "Put tape across gap between buttocks at Hip level. Measure from Waist Back to mid-Crotch, either at the vagina or between testicles and anus).",
        "diagram": "Np2",
        "full_name": "Crotch length, back",
        "number": "N02"
    },
    "crotch_length_f": {
        "description": "From Waist Front to start of vagina or end of testicles. ('Crotch length' - 'Crotch length, back').",
        "diagram": "Np2",
        "full_name": "Crotch length, front",
        "number": "N03"
    },
    "dart_width_bust": {
        "description": "Cette information provient de tableaux de certains systèmes de patronage, ex: Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp2",
        "full_name": "Largeur de pince de poitrine",
        "number": "Q02"
    },
    "dart_width_shoulder": {
        "description": "Cette information provient de tableaux de certains systèmes de patronage, ex: Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp1",
        "full_name": "Largeur de pince d'épaule",
        "number": "Q01"
    },
    "dart_width_waist": {
        "description": "Cette information provient de tableaux de certains systèmes de patronage, ex: Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp3",
        "full_name": "Largeur de pince de taille",
        "number": "Q03"
    },
    "foot_circ": {
        "description": "Mesurer la circonférence au plus large du pied.",
        "diagram": "Ep2",
        "full_name": "Tour de pied",
        "number": "E03"
    },
    "foot_instep_circ": {
        "description": "Mesure du tour au plus creux du cou-de-pied",
        "diagram": "Ep2",
        "full_name": "Tour de cou-de-pied",
        "number": "E04"
    },
    "foot_length": {
        "description": "Mesure du derrière du talon au bout du plus grand orteil.",
        "diagram": "Ep2",
        "full_name": "Longueur de pied",
        "number": "E02"
    },
    "foot_width": {
        "description": "Mesurer la partie la plus large du pied.",
        "diagram": "Ep1",
        "full_name": "Largeur de pied",
        "number": "E01"
    },
    "hand_circ": {
        "description": "Mettre le pouce vers le plus petit doigt, garder les doigts serrés. Mesurer la circonférence au plus large de la main.",
        "diagram": "Dp3",
        "full_name": "Tour de main",
        "number": "D05"
    },
    "hand_length": {
        "description": "distance du poignet au bout du majeur.",
        "diagram": "Dp1",
        "full_name": "longueur de main",
        "number": "D02"
    },
    "hand_palm_circ": {
        "description": "Circonférence là où la paume est la plus large.",
        "diagram": "Dp2",
        "full_name": "Tour de paume",
        "number": "D04"
    },
    "hand_palm_length": {
        "description": "Distance du poignet à la base du majeur.",
        "diagram": "Dp1",
        "full_name": "Main: Longueur paume",
        "number": "D01"
    },
    "hand_palm_width": {
        "description": "Mesurer là où la paume est la plus large.",
        "diagram": "Dp1",
        "full_name": "Largeur paume",
        "number": "D03"
    },
    "head_chin_to_neck_back": {
        "description": "Longueur verticale du menton à la nuque. ('Stature' - 'Hauteur de la 7ème cervicale' - 'Hauteur de tête')",
        "diagram": "Fp3",
        "full_name": "Tête: Du menton à la nuque",
        "number": "F06"
    },
    "head_circ": {
        "description": "Mesurer la circonférence au plus large de la tête.",
        "diagram": "Fp1",
        "full_name": "Tour de tête",
        "number": "F01"
    },
    "head_crown_to_neck_back": {
        "description": "Longueur verticale du sommet du crâne à la nuque. ('Taille - 'Hauteur de la 7ème cervicale')",
        "diagram": "Fp3",
        "full_name": "Tête: Du sommet à la nuque",
        "number": "F05"
    },
    "head_depth": {
        "description": "Distance horizontale depuis le visage jusqu'à l'arrière de la tête",
        "diagram": "Fp1",
        "full_name": "Profondeur de tête",
        "number": "F03"
    },
    "head_length": {
        "description": "Distance verticale depuis le sommet du crâne jusqu'à la base de la mâchoire.",
        "diagram": "Fp1",
        "full_name": "hauteur de tête",
        "number": "F02"
    },
    "head_width": {
        "description": "Distance horizontale d'un côté à l'autre de la tête, au plus large.",
        "diagram": "Fp2",
        "full_name": "Largeur de tête",
        "number": "F04"
    },
    "height": {
        "description": "Hauteur du sommet de la tête au sol.",
        "diagram": "Ap1",
        "full_name": "Stature",
        "number": "A01"
    },
    "height_ankle": {
        "description": "Longueur verticale du point où le devant de la jambe rencontre le pied au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur de la cheville",
        "number": "A11"
    },
    "height_ankle_high": {
        "description": "Distance verticale du point le plus rentré de la cheville jusqu'au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur du haut de la cheville",
        "number": "A10"
    },
    "height_armpit": {
        "description": "Longueur verticale de l'aisselle au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur de l'aisselle",
        "number": "A04"
    },
    "height_bustpoint": {
        "description": "Hauteur de la pointe de sein.",
        "diagram": "Ap1",
        "full_name": "Hauteur de la poitrine",
        "number": "A14"
    },
    "height_calf": {
        "description": "Longueur verticale du plus large du mollet au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur du mollet",
        "number": "A09"
    },
    "height_gluteal_fold": {
        "description": "Distance verticale du pli glutéal (où le muscle glutéal rencontre le haut de l'arrière de la cuisse) jusqu'au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur sous fesses",
        "number": "A07"
    },
    "height_highhip": {
        "description": "Longueur verticale des petites hanches, là où le ventre est le plus proéminent, au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur du bassin",
        "number": "A12"
    },
    "height_hip": {
        "description": "Longueur verticale de la hanche au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur des hanches",
        "number": "A06"
    },
    "height_knee": {
        "description": "Longueur verticale du pli derrière le genou au sol",
        "diagram": "Ap1",
        "full_name": "Hauteur du genou",
        "number": "A08"
    },
    "height_knee_to_ankle": {
        "description": "Distance verticale du pli derrière le genou au point où le devant de la jambe rencontre le pied.",
        "diagram": "Ap2",
        "full_name": "Distance Genou à la cheville",
        "number": "A21"
    },
    "height_neck_back": {
        "description": "Distance verticale de la 7ème cervicale au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur de la 7ème cervicale",
        "number": "A02"
    },
    "height_neck_back_to_knee": {
        "description": "Distance de la 7ème cervicale au pli derrière le genou.",
        "diagram": "Ap2",
        "full_name": "Distance: 7ème cervicale au genou",
        "number": "A18"
    },
    "height_neck_back_to_waist_side": {
        "description": "Dsiatnce verticale de la 7ème cervicale au côté de la taille. ('Hauteur de la 7ème cervicale' - 'Hauteur latérale tour de taille').",
        "diagram": "Ap2",
        "full_name": "Distance: 7ème cervicale au tour de taille latéral",
        "number": "A22"
    },
    "height_neck_front": {
        "description": "Longueur verticale du devant de l'encolure au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur encolure devant",
        "number": "A16"
    },
    "height_neck_side": {
        "description": "Longueur verticale du point d'encolure latéral au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur encolure latérale",
        "number": "A17"
    },
    "height_scapula": {
        "description": "Distance verticale de l'omoplate au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur de l'omoplate",
        "number": "A03"
    },
    "height_shoulder_tip": {
        "description": "Hauteur du point d'épaule au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur du Point d'épaule",
        "number": "A15"
    },
    "height_waist_back": {
        "description": "Hauteur verticale de la taille dos au sol. (\"Hauteur de la taille devant\" - \"Hauteur de l'entrejambe\")",
        "diagram": "Ap2",
        "full_name": "Hauteur de la taille, dos",
        "number": "A23"
    },
    "height_waist_front": {
        "description": "Longueur verticale du devant de la taille au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur de la taille devant",
        "number": "A13"
    },
    "height_waist_side": {
        "description": "Longueur verticale du côté de la taille au sol.",
        "diagram": "Ap1",
        "full_name": "Hauteur latérale tour de taille",
        "number": "A05"
    },
    "height_waist_side_to_hip": {
        "description": "Distance latérale tour de taille au tour de hanches.",
        "diagram": "Ap2",
        "full_name": "Distance latérale tour de taille aux tour de hanches",
        "number": "A20"
    },
    "height_waist_side_to_knee": {
        "description": "Longueur verticale du côté de la taille au pli derrière le genou.",
        "diagram": "Ap2",
        "full_name": "Distance latérale tour de taille au genou",
        "number": "A19"
    },
    "highbust_arc_b": {
        "description": "Du point de sur-poitrine devant à point de sur-poitrine latéral par le dos ('Tour sur-poitrinaire' - 'arc sur-poitrinaire, devant')",
        "diagram": "Gp4",
        "full_name": "Demi-tour sur-poitrinaire, dos",
        "number": "G27"
    },
    "highbust_arc_f": {
        "description": "De chaque pli du bras par la carrure devant.",
        "diagram": "Gp2",
        "full_name": "Arc sur-poitrinaire, devant",
        "number": "G11"
    },
    "highbust_arc_half_b": {
        "description": "Moitié de l'arc sur-poitrinaire dos. Du point de sur-poitrine devant à point de sur-poitrine latéral ('Arc sur-poitrinaire, dos' / 2).",
        "diagram": "Gp5",
        "full_name": "Moitié d'arc sur-poitrinaire, dos",
        "number": "G35"
    },
    "highbust_arc_half_f": {
        "description": "Moitié de 'arc sur-poitrinaire'. Du point de sur-poitrine devant à point de sur-poitrine latéral ('arc sur-poitrinaire, devant' / 2)",
        "diagram": "Gp3",
        "full_name": "Moitié d'arc sur-poitrinaire, devant",
        "number": "G19"
    },
    "highbust_b_over_shoulder_to_highbust_f": {
        "description": "From Highbust Back, over Shoulder, then aim at Bustpoint, stopping measurement at Highbust level.",
        "diagram": "Pp11",
        "full_name": "Highbust Back, over Shoulder, to Highbust level",
        "number": "P11"
    },
    "highbust_back_over_shoulder_to_armfold_front": {
        "description": "From Highbust Back over Shoulder to Armfold Front.",
        "diagram": "Pp4",
        "full_name": "Highbust Back, over Shoulder, to Armfold Front",
        "number": "P04"
    },
    "highbust_back_over_shoulder_to_waist_front": {
        "description": "From Highbust Back, over Shoulder touching  Neck Side, to Waist Front.",
        "diagram": "Pp5",
        "full_name": "Highbust Back, over Shoulder, to Waist Front",
        "number": "P05"
    },
    "highbust_circ": {
        "description": "Tour au dessus de la poitrine, passant entre les deux plis de bras et sous les aisselles.",
        "diagram": "Gp1",
        "full_name": "Tour sur-poitrinaire",
        "number": "G03"
    },
    "highbust_to_waist_b": {
        "description": "Distance de la ligne de sur-poitrine dos à la taille dos ('Encolure dos à taille dos' - 'Encolure dos à sur-poitrine dos').",
        "diagram": "Hp7",
        "full_name": "Sur-poitrine dos à taille dos",
        "number": "H22"
    },
    "highbust_to_waist_f": {
        "description": "Distance du milieu de la ligne de sur-poitrine devant à la ligne de taille , devant, vertical. On utilise un bolduc entre les pointes de sein. ('Distance entre l'encolure et la taille , devant' - 'Encolure devant à ligne sur-poitrinaire, devant').",
        "diagram": "Hp4",
        "full_name": "ligne de sur-poitrine devant à ligne de taille",
        "number": "H08"
    },
    "highhip_arc_b": {
        "description": "D'un coté à l'autre du bassin par le dos. ('Tour de bassin' - 'Demi_tour de bassin, devant').",
        "diagram": "Gp4",
        "full_name": "Demi tour de bassin, dos",
        "number": "G32"
    },
    "highhip_arc_f": {
        "description": "D'un coté à l'autre du bassin, devant.",
        "diagram": "Gp2",
        "full_name": "Arc bassin, devant.",
        "number": "G16"
    },
    "highhip_arc_half_b": {
        "description": "Moitié de 'demi-tour de bassin, dos'. ('Moitié de demi-tour de bassin, dos' / 2).",
        "diagram": "Gp5",
        "full_name": "Moitié de demi-tour de bassin, dos",
        "number": "G40"
    },
    "highhip_arc_half_f": {
        "description": "Moitié de 'demi-tour de bassin, devant'. ('Moitié de demi-tour de bassin, devant' / 2).",
        "diagram": "Gp3",
        "full_name": "Moitié de demi-tour de bassin, devant",
        "number": "G24"
    },
    "highhip_circ": {
        "description": "Circonférence du bassin, là où le ventre est le plus fort, parallèle au sol.",
        "diagram": "Gp1",
        "full_name": "Tour de bassin",
        "number": "G08"
    },
    "hip_arc_b": {
        "description": "D'un coté à l'autres des hanches par le dos. ('Tour de hanches' - 'Demi tour de hanches, devant').",
        "diagram": "Gp4",
        "full_name": "Demi tour de hanches, dos",
        "number": "G33"
    },
    "hip_arc_f": {
        "description": "D'un coté à l'autre des hanches, devant.",
        "diagram": "Gp2",
        "full_name": "Arc hanches, devant.",
        "number": "G17"
    },
    "hip_arc_half_b": {
        "description": "Moitié du 'demi-tour de hanches, dos'. ('Demi-tour de hanches, dos' / 2).",
        "diagram": "Gp5",
        "full_name": "Moitié du demi-tour de hanche, dos",
        "number": "G41"
    },
    "hip_arc_half_f": {
        "description": "Moitié du 'demi-tour de hanches'. ('Demi-tour de hanches, devant' / 2).",
        "diagram": "Gp3",
        "full_name": "Moitié du demi-tour de hanche, devant",
        "number": "G25"
    },
    "hip_circ": {
        "description": "Tour de hanches, où elles sont le plus larges, parallèle au sol.",
        "diagram": "Gp1",
        "full_name": "Tour de hanches",
        "number": "G09"
    },
    "hip_circ_with_abdomen": {
        "description": "Mesure du tour de hanches intégrant la profondeur de ventre ('Demi-hanches, dos' + 'Demi-hanches avec ventre, devant').",
        "diagram": "Gp9",
        "full_name": "Tour de hanches, incluant le ventre",
        "number": "G46"
    },
    "hip_with_abdomen_arc_f": {
        "description": "Feuille de papier autour du ventre, scotché sur les cotés. La mesure s'effectue d'un coté à l'autre de la hanche en passant par la feuille de papier.",
        "diagram": "Gp6",
        "full_name": "Segment de hanche, devant, passant par le ventre.",
        "number": "G42"
    },
    "indent_ankle_high": {
        "description": "Distance horizontale entre un support plat placé perpendiculairement au talon et le point le plus creux de la cheville.",
        "diagram": "Cp2",
        "full_name": "Creux talon d'achille",
        "number": "C03"
    },
    "indent_neck_back": {
        "description": "Distance horizontale entre le sommet des homoplates et le creux de la nuque",
        "diagram": "Cp1",
        "full_name": "Cambrure de la nuque",
        "number": "C01"
    },
    "indent_waist_back": {
        "description": "Distance horizontale entre une ligne imaginaire passant par le sommet des homoplates et le creux lombaire (cambrure)",
        "diagram": "Cp2",
        "full_name": "Cambrure",
        "number": "C02"
    },
    "leg_ankle_circ": {
        "description": "Ankle circumference where front of leg meets the top of the foot.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle circumference",
        "number": "M09"
    },
    "leg_ankle_diag_circ": {
        "description": "Ankle circumference diagonal from top of foot to bottom of heel.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle diagonal circumference",
        "number": "M11"
    },
    "leg_ankle_high_circ": {
        "description": "Ankle circumference where the indentation at the back of the ankle is the deepest.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle High circumference",
        "number": "M08"
    },
    "leg_calf_circ": {
        "description": "Calf circumference at the largest part of lower leg.",
        "diagram": "Mp2",
        "full_name": "Leg: Calf circumference",
        "number": "M07"
    },
    "leg_crotch_to_ankle": {
        "description": "From Crotch to Ankle. ('Leg: Crotch to Floor' - 'Height: Ankle').",
        "diagram": "Mp3",
        "full_name": "Leg: Crotch to Ankle",
        "number": "M12"
    },
    "leg_crotch_to_floor": {
        "description": "Stand feet close together. Measure from crotch level (touching body, no extra space) down to floor.",
        "diagram": "Mp1",
        "full_name": "hauteur d'entrejambe",
        "number": "M01"
    },
    "leg_knee_circ": {
        "description": "Knee circumference with straight leg.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee circumference",
        "number": "M05"
    },
    "leg_knee_circ_bent": {
        "description": "Knee circumference with leg bent.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee circumference, bent",
        "number": "M10"
    },
    "leg_knee_small_circ": {
        "description": "Leg circumference just below the knee.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee Small circumference",
        "number": "M06"
    },
    "leg_thigh_mid_circ": {
        "description": "Thigh circumference about halfway between Crotch and Knee.",
        "diagram": "Mp2",
        "full_name": "Leg: Thigh Middle circumference",
        "number": "M04"
    },
    "leg_thigh_upper_circ": {
        "description": "Thigh circumference at the fullest part of the upper Thigh near the Crotch.",
        "diagram": "Mp2",
        "full_name": "Leg: Thigh Upper circumference",
        "number": "M03"
    },
    "leg_waist_side_to_ankle": {
        "description": "From Waist Side to Ankle. ('Leg: Waist Side to Floor' - 'Height: Ankle').",
        "diagram": "Mp3",
        "full_name": "Leg: Waist Side to Ankle",
        "number": "M13"
    },
    "leg_waist_side_to_floor": {
        "description": "From Waist Side along curve to Hip level then straight down to floor.",
        "diagram": "Mp1",
        "full_name": "Leg: Waist Side to floor",
        "number": "M02"
    },
    "leg_waist_side_to_knee": {
        "description": "From Waist Side along curve to Hip level then straight down to  Knee level. ('Leg: Waist Side to Floor' - 'Height Knee').",
        "diagram": "Mp3",
        "full_name": "Leg: Waist Side to Knee",
        "number": "M14"
    },
    "lowbust_arc_b": {
        "description": "Segment sous-mammaire d'un coté à l'autre, passant par le dos ('Tour sous-mammaire' - 'Demi-tour sous-mammaire, devant').",
        "diagram": "Gp4",
        "full_name": "Demi-arc sous-mammaire, dos",
        "number": "G29"
    },
    "lowbust_arc_f": {
        "description": "D'un coté à l'autre, sous la poitrine.",
        "diagram": "Gp2",
        "full_name": "Arc sous-mammaire, devant.",
        "number": "G13"
    },
    "lowbust_arc_half_b": {
        "description": "Moitié de 'Arc sous-mammaire, dos' ('Arc sous-mammaire, dos' / 2).",
        "diagram": "Gp5",
        "full_name": "Demi-arc sous-mammaire, dos",
        "number": "G37"
    },
    "lowbust_arc_half_f": {
        "description": "Moitié de 'Arc sous-mammaire, devant' ('Arc sous-mammaire, devant' / 2).",
        "diagram": "Gp3",
        "full_name": "Demi-arc sous-mammaire, devant",
        "number": "G21"
    },
    "lowbust_circ": {
        "description": "Tour de la cage thoracique sous les seins, parallèle au sol.",
        "diagram": "Gp1",
        "full_name": "Tour sous-mammaire",
        "number": "G05"
    },
    "lowbust_to_waist_b": {
        "description": "Distance de la ligne sous-mammaire, dos à la ligne de taille, dos.",
        "diagram": "Hp7",
        "full_name": "Sous-mammaire à taille, dos",
        "number": "H25"
    },
    "lowbust_to_waist_f": {
        "description": "Distance de la ligne sous-mammaire devant à la taille devant",
        "diagram": "Hp4",
        "full_name": "Ligne sous-mammaire devant à taille devant",
        "number": "H11"
    },
    "neck_arc_b": {
        "description": "D'un point d'encolure à l'autre en passant par le dos ('Encolure' - demi-encolure, devant').",
        "diagram": "Gp4",
        "full_name": "Demi-encolure, dos",
        "number": "G26"
    },
    "neck_arc_f": {
        "description": "Demi-encolure devant, d'un point d'encolure à l'autre.",
        "diagram": "Gp2",
        "full_name": "Demie encolure devant",
        "number": "G10"
    },
    "neck_arc_half_b": {
        "description": "Moitié de 'demi-ecolure, dos'. ('Demi-encolure, dos' / 2).",
        "diagram": "Gp5",
        "full_name": "Moitié de demi-encolure, dos.",
        "number": "G34"
    },
    "neck_arc_half_f": {
        "description": "Moitié de demi-ecolure ('Demi-encolure, devant' / 2).",
        "diagram": "Gp3",
        "full_name": "Moitié de demi-encolure, devant.",
        "number": "G18"
    },
    "neck_back_to_across_back": {
        "description": "Mesure du point d'encolure dos à la ligne de carrure dos",
        "diagram": "Hp13",
        "full_name": "Encolure dos à la carrure dos",
        "number": "H41"
    },
    "neck_back_to_armfold_front": {
        "description": "From Neck Back over Shoulder to Armfold Front.",
        "diagram": "Pp2",
        "full_name": "Neck Back to Armfold Front",
        "number": "P02"
    },
    "neck_back_to_armfold_front_to_highbust_back": {
        "description": "From Neck Back over Shoulder to Armfold Front, under arm to Highbust Back.",
        "diagram": "Pp8",
        "full_name": "Neck Back, to Armfold Front, to Highbust Back",
        "number": "P08"
    },
    "neck_back_to_armfold_front_to_neck_back": {
        "description": "From Neck Back, over Shoulder to Armfold Front, under arm and return to start.",
        "diagram": "Pp6",
        "full_name": "Neck Back, to Armfold Front, to Neck Back",
        "number": "P06"
    },
    "neck_back_to_armfold_front_to_waist_side": {
        "description": "From Neck Back, over Shoulder, down chest to Waist Side.",
        "diagram": "Pp3",
        "full_name": "Neck Back, over Shoulder, to Waist Side",
        "number": "P03"
    },
    "neck_back_to_bust_b": {
        "description": "Distance du point d'encolure dos à la ligne de poitrine dos.",
        "diagram": "Hp7",
        "full_name": "Encolure dos à la poitrine dos",
        "number": "H23"
    },
    "neck_back_to_bust_front": {
        "description": "From Neck Back, over Shoulder, to Bust Front.",
        "diagram": "Pp1",
        "full_name": "Neck Back to Bust Front",
        "number": "P01"
    },
    "neck_back_to_highbust_b": {
        "description": "Distance du point d'encolure dos à la ligne de sur poitrine, dos.",
        "diagram": "Hp7",
        "full_name": "Encolure dos à sur-poitrine dos",
        "number": "H21"
    },
    "neck_back_to_shoulder_tip_b": {
        "description": "Distance du point d'encolure dos jusqu'au point d'épaule.",
        "diagram": "Ip6",
        "full_name": "Du point d'encolure dos jusqu'au point d'épaule.",
        "number": "I13"
    },
    "neck_back_to_waist_b": {
        "description": "Distance du point d'encolure dos à la ligne de taille dos.",
        "diagram": "Hp6",
        "full_name": "Encolure dos à la taille dos",
        "number": "H19"
    },
    "neck_back_to_waist_front": {
        "description": "From Neck Back around Neck Side down to Waist Front.",
        "diagram": "Op1",
        "full_name": "Neck Back to Waist Front",
        "number": "O01"
    },
    "neck_back_to_waist_side": {
        "description": "From Neck Back diagonal across back to Waist Side.",
        "diagram": "Kp5",
        "full_name": "Neck Back to Waist Side",
        "number": "K06"
    },
    "neck_circ": {
        "description": "Tour de cou à la base, en passant par la 7eme cervicale, de côtés et de devant.",
        "diagram": "Gp1",
        "full_name": "Encolure",
        "number": "G02"
    },
    "neck_front_to_bust_f": {
        "description": "Distance du point d'encolure au milieu devant à la ligne de sur-poitrine, vertical. On utilise un bolduc entre les pointes de sein.",
        "diagram": "Hp4",
        "full_name": "Encolure devant à ligne  de poitrine, devant",
        "number": "H09"
    },
    "neck_front_to_highbust_f": {
        "description": "Distance du point d'encolure au milieu devant à la ligne de sur-poitrine, vertical.",
        "diagram": "Hp4",
        "full_name": "Encolure devant à ligne sur-poitrinaire, devant",
        "number": "H07"
    },
    "neck_front_to_shoulder_tip_f": {
        "description": "Distance du point d'encolure devant jusqu'au point d'épaule.",
        "diagram": "Ip5",
        "full_name": "Du point d'encolure devant jusqu'au point d'épaule.",
        "number": "I12"
    },
    "neck_front_to_waist_f": {
        "description": "Distance de l'encolure devant à la taille devant, en passant par le tour de poitrine.",
        "diagram": "Hp1",
        "full_name": "Distance entre l'encolure et la taille , devant",
        "number": "H01"
    },
    "neck_front_to_waist_flat_f": {
        "description": "Distance de l'encolure devant à la taille devant, en passant entre les seins.",
        "diagram": "Hp2",
        "full_name": "Distance entre l'encolure et la taille à plat, devant",
        "number": "H02"
    },
    "neck_front_to_waist_side": {
        "description": "From Neck Front diagonal to Waist Side.",
        "diagram": "Kp2",
        "full_name": "Neck Front to Waist Side",
        "number": "K02"
    },
    "neck_mid_circ": {
        "description": "Tour de cou, à la moitié de la machoire et du torse.",
        "diagram": "Gp1",
        "full_name": "Tour de cou, au milieu",
        "number": "G01"
    },
    "neck_side_to_armfold_b": {
        "description": "From Neck Side diagonal to Armfold Back.",
        "diagram": "Kp9",
        "full_name": "Neck Side to Armfold Back",
        "number": "K11"
    },
    "neck_side_to_armfold_f": {
        "description": "From Neck Side diagonal to Armfold Front.",
        "diagram": "Kp6",
        "full_name": "Neck Side to Armfold Front",
        "number": "K08"
    },
    "neck_side_to_armpit_b": {
        "description": "From Neck Side diagonal across back to Highbust Side (Armpit).",
        "diagram": "Kp10",
        "full_name": "Neck Side to Highbust Side, back",
        "number": "K12"
    },
    "neck_side_to_armpit_f": {
        "description": "From Neck Side diagonal across front to Highbust Side (Armpit).",
        "diagram": "Kp7",
        "full_name": "Neck Side to Highbust Side, front",
        "number": "K09"
    },
    "neck_side_to_bust_b": {
        "description": "Distance du point d'encolure latéral à la ligne de poitrine, dos, vertical.",
        "diagram": "Hp8",
        "full_name": "Encolure latéral a poitrine, dos",
        "number": "H27"
    },
    "neck_side_to_bust_f": {
        "description": "Distance du point d'encolure latéral à la ligne de poitrine, devant, vertical.",
        "diagram": "Hp5",
        "full_name": "Du point d'encolure latéral a la ligne de poitrine, devant",
        "number": "H14"
    },
    "neck_side_to_bust_side_b": {
        "description": "Neck Side diagonal across back to Bust Side.",
        "diagram": "Kp11",
        "full_name": "Neck Side to Bust Side, back",
        "number": "K13"
    },
    "neck_side_to_bust_side_f": {
        "description": "Neck Side diagonal across front to Bust Side.",
        "diagram": "Kp8",
        "full_name": "Neck Side to Bust Side, front",
        "number": "K10"
    },
    "neck_side_to_highbust_b": {
        "description": "Distance du point d'encolure latéral à la ligne de sur-poitrine, dos, vertical.",
        "diagram": "Hp8",
        "full_name": "Du point d'encolure latéral à la ligne de sur-poirtrine, dos",
        "number": "H28"
    },
    "neck_side_to_highbust_f": {
        "description": "Distance du point d'encolure latéral à la ligne de sur-poitrine, devant, vertical.",
        "diagram": "Hp5",
        "full_name": "Du point d'encolure latéral à la ligne de sur-poirtrine, devant",
        "number": "H15"
    },
    "neck_side_to_waist_b": {
        "description": "Du point d'encolure latéral à la taille dos.",
        "diagram": "Hp6",
        "full_name": "Encolure latéral à la taille, dos",
        "number": "H18"
    },
    "neck_side_to_waist_bustpoint_f": {
        "description": "Distance du point d'encolure latéral, passant par la pointe de sein, jusqu'à la taille.",
        "diagram": "Hp3",
        "full_name": "Point d'encolure latéral à la taille par la pointe de sein",
        "number": "H06"
    },
    "neck_side_to_waist_f": {
        "description": "Distance du point d'encolure latéral à la taille devant.",
        "diagram": "Hp3",
        "full_name": "Encolure latéral à la taille, devant",
        "number": "H05"
    },
    "neck_side_to_waist_scapula_b": {
        "description": "Distance du point d'encolure latéral, passant par lhomoplate, jusqu'à la taille.",
        "diagram": "Hp6",
        "full_name": "Point d'encolure latéral à la taille par l'homoplate",
        "number": "H20"
    },
    "neck_side_to_waist_side_b": {
        "description": "From Neck Side diagonal across back to Waist Side.",
        "diagram": "Kp5",
        "full_name": "Neck Side to Waist Side, back",
        "number": "K07"
    },
    "neck_side_to_waist_side_f": {
        "description": "From Neck Side diagonal across front to Waist Side.",
        "diagram": "Kp2",
        "full_name": "Neck Side to Waist Side, front",
        "number": "K03"
    },
    "neck_width": {
        "description": "Mesure entre les pendants verticaux d'un collier ouvert sur le devant.",
        "diagram": "Ip7",
        "full_name": "Largeur d'encolure",
        "number": "I14"
    },
    "rib_arc_b": {
        "description": "D'un coté à l'autre de la cage thoracique, dos. ('Tour thorax' - 'Demi-thorax, devant').",
        "diagram": "Gp4",
        "full_name": "Demi-thorax, dos",
        "number": "G30"
    },
    "rib_arc_f": {
        "description": "D'un coté à l'autre de la cage thoracique, devant.",
        "diagram": "Gp2",
        "full_name": "Demi-thorax, devant.",
        "number": "G14"
    },
    "rib_arc_half_b": {
        "description": "Moitié de 'Arc thoracique, dos'. ('Arc thoracique, dos' / 2).",
        "diagram": "Gp5",
        "full_name": "Demi-arc thoracique, dos",
        "number": "G38"
    },
    "rib_arc_half_f": {
        "description": "Moitié de 'Arc thoracique, devant'. ('Arc thoracique, devant' / 2).",
        "diagram": "Gp3",
        "full_name": "Demi-arc thoracique, devant",
        "number": "G22"
    },
    "rib_circ": {
        "description": "Tour de la cage thoracique au niveau des cotes flottantes, parallèle au sol.",
        "diagram": "Gp1",
        "full_name": "Tour du thorax",
        "number": "G06"
    },
    "rib_to_waist_side": {
        "description": "De la ligne de thorax passant par la dernière cote flottante et la taille, en latéral.",
        "diagram": "Hp4",
        "full_name": "Distance latérale entre le thorax et la taille ",
        "number": "H12"
    },
    "rise_length_b": {
        "description": "Vertical distance from Waist Back to Crotch level. ('Height: Waist Back' - 'Leg: Crotch to Floor')",
        "diagram": "Np4",
        "full_name": "Rise length, back",
        "number": "N06"
    },
    "rise_length_diag": {
        "description": "Measure from Waist Side diagonally to a string tied at the top of the leg, seated on a hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, diagonal",
        "number": "N05"
    },
    "rise_length_f": {
        "description": "Vertical Distance from Waist Front to Crotch level. ('Height: Waist Front' - 'Leg: Crotch to Floor')",
        "diagram": "Np4",
        "full_name": "Rise length, front",
        "number": "N07"
    },
    "rise_length_side": {
        "description": "Vertical distance from Waist side down to Crotch level. Use formula (Height: Waist side - Leg: Crotch to floor).",
        "diagram": "Np5",
        "full_name": "Rise length, side",
        "number": "N08"
    },
    "rise_length_side_sitting": {
        "description": "From Waist Side around hp curve down to surface, while seated on hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, side, sitting",
        "number": "N04"
    },
    "shoulder_center_to_highbust_b": {
        "description": "Distance du centre de l'épaule dos à la ligne de sur-poitrine, en passant par l'homoplate.",
        "diagram": "Hp8",
        "full_name": "de centre épaule à sur-poitrine, dos",
        "number": "H29"
    },
    "shoulder_center_to_highbust_f": {
        "description": "Distance du centre de l'épaule devant à la ligne de sur-poitrine, à l'opposé de la pointe de sein.",
        "diagram": "Hp5",
        "full_name": "de centre épaule à sur-poitrine, devant",
        "number": "H16"
    },
    "shoulder_length": {
        "description": "Distance de la base latérale de l'encolure jusqu'à l'extrémité de l'épaule.",
        "diagram": "Ip1",
        "full_name": "Longueur d'épaule",
        "number": "I01"
    },
    "shoulder_slope_neck_back_angle": {
        "description": "Angle formé par la ligne qui passe par le point d'encolure dos et le point d'épaule, et la ligne passant par le point d'encolure, parallèle au sol.",
        "diagram": "Hp11",
        "full_name": "Angle pente d'épaule à l'encolure dos",
        "number": "H38"
    },
    "shoulder_slope_neck_back_height": {
        "description": "Distance entre la nuque et l'extrémité de l'épaule.",
        "diagram": "Hp11",
        "full_name": "Hauteur pente d'épaule à l'encolure dos",
        "number": "H39"
    },
    "shoulder_slope_neck_side_angle": {
        "description": "Angle formé par la ligne qui passe par le point d'encolure latéral et le point d'épaule, et la ligne verticale passant par le point d'épaule.",
        "diagram": "Hp11",
        "full_name": "Pente d'épaule à l'encolure laterale",
        "number": "H36"
    },
    "shoulder_slope_neck_side_length": {
        "description": "Distance verticale entre la base latérale de l'encolure jusqu'au point d'épaule.",
        "diagram": "Hp11",
        "full_name": "Hauteur pente d'épaule à l'encolure laterale",
        "number": "H37"
    },
    "shoulder_slope_shoulder_tip_angle": {
        "description": "Angle formé par la ligne qui passe par la base latérale de l'encolure et l'extrémité de l'épaule, et la ligne verticale passant par l'extrémité de l'épaule.",
        "diagram": "Hp12",
        "full_name": "Pente d'épaule au point d'épaule.",
        "number": "H40"
    },
    "shoulder_tip_to_armfold_b": {
        "description": "Mesure de l'emmanchure du point d'épaule au point de pli du bras, dos.",
        "diagram": "Hp8",
        "full_name": "Point d'épaule à pli du bras, dos",
        "number": "H26"
    },
    "shoulder_tip_to_armfold_f": {
        "description": "Mesure de l'emmanchure du point d'épaule au point de pli du bras, devant.",
        "diagram": "Hp5",
        "full_name": "Pointe d'épaule à pli du bras, devant",
        "number": "H13"
    },
    "shoulder_tip_to_shoulder_tip_b": {
        "description": "Longueur d'un bout de l'épaule à l'autre, en passant par le dos.",
        "diagram": "Ip3",
        "full_name": "D'un bout de l'épaule à l'autre, dos",
        "number": "I07"
    },
    "shoulder_tip_to_shoulder_tip_f": {
        "description": "Longueur d'un bout de l'épaule à l'autre, en passant par le devant.",
        "diagram": "Ip1",
        "full_name": "D'un bout de l'épaule à l'autre, devant",
        "number": "I02"
    },
    "shoulder_tip_to_shoulder_tip_half_b": {
        "description": "Moitié de la distance entre les deux points d'épaule, dos ('point d'épaule à point d'épaule' / 2).",
        "diagram": "Ip4",
        "full_name": "Demie distance entre les deux points d'épaule, dos",
        "number": "I10"
    },
    "shoulder_tip_to_shoulder_tip_half_f": {
        "description": "Moitié de la distance entre les deux points d'épaule, devant ('point d'épaule à point d'épaule' / 2).",
        "diagram": "Ip2",
        "full_name": "demie_distance_entre_les_deux_points_dépaule_devant",
        "number": "I05"
    },
    "shoulder_tip_to_waist_b_1in_offset": {
        "description": "Mark 1in (2.54cm) outward from Waist Back along Waist level. Measure from Shoulder Tip diagonal to mark.",
        "diagram": "Kp4",
        "full_name": "Shoulder Tip to Waist Back, with 1in (2.54cm) offset",
        "number": "K05"
    },
    "shoulder_tip_to_waist_back": {
        "description": "From Shoulder Tip diagonal to Waist Back.",
        "diagram": "Kp3",
        "full_name": "Shoulder Tip to Waist Back",
        "number": "K04"
    },
    "shoulder_tip_to_waist_front": {
        "description": "From Shoulder Tip diagonal to Waist Front.",
        "diagram": "Kp1",
        "full_name": "Shoulder Tip to Waist Front",
        "number": "K01"
    },
    "shoulder_tip_to_waist_side_b": {
        "description": "Distance du point d'épaule, passant par l'emmanchure dos, vers la taille en latéral.",
        "diagram": "Hp6",
        "full_name": "Du point d'épaule à la taille en latéral, dos",
        "number": "H17"
    },
    "shoulder_tip_to_waist_side_f": {
        "description": "Distance du point d'épaule, en contournant l'emmanchure par devant, descendant à la taille en latéral.",
        "diagram": "Hp3",
        "full_name": "Point d'épaule à la taille, latéral, devant",
        "number": "H04"
    },
    "waist_arc_b": {
        "description": "D'un coté à l'autre de la taille par le dos ('Tour de taille' - 'Demi-taille, devant').",
        "diagram": "Gp4",
        "full_name": "Demi-taille, dos",
        "number": "G31"
    },
    "waist_arc_f": {
        "description": "D'un coté à l'autre de la taille, devant.",
        "diagram": "Gp2",
        "full_name": "Arc taille, devant.",
        "number": "G15"
    },
    "waist_arc_half_b": {
        "description": "Moitié de d\"emi-tour de taille, dos\". ('demi-tour de taille, dos.' / 2).",
        "diagram": "Gp5",
        "full_name": "Moitié du demi-tour de taille, dos.",
        "number": "G39"
    },
    "waist_arc_half_f": {
        "description": "Moitié de d\"emi-tour de taille, devant\". ('demi-tour de taille, devant.' / 2).",
        "diagram": "Gp3",
        "full_name": "Moitié du demi-tour de taille, devant.",
        "number": "G23"
    },
    "waist_circ": {
        "description": "Tour de taille, en suivant les lignes du corps. En général la taille est plus haute dans le dos.",
        "diagram": "Gp1",
        "full_name": "Tour de taille",
        "number": "G07"
    },
    "waist_natural_arc_b": {
        "description": "From Side to Side at Natural Waist level, across the back. Calculate as ( Natural Waist circumference  - Natural Waist arc (front) ).",
        "diagram": "Op5",
        "full_name": "Natural Waist arc, back",
        "number": "O05"
    },
    "waist_natural_arc_f": {
        "description": "From Side to Side at the Natural Waist level, across the front.",
        "diagram": "Op4",
        "full_name": "Natural Waist arc, front",
        "number": "O04"
    },
    "waist_natural_circ": {
        "description": "Torso circumference at men's natural side Abdominal Obliques indentation, if Oblique indentation isn't found then just below the Navel level.",
        "diagram": "Op3",
        "full_name": "Natural Waist circumference",
        "number": "O03"
    },
    "waist_to_highhip_b": {
        "description": "Distance de la ligne de taille dans le dos à la ligne de bassin dos.",
        "diagram": "Hp10",
        "full_name": "Taille dos à bassin dos",
        "number": "H33"
    },
    "waist_to_highhip_f": {
        "description": "Distance de la ligne de taille devant à la ligne d ebassin bassin devant.",
        "diagram": "Hp9",
        "full_name": "Taille devant à bassin devant",
        "number": "H30"
    },
    "waist_to_highhip_side": {
        "description": "Distance de la ligne de taille latéral à la ligne de sur-poitrine latéral.",
        "diagram": "Hp9",
        "full_name": "Taille latéral à sur-poitrine latéral",
        "number": "H32"
    },
    "waist_to_hip_b": {
        "description": "Distance de la ligne de taille dans le dos à la ligne de hanches dos. Nécessite un bolduc tendu au sommet des fesses.",
        "diagram": "Hp10",
        "full_name": "Taille dos aux hanches dos.",
        "number": "H34"
    },
    "waist_to_hip_f": {
        "description": "Distance de la ligne de taille devant à la ligne de hanches devant",
        "diagram": "Hp9",
        "full_name": "Taille devant à hanches devant",
        "number": "H31"
    },
    "waist_to_hip_side": {
        "description": "Distance de la ligne de taille à la ligne de hanches, en latéral.",
        "diagram": "Hp10",
        "full_name": "Taille au hanches en lateral",
        "number": "H35"
    },
    "waist_to_natural_waist_b": {
        "description": "Length from Waist Back to Natural Waist Back.",
        "diagram": "Op7",
        "full_name": "Waist Back to Natural Waist Back",
        "number": "O07"
    },
    "waist_to_natural_waist_f": {
        "description": "Length from Waist Front to Natural Waist Front.",
        "diagram": "Op6",
        "full_name": "Waist Front to Natural Waist Front",
        "number": "O06"
    },
    "waist_to_waist_halter": {
        "description": "From Waist level around Neck Back to Waist level.",
        "diagram": "Op2",
        "full_name": "Waist to Waist Halter, around Neck Back",
        "number": "O02"
    },
    "width_abdomen_to_hip": {
        "description": "Distance horizontale entre le sommet du ventre et le sommet du fessier",
        "diagram": "Bp2",
        "full_name": "Largeur de l'aplomb du ventre et fessier",
        "number": "B05"
    },
    "width_bust": {
        "description": "Largeur horizontale d'un côté du buste à l'autre.",
        "diagram": "Bp1",
        "full_name": "Largeur: Buste",
        "number": "B02"
    },
    "width_hip": {
        "description": "Longueur horizontale d'un côté de hanches à l'autre.",
        "diagram": "Bp1",
        "full_name": "Largeur de hanches",
        "number": "B04"
    },
    "width_shoulder": {
        "description": "Largeur horizontale d'un bout de l'épaule à l'autre.",
        "diagram": "Bp1",
        "full_name": "Largeur: Epaule ",
        "number": "B01"
    },
    "width_waist": {
        "description": "Largeur horizontale d'un côté de la taille à l'autre.",
        "diagram": "Bp1",
        "full_name": "Largeur: Taille",
        "number": "B03"
    }
}
