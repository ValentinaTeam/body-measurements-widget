module.exports = {
    "across_back_b": {
        "description": "Von der einen Achsel zur anderen die kürzeste Strecke über den Rücken messen.",
        "diagram": "Ip3",
        "full_name": "Über den Rücken gemessen",
        "number": "I08"
    },
    "across_back_center_to_armfold_front_to_across_back_center": {
        "description": "Vom Zentrum von 'Über den Rücken gemessen' über die Schulter, unter dem Arm durch und zurück zum Anfang messen.",
        "diagram": "Pp7",
        "full_name": "'Über den Rücken gemessen'-Zentrum, um die Schulter herum messen.",
        "number": "P07"
    },
    "across_back_half_b": {
        "description": "Die Hälfte von 'Über den Rücken gemessen' ('Über den Rücken gemessen' / 2)",
        "diagram": "Ip4",
        "full_name": "Über den Rücken gemessen, halb",
        "number": "I11"
    },
    "across_back_to_waist_b": {
        "description": "Von der Mitte von 'Über den Rücken gemessen' zur Taille hinten messen.",
        "diagram": "Hp13",
        "full_name": "Von 'Über den Rücken gemessen' zur Taille hinten",
        "number": "H42"
    },
    "across_chest_f": {
        "description": "Von der einen Achsel zur anderen die kürzeste Strecke über die Brust messen.",
        "diagram": "Ip1",
        "full_name": "Über die Brust gemessen",
        "number": "I03"
    },
    "across_chest_half_f": {
        "description": "Die Hälfte von 'Über die Brust gemessen' ('Über die Brust gemessen' / 2)",
        "diagram": "Ip2",
        "full_name": "Über die Brust gemessen, halb",
        "number": "I06"
    },
    "arm_above_elbow_circ": {
        "description": "Der Umfang des Oberarms auf Höhe des Bizeps.",
        "diagram": "Lp4",
        "full_name": "Arm: Umfang über dem Ellenbogen",
        "number": "L12"
    },
    "arm_across_back_center_to_elbow_bent": {
        "description": "Arm mit dem Ellenbogen nach außen beugen, die Hand nach vorn gehalten. Von der Mitte des Rückens zur Ellenbogenspitze messen.",
        "diagram": "Op10",
        "full_name": "Arm: Mitte von \"Über den Rücken\" zum Ellenbogen, hoch gebeugt",
        "number": "O12"
    },
    "arm_across_back_center_to_wrist_bent": {
        "description": "Arm mit dem Ellenbogen nach außen beugen, die Hand nach vorn gehalten. Von der Mitte des Rückens über den Ellenbogen zum Handgelenk messen.",
        "diagram": "Op10",
        "full_name": "Arm: Mitte von \"Über den Rücken\" zum Handgelenk, hoch gebeugt",
        "number": "O13"
    },
    "arm_armpit_to_elbow": {
        "description": "Bei gestrecktem Arm von der Achsel zum Ellenbogen messen.",
        "diagram": "Lp3",
        "full_name": "Arm: Achsel zum Ellenbogen, innen",
        "number": "L09"
    },
    "arm_armpit_to_wrist": {
        "description": "Bei gestrecktem Arm von der Achsel zum Handgelenk messen.",
        "diagram": "Lp3",
        "full_name": "Arm: Achsel zum Handgelenk, innen",
        "number": "L08"
    },
    "arm_armscye_back_center_to_wrist_bent": {
        "description": "Arm mit dem Ellenbogen nach außen beugen, die Hand nach vorn gehalten. Von der Rückseite des Ärmellochs zur Ellenbogenspitze messen.",
        "diagram": "Op11",
        "full_name": "Arm: Ärmelloch-Rückseite zum Handgelenk, hoch gebeugt",
        "number": "O14"
    },
    "arm_elbow_circ": {
        "description": "Arm: Umfang des Ellenbogens, gestreckt",
        "diagram": "Lp4",
        "full_name": "Arm: Umfang des Ellenbogens",
        "number": "L13"
    },
    "arm_elbow_circ_bent": {
        "description": "Umfang des Ellenbogens messen, während dieser gebeugt ist.",
        "diagram": "Lp1",
        "full_name": "Arm: Umfang des Ellenbogens, gebeugt",
        "number": "L04"
    },
    "arm_elbow_to_wrist": {
        "description": "Vom Ellenbogen zum Handgelenk messen, den Arm dabei gestreckt halten.",
        "diagram": "Lp2",
        "full_name": "Arm: Ellbogen zum Handgelenk",
        "number": "L07"
    },
    "arm_elbow_to_wrist_bent": {
        "description": "Ellenbogen zum Handgelenk. ('Arm: Schulterspitze zum Handgelenk, gebogen' - 'Arm: Schulterspitze zum Ellbogen, gebogen').",
        "diagram": "Lp1",
        "full_name": "Arm: Ellbogen zum Handgelenk, gebeugt",
        "number": "L03"
    },
    "arm_elbow_to_wrist_inside": {
        "description": "Von der Innenseite des Ellenbogens zum Handgelenk messen. ('Arm: Achsel zum Handgelenk, innen' - 'Arm: Achsel zum Ellenbogen, innen').",
        "diagram": "Lp3",
        "full_name": "Arm: Ellenbogen zum Handgelenk, innen",
        "number": "L10"
    },
    "arm_lower_circ": {
        "description": "Umfang der breitesten Stelle des Unterarms.",
        "diagram": "Lp4",
        "full_name": "Arm: Umfang des Unterarms",
        "number": "L14"
    },
    "arm_neck_back_to_elbow_bent": {
        "description": "Arm mit dem Ellenbogen nach außen beugen, die Hand nach vorn gehalten. Von der Hinterseite des Nackens zur Ellenbogenspitze messen.",
        "diagram": "Op8",
        "full_name": "Arm: Hinterseite des Nackens bis zum Ellenbogen, hoch gebeugt",
        "number": "O08"
    },
    "arm_neck_back_to_wrist_bent": {
        "description": "Arm mit dem Ellenbogen nach außen beugen, die Hand nach vorn gehalten. Vom Nacken über den Ellenbogen zum Handgelenk messen.",
        "diagram": "Op8",
        "full_name": "Arm: Nacken zum Handgelenk, hoch gebeugt",
        "number": "O09"
    },
    "arm_neck_side_to_elbow_bent": {
        "description": "Arm mit dem Ellenbogen nach außen beugen, die Hand nach vorn gehalten. Von der Seite des Halses zur Ellenbogenspitze messen.",
        "diagram": "Op9",
        "full_name": "Arm: Halsseite zum Ellenbogen, hoch gebeugt",
        "number": "O10"
    },
    "arm_neck_side_to_finger_tip": {
        "description": "Von der Seite des Halses zum Handgelenk messen. ('Schulterbreite' + 'Arm: Schulterspitze zum Handgelenk' + 'Handgelenk').",
        "diagram": "Lp7",
        "full_name": "Arm: Halsseite zu Fingerspitzen",
        "number": "L18"
    },
    "arm_neck_side_to_outer_elbow": {
        "description": "Von der Halsseite über die Schulterspitze herunter zum Ellenbogen messen. ('Schulterlänge' + 'Arm: Schulterspitze zum Ellenbogen')",
        "diagram": "Lp10",
        "full_name": "Arm: Halsseite zum äußeren Ellenbogen",
        "number": "L22"
    },
    "arm_neck_side_to_wrist": {
        "description": "Von der Seite des Halses zum Handgelenk messen. ('Schulterbreite' + 'Arm: Schulterspitze zum Handgelenk').",
        "diagram": "Lp6",
        "full_name": "Arm: Halsseite zum Handgelenk",
        "number": "L17"
    },
    "arm_neck_side_to_wrist_bent": {
        "description": "Arm mit dem Ellenbogen nach außen beugen, die Hand nach vorn gehalten. Von der Seite des Halses über den Ellenbogen zum Handgelenk messen.",
        "diagram": "Op9",
        "full_name": "Arm: Halsseite zum Handgelenk, hoch gebeugt",
        "number": "O11"
    },
    "arm_shoulder_tip_to_armfold_line": {
        "description": "Von der Schulterspitze zur Achselhöhe messen.",
        "diagram": "Lp5",
        "full_name": "Arm: Schulterspitze zur Achsel",
        "number": "L16"
    },
    "arm_shoulder_tip_to_elbow": {
        "description": "Arm gerade ausstrecken, von der Schulterspitze bis zum Ellbogen messen.",
        "diagram": "Lp2",
        "full_name": "Arm: Schulterspitze zum Ellbogen",
        "number": "L06"
    },
    "arm_shoulder_tip_to_elbow_bent": {
        "description": "Arm beugen, von der Schulterspitze bis zum Ellbogen messen.",
        "diagram": "Lp1",
        "full_name": "Arm: Schulterspitze zum Ellbogen, gebeugt",
        "number": "L02"
    },
    "arm_shoulder_tip_to_wrist": {
        "description": "Von der Schulterspitze zum Handgelenk messen, während der Arm gerade gehalten wird.",
        "diagram": "Lp2",
        "full_name": "Arm: Schulterspitze zum Handgelenk",
        "number": "L05"
    },
    "arm_shoulder_tip_to_wrist_bent": {
        "description": "Arm beugen, von der Schulterspitze über den Ellenbogen bis zum Handgelenkknochen messen.",
        "diagram": "Lp1",
        "full_name": "Arm: Schulterspitze zum Handgelenk, gebeugt",
        "number": "L01"
    },
    "arm_upper_circ": {
        "description": "Der Umfang des Oberarms auf Höhe der Achsel.",
        "diagram": "Lp4",
        "full_name": "Arm: Umfang des Oberarms",
        "number": "L11"
    },
    "arm_wrist_circ": {
        "description": "Der Umfang des Handgelenks.",
        "diagram": "Lp4",
        "full_name": "Arm: Handgelenksumfang",
        "number": "L15"
    },
    "armfold_to_armfold_b": {
        "description": "Von der einen Achsel zur anderen über den Rücken messen.",
        "diagram": "Ip3",
        "full_name": "Achsel zur Achsel, hinten",
        "number": "I09"
    },
    "armfold_to_armfold_bust": {
        "description": "In einer Kurve von der linken Achsel über das Zentrum der Brust zur rechten Achsel messen.",
        "diagram": "Pp9",
        "full_name": "Achsel zu Achsel, vorne, als Kurve über die Brust",
        "number": "P09"
    },
    "armfold_to_armfold_f": {
        "description": "Von der einen Achsel zur anderen messen. Die kürzeste Strecke wählen, diese muss nicht parallel zum Boden sein.",
        "diagram": "Ip1",
        "full_name": "Achsel zu Achsel, vorne",
        "number": "I04"
    },
    "armfold_to_bust_front": {
        "description": "In einer geraden, kürzestmöglichen Linie von der Achsel vorne zur Mitte der Brust messen.",
        "diagram": "Pp10",
        "full_name": "Achsel zur Brust",
        "number": "P10"
    },
    "armpit_to_waist_side": {
        "description": "Von der Achselhöhle zur Seite der Taille messen.",
        "diagram": "Hp3",
        "full_name": "Achselhöhle zur Taillenseite",
        "number": "H03"
    },
    "armscye_arc": {
        "description": "Auf der Höhe von 'Über die Brust/den Rücken gemessen' vom Ärmelloch vorne über die Schulterspitze zum Ärmelloch hinten messen.",
        "diagram": "Pp12",
        "full_name": "Ärmelloch: Bogen",
        "number": "P12"
    },
    "armscye_circ": {
        "description": "Arm seitlich hängen lassen. Umfang des Ärmellochs über die Schulterspitze und die Achsel messen.",
        "diagram": "Lp8",
        "full_name": "Ärmelloch: Umfang",
        "number": "L19"
    },
    "armscye_length": {
        "description": "Vertikaler Abstand von der Schulterspitze zur Achsel.",
        "diagram": "Lp8",
        "full_name": "Ärmelloch: Länge",
        "number": "L20"
    },
    "armscye_width": {
        "description": "Horizontaler Abstand zwischen der Vorder- und Rückseite des Ärmellochs.",
        "diagram": "Lp9",
        "full_name": "Ärmelloch: Breite",
        "number": "L21"
    },
    "body_armfold_circ": {
        "description": "Um Arme und Brustkorb herum auf Achselhöhe messen.",
        "diagram": "Gp7",
        "full_name": "Körperumfang auf Achselhöhe",
        "number": "G43"
    },
    "body_bust_circ": {
        "description": "Um Arme und Torso herum auf Brusthöhe messen.",
        "diagram": "Gp7",
        "full_name": "Körperumfang auf Brusthöhe",
        "number": "G44"
    },
    "body_torso_circ": {
        "description": "Torsoumfang von der Schultermitte durch den Schritt zurück zur Schultermitte.",
        "diagram": "Gp8",
        "full_name": "Körperumfang des gesamten Torso",
        "number": "G45"
    },
    "bust_arc_b": {
        "description": "Von Brustseite zu Brustseite über den Rücken gemessen. ('Brustumfang' - 'Brustumfang vorne').",
        "diagram": "Gp4",
        "full_name": "Brustumfang, hinten",
        "number": "G28"
    },
    "bust_arc_f": {
        "description": "Von Brustseite zu Brustseite über die Brüste gemessen.",
        "diagram": "Gp2",
        "full_name": "Brustumfang vorne",
        "number": "G12"
    },
    "bust_arc_half_b": {
        "description": "Die Hälfte 'Brustumfang hinten'. ('Brustumfang, hinten' / 2).",
        "diagram": "Gp5",
        "full_name": "Hälfte des hinterer Brustumfang",
        "number": "G36"
    },
    "bust_arc_half_f": {
        "description": "Die Hälfte des vorderen Brustumfang. ('Brustumfang vorne'/2).",
        "diagram": "Gp3",
        "full_name": "Hälfte des vorderen Brustumfang",
        "number": "G20"
    },
    "bust_circ": {
        "description": "Brustumfang, parallel zum Boden gemessen.",
        "diagram": "Gp1",
        "full_name": "Brustumfang",
        "number": "G04"
    },
    "bust_to_waist_b": {
        "description": "Von der Brust zur Taille, über den Rücken gemessen. ('Nacken hinten bis Taille hinten' - 'Nacken zum Rücken auf Brusthöhe')",
        "diagram": "Hp7",
        "full_name": "Brust hinten zur Taille hinten",
        "number": "H24"
    },
    "bust_to_waist_f": {
        "description": "Von der Vorderseite der Brust bis zur Taille messen. ('Hals vorne zur Taille vorne' - 'Hals vorne zur Brust vorne').",
        "diagram": "Hp4",
        "full_name": "Brust vorne zur Taille vorne",
        "number": "H10"
    },
    "bustpoint_neck_side_to_waist": {
        "description": "Von der Halsseite zum Brustpunkt, dann gerade herunter zur Taillenhöhe messen. ('Halsseite zum Brustpunkt' + 'Brustpunkt zur Taillenhöhe')",
        "diagram": "Jp3",
        "full_name": "Brustpunkt, von der Nackenseite bis zur Taillenhöhe",
        "number": "J06"
    },
    "bustpoint_to_bustpoint": {
        "description": "Vom einen Brustpunkt zum anderen.",
        "diagram": "Jp1",
        "full_name": "Brustpunkt zu Brustpunkt",
        "number": "J01"
    },
    "bustpoint_to_bustpoint_half": {
        "description": "Die Hälfte von 'Brustpunkt zu Brustpunkt'. ('Brustpunkt zu Brustpunkt' / 2)",
        "diagram": "Jp2",
        "full_name": "Brustpunkt zu Brustpunkt, Hälfte",
        "number": "J05"
    },
    "bustpoint_to_bustpoint_halter": {
        "description": "Vom einen Brustpunkt um den Nacken runter zum anderen Brustpunkt messen.",
        "diagram": "Jp5",
        "full_name": "Brustpunkt zu Brustpunkt Träger",
        "number": "J09"
    },
    "bustpoint_to_lowbust": {
        "description": "Vom Brustpunkt zur Höhe der Unterbrust messen, dabei der Kurve der Brust folgen.",
        "diagram": "Jp1",
        "full_name": "Brustpunkt zur Unterbrust",
        "number": "J03"
    },
    "bustpoint_to_neck_side": {
        "description": "Vom Brustpunkt zur Seite des Halses messen.",
        "diagram": "Jp1",
        "full_name": "Brustpunkt zur Halsseite",
        "number": "J02"
    },
    "bustpoint_to_shoulder_center": {
        "description": "Von der Schultermitte zum Brustpunkt messen.",
        "diagram": "Jp6",
        "full_name": "Brustpunkt zur Schultermitte",
        "number": "J10"
    },
    "bustpoint_to_shoulder_tip": {
        "description": "Vom Brustpunkt zur Schulterspitze messen.",
        "diagram": "Jp4",
        "full_name": "Brustpunkt zur Schulterspitze",
        "number": "J07"
    },
    "bustpoint_to_waist": {
        "description": "Vom Brustpunkt in einer geraden Linie runter zur Taillenhöhe messen (nicht am Körper entlang messen).",
        "diagram": "Jp1",
        "full_name": "Brustpunkt zur Taillenhöhe",
        "number": "J04"
    },
    "bustpoint_to_waist_front": {
        "description": "Vom Brustpunkt in einer geraden Linie runter zur Vorderseite der Taille messen (nicht am Körper entlang messen).",
        "diagram": "Jp4",
        "full_name": "Brustpunkt zur Taille vorne",
        "number": "J08"
    },
    "crotch_length": {
        "description": "Den Spalt zwischen den Pobacken mit Klebeband überbrücken. Von der Rückseite der Taille zur Mitte des Schritts messen, entweder zur Scheide oder zwischen Anus und Testikeln.",
        "diagram": "Np1",
        "full_name": "Schrittlänge",
        "number": "N01"
    },
    "crotch_length_b": {
        "description": "Den Spalt zwischen den Pobacken auf Hüfthöhe mit Klebeband überbrücken. Von der Rückseite der Taille zur Mitte des Schritts messen, entweder zur Scheide oder zwischen Anus und Testikeln.",
        "diagram": "Np2",
        "full_name": "Schrittlänge, hinten",
        "number": "N02"
    },
    "crotch_length_f": {
        "description": "Von der Vorderseite der Taille zur Scheide oder zum Ende der Testikel messen. ('Schrittlänge' - 'Schrittlänge, hinten')",
        "diagram": "Np2",
        "full_name": "Schrittlänge, vorne",
        "number": "N03"
    },
    "dart_width_bust": {
        "description": "Diese Informationen wurden von den Schnittmuster-Tabellen von einigen Schnittmustersystemen wie z. B.  Winifred P. Aldrich's \"Metric Pattern Cutting\". übernommen.",
        "diagram": "Qp2",
        "full_name": "Abnäherbreite: Brust",
        "number": "Q02"
    },
    "dart_width_shoulder": {
        "description": "Diese Informationen wurden von den Schnittmuster-Tabellen von einigen Schnittmustersystemen wie z. B.  Winifred P. Aldrich's \"Metric Pattern Cutting\". übernommen.",
        "diagram": "Qp1",
        "full_name": "Abnäherbreite: Schulter",
        "number": "Q01"
    },
    "dart_width_waist": {
        "description": "Diese Informationen wurden von den Schnittmuster-Tabellen von einigen Schnittmustersystemen wie z. B. Winifred P. Aldrich's \"Metric Pattern Cutting\" übernommen.",
        "diagram": "Qp3",
        "full_name": "Abnäherbreite: Taille",
        "number": "Q03"
    },
    "foot_circ": {
        "description": "Umfang am breitesten Teil des Fußes messen.",
        "diagram": "Ep2",
        "full_name": "Fuss: Umfang",
        "number": "E03"
    },
    "foot_instep_circ": {
        "description": "Am höchsten Teil der Spanns messen.",
        "diagram": "Ep2",
        "full_name": "Fuß: Spannumfang",
        "number": "E04"
    },
    "foot_length": {
        "description": "Messen Sie von der Ferse bis zum Ende der längsten Zehe.",
        "diagram": "Ep2",
        "full_name": "Fuss: Länge",
        "number": "E02"
    },
    "foot_width": {
        "description": "An der stärksten Stelle des Fusses messen.",
        "diagram": "Ep1",
        "full_name": "Fuss: Breite",
        "number": "E01"
    },
    "hand_circ": {
        "description": "Finger zusammenhalten und um kleinen Finger und Daumen messen. Umfang an der stärksten Stelle nehmen..",
        "diagram": "Dp3",
        "full_name": "Hand: Umfang",
        "number": "D05"
    },
    "hand_length": {
        "description": "Länge vom Handgelenk bis zum Ende des Mittelfingers. ",
        "diagram": "Dp1",
        "full_name": "Hand: Länge",
        "number": "D02"
    },
    "hand_palm_circ": {
        "description": "Umfang, an der die Handfläche am stärksten ist.",
        "diagram": "Dp2",
        "full_name": "Hand: Handflächenumfang",
        "number": "D04"
    },
    "hand_palm_length": {
        "description": "Länge vom Handgelenk bis zum Ansatz des Mittelfingers.",
        "diagram": "Dp1",
        "full_name": "Hand: Länge der Handfläche",
        "number": "D01"
    },
    "hand_palm_width": {
        "description": "Messen der breitesten Stelle der Handfkäche.",
        "diagram": "Dp1",
        "full_name": "Breite: Handfläche",
        "number": "D03"
    },
    "head_chin_to_neck_back": {
        "description": "Vertikale Abmessung vom Kinn zum hinteren Halsansatz. ('Höhe' . 'Höhe hinterer Halsansatz' - 'Kopf: Länge')",
        "diagram": "Fp3",
        "full_name": "Kopf: Kinn zum hinteren Halsansatz",
        "number": "F06"
    },
    "head_circ": {
        "description": "Messen der stärksten Stelle des Kopfumfanges.",
        "diagram": "Fp1",
        "full_name": "Kopf: Umfang",
        "number": "F01"
    },
    "head_crown_to_neck_back": {
        "description": "Vertikale Abmessung vom Scheitel bis zum hinteren Halsansatz ('Höhe: Gesamt' - 'Höhe: hinterer Halsansatz')",
        "diagram": "Fp3",
        "full_name": "Kopf: Scheitel bis zum hinteren Halsansatz",
        "number": "F05"
    },
    "head_depth": {
        "description": "Horizontalen Abstand vor der Stirn zum Hinterkopf .",
        "diagram": "Fp1",
        "full_name": "Kopf: Tiefe",
        "number": "F03"
    },
    "head_length": {
        "description": "Vertikale Abmessung vom Scheitel zur Unterseite des Kiefers.",
        "diagram": "Fp1",
        "full_name": "Kopf: Länge",
        "number": "F02"
    },
    "head_width": {
        "description": "Horizontale Abstand von Schläfe zu Schläfe , wo der Kopf am breitesten ist.",
        "diagram": "Fp2",
        "full_name": "Kopf: Breite",
        "number": "F04"
    },
    "height": {
        "description": "Vertikale Abmessung vom Scheitel zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Total",
        "number": "A01"
    },
    "height_ankle": {
        "description": "Vertikale Abmessung von der Stelle an der das Vorderbein den Fuß berührt bis zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Sprunggelenk",
        "number": "A11"
    },
    "height_ankle_high": {
        "description": "Vertikale Abmessung vom tiefsten Einschnitt des Sprunggelenks bis zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Knöchel",
        "number": "A10"
    },
    "height_armpit": {
        "description": "Vertikale Abmessung von der Achsel zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Achse",
        "number": "A04"
    },
    "height_bustpoint": {
        "description": "Vertikaler Abstand vom Brustpunkt bis zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Brustpunkt",
        "number": "A14"
    },
    "height_calf": {
        "description": "Vertikale Abmessung vom dicksten Punkt der Wade zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Wade",
        "number": "A09"
    },
    "height_gluteal_fold": {
        "description": "Vertikale Abmessung vom Gesäß, bei dem die Po-Muskeln die Schenkel berühren, bis zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Gesäß",
        "number": "A07"
    },
    "height_highhip": {
        "description": "Vertikalen Abstand von der hohen Hüfte Ebene, stärkste Stelle des  vorderen Bauches, bis zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Hohe Hüfte",
        "number": "A12"
    },
    "height_hip": {
        "description": "Vertikale Abmessung vom Hüfte zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Hüfte",
        "number": "A06"
    },
    "height_knee": {
        "description": "Vertikale Abmessung von der Kniekehle bis zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Knie",
        "number": "A08"
    },
    "height_knee_to_ankle": {
        "description": "Vertikale Abmessung von der Kniekehle zu dem Punkt an dem sich das Vorderbein und der Fuß treffen.",
        "diagram": "Ap2",
        "full_name": "Höhe: Knie zu der Achsel",
        "number": "A21"
    },
    "height_neck_back": {
        "description": "Vertikale Abmessung vom Halswirbel zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Nacken hinten",
        "number": "A02"
    },
    "height_neck_back_to_knee": {
        "description": "Vertikale Abmessung vom Halswirbel (cervicale vertebra) zur Kniekehle",
        "diagram": "Ap2",
        "full_name": "Höhe: Halswirbel zu den Knien",
        "number": "A18"
    },
    "height_neck_back_to_waist_side": {
        "description": "Vertikale Abmessung vom Halswirbel zu der seitlichen Taillie. ('Höhe Halswirbel' - 'Höhe: seitliche Taillie').",
        "diagram": "Ap2",
        "full_name": "Höhe: Halswirbel zu der seitlichen Taillie",
        "number": "A22"
    },
    "height_neck_front": {
        "description": "Vertikale Abmessung von der vorderseite des Halses zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Hals vorne",
        "number": "A16"
    },
    "height_neck_side": {
        "description": "Vertikale Abmessung von der Halsseiten zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Halsseite",
        "number": "A17"
    },
    "height_scapula": {
        "description": "Vertikale Abmessumg vom Schulterblatt zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Schulterblatt",
        "number": "A03"
    },
    "height_shoulder_tip": {
        "description": "Vertikale Abmessung von der Schulterspitze zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Schulterspitze",
        "number": "A15"
    },
    "height_waist_back": {
        "description": "Senkrechter Abstand von der Taillenrückseite zum Boden. ('Höhe: Taille vorne' - 'Bein: Schritt zum Boden')",
        "diagram": "Ap2",
        "full_name": "Höhe: Taille hinten",
        "number": "A23"
    },
    "height_waist_front": {
        "description": "Vertikale Abmessung von der vorderen Taillie zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: vordere Taillie",
        "number": "A13"
    },
    "height_waist_side": {
        "description": "Vertikale Abmessung der Taille bis zum Boden.",
        "diagram": "Ap1",
        "full_name": "Höhe: Seitliche Taille",
        "number": "A05"
    },
    "height_waist_side_to_hip": {
        "description": "Vertikale Abmessung von der seitlichen Taillie zum Hüftgelenk",
        "diagram": "Ap2",
        "full_name": "Höhe: seitliche Taillie zu der Hüfte",
        "number": "A20"
    },
    "height_waist_side_to_knee": {
        "description": "Vertikale Abmessung von der seitlichen Taillie zu der Kniekehle.",
        "diagram": "Ap2",
        "full_name": "Höhe: Seitliche Taillie zu den Knien",
        "number": "A19"
    },
    "highbust_arc_b": {
        "description": "Die Hälfte des hinteren Brustumfanges. Von Seite zu Seite über den Rücken gemessen. ('Hoher Brustumfang vorne'/2).",
        "diagram": "Gp4",
        "full_name": "Hoher Brustumfang hinten",
        "number": "G27"
    },
    "highbust_arc_f": {
        "description": "Oberbrustumfang von Seite  (Achselhöhle) zu Seite (Achselhöhle), gemessen über die Brüste.",
        "diagram": "Gp2",
        "full_name": "Oberbrustumfang vorne",
        "number": "G11"
    },
    "highbust_arc_half_b": {
        "description": "Die Hälfte des hinteren hoheh Brustumfanges. Von Seite zu Seite über den Rücken gemessen. ('Hoher Brustumfang hinten'/2).",
        "diagram": "Gp5",
        "full_name": "Hälfte des hinteren hohen Brustumfang",
        "number": "G35"
    },
    "highbust_arc_half_f": {
        "description": "Die Hälfte des vorderen Brustumfanges. Von Seite zu Seite über die Brüste gemessen. ('Hoher Brustumfang vorne'/2).",
        "diagram": "Gp3",
        "full_name": "Hälfte des vorderen Brustumfang",
        "number": "G19"
    },
    "highbust_b_over_shoulder_to_highbust_f": {
        "description": "Von der Höhe der Oberbrust hinten über die Schulter Richtung Brustpunkt zur Oberbrust messen.",
        "diagram": "Pp11",
        "full_name": "Oberbrust hinten über Schulter zur Oberbrusthöhe",
        "number": "P11"
    },
    "highbust_back_over_shoulder_to_armfold_front": {
        "description": "Vom Rücken auf Höhe der Oberbrust über die Schulter zur Achsel vorne messen.",
        "diagram": "Pp4",
        "full_name": "Oberbrust hinten über Schulter zur Achsel vorne",
        "number": "P04"
    },
    "highbust_back_over_shoulder_to_waist_front": {
        "description": "Vom Rücken auf Höhe der Oberbrust über die Schulter (den Halls berührend) zur Taillenvorderseite messen.",
        "diagram": "Pp5",
        "full_name": "Oberbrust hinten über Schulter zur Taille vorne",
        "number": "P05"
    },
    "highbust_circ": {
        "description": "Oberbrustumfang, kürzeste Strecke über die Brüste gemessen unter den Achseln entlang.",
        "diagram": "Gp1",
        "full_name": "Hoher Brustumfang",
        "number": "G03"
    },
    "highbust_to_waist_b": {
        "description": "Auf dem Rücken messen: Von der Oberbrust zur Taille. ('Nacken hinten bis Taille hinten' - 'Nacken zur Oberbrust hinten')",
        "diagram": "Hp7",
        "full_name": "Oberbrust hinten zur Taille hinten",
        "number": "H22"
    },
    "highbust_to_waist_f": {
        "description": "Von der Vorderseite der Oberbrust bis zur Taille messen. Die Lücke zwischen den Brustpunkten z.B. mit Band schließen. ('Hals vorne zur Taille vorne' - 'Hals vorne zur Oberbrust vorne').",
        "diagram": "Hp4",
        "full_name": "Oberbrust vorne zur Taille vorne",
        "number": "H08"
    },
    "highhip_arc_b": {
        "description": "Von hohe Hüfte Seite zu hohe Hüfte Seite über den Rücken gemessen. ('Hohe Hüfte Umfang' - 'Hohe Hüfte Umfang, vorne').",
        "diagram": "Gp4",
        "full_name": "Hohe Hüfte, hinten",
        "number": "G32"
    },
    "highhip_arc_f": {
        "description": "Von hohe Hüfte Seite zu hohe Hüfte Seite über die Front gemessen.",
        "diagram": "Gp2",
        "full_name": "Umfang hohe Hüfte vorne",
        "number": "G16"
    },
    "highhip_arc_half_b": {
        "description": "Die Häfte 'der hohen Hüfte hinten'. Von der Mitte des Rücken bis zur Seite gemessen, auf dem Niveau der hohen Hüfte. ('Hohe Hüfte hinten/2').",
        "diagram": "Gp5",
        "full_name": "Die Hälfte des hinteren Umfangs hohe Hüfte",
        "number": "G40"
    },
    "highhip_arc_half_f": {
        "description": "Die Hälfte des vorderen hohen Hüfte. ('Umfang hohe Hüfte, vorne'/2).",
        "diagram": "Gp3",
        "full_name": "Hälfte Umfang der hohen Hüfte vorne",
        "number": "G24"
    },
    "highhip_circ": {
        "description": "Umfang der hohen Höfte , Stelle des stärksten Bauchumfanges, parallel zum Boden gemessen.",
        "diagram": "Gp1",
        "full_name": "Umfang hohe Hüfte",
        "number": "G08"
    },
    "hip_arc_b": {
        "description": "Von Hüftseite zu Hüfteseite über die Rückseite. ('Hüftumfang' - 'Hüftumfang vorne').",
        "diagram": "Gp4",
        "full_name": "Hüftumfang, hinten",
        "number": "G33"
    },
    "hip_arc_f": {
        "description": "Hüftumfang vorne von Seite zu Seite gemessen.",
        "diagram": "Gp2",
        "full_name": "Hüftumfang vorne",
        "number": "G17"
    },
    "hip_arc_half_b": {
        "description": "Hälfte vom 'Hüftumfang hinten'. ('Hüftumfang hinten' / 2).",
        "diagram": "Gp5",
        "full_name": "Hälfte des hinteren Hüftumfanges",
        "number": "G41"
    },
    "hip_arc_half_f": {
        "description": "Hälfte von 'Hüftumfang vorne'. ('Hüftumfang vorne' / 2).",
        "diagram": "Gp3",
        "full_name": "Hälfte des vorderen Hüftumfanges",
        "number": "G25"
    },
    "hip_circ": {
        "description": "Stärkste Stelle des Hüftumfanges, parallel zum Boden gemessen.",
        "diagram": "Gp1",
        "full_name": "Hüftumfang",
        "number": "G09"
    },
    "hip_circ_with_abdomen": {
        "description": "Auf Höhe der Hüfte messen, den Bauch mit berücksichtigen. (Hüftumfang hinten + Vorderer Hüftumfang mit Bauch)",
        "diagram": "Gp9",
        "full_name": "Hüftumfang, inklusive Bauch",
        "number": "G46"
    },
    "hip_with_abdomen_arc_f": {
        "description": "Festes Papier über die Vorderseite des Bauchs legen und an den Seiten festkleben. Über das Papier vorne von Hüftseite zu Hüftseite messen.",
        "diagram": "Gp6",
        "full_name": "Vorderer Hüftumfang mit Bauch",
        "number": "G42"
    },
    "indent_ankle_high": {
        "description": "Horizontale Abstand zwischen einer flachen Linie senkrecht aufgestellt, um Ferse und tiefste Einbuchtung am Knöchel.",
        "diagram": "Cp2",
        "full_name": "Vertiefung: Knöchel",
        "number": "C03"
    },
    "indent_neck_back": {
        "description": "Horizontalen Abstand von Schulterblatt (Ansatzpunkt) bis zum Halswirbel.",
        "diagram": "Cp1",
        "full_name": "Vertiefung: Nacken hinten",
        "number": "C01"
    },
    "indent_waist_back": {
        "description": "Horizontale Abstand zwischen einer flachen Linie senkrecht aufgestellt, um Ferse und tiefste Einbuchtung am Knöchel.",
        "diagram": "Cp2",
        "full_name": "Vertiefung: Taille hinten",
        "number": "C02"
    },
    "leg_ankle_circ": {
        "description": "Umfang des Knöchels da, wo die Vorderseite des Beins den höchsten Teil des Fußes trifft.",
        "diagram": "Mp2",
        "full_name": "Bein: Umfang des Knöchels",
        "number": "M09"
    },
    "leg_ankle_diag_circ": {
        "description": "Der Umfang des Knöchels diagonal von der Oberseite des Fußes zur Unterseite der Ferse.",
        "diagram": "Mp2",
        "full_name": "Bein: Diagonaler Umfang des Knöchels",
        "number": "M11"
    },
    "leg_ankle_high_circ": {
        "description": "Umfang des Knöchels da, wo die Einbuchtung an der Rückseite des Knöchels am stärksten ausgeprägt ist.",
        "diagram": "Mp2",
        "full_name": "Bein: oberer Knöchelumfang",
        "number": "M08"
    },
    "leg_calf_circ": {
        "description": "Wadenumfang an der stärksten Stelle des Unterschenkels.",
        "diagram": "Mp2",
        "full_name": "Bein: Wadenumfang",
        "number": "M07"
    },
    "leg_crotch_to_ankle": {
        "description": "Vom Schritt zum Knöchel messen. ('Bein: Schritt zum Boden' - 'Höhe: Knöchel')",
        "diagram": "Mp3",
        "full_name": "Bein: Schritt zum Knöchel",
        "number": "M12"
    },
    "leg_crotch_to_floor": {
        "description": "Füße eng zusammen stellen. Von der Schritthöhe (Körper berühren, kein Abstand) bis zum Boden messen.",
        "diagram": "Mp1",
        "full_name": "Beine: Vom Schritt bis Boden",
        "number": "M01"
    },
    "leg_knee_circ": {
        "description": "Knie Umfang mit geradem Bein.",
        "diagram": "Mp2",
        "full_name": "Bein: Knieumfang",
        "number": "M05"
    },
    "leg_knee_circ_bent": {
        "description": "Knieumfang mit abgewinkeltem Bein.",
        "diagram": "Mp2",
        "full_name": "Bein: Umfang des gewinkelten Knies",
        "number": "M10"
    },
    "leg_knee_small_circ": {
        "description": "Beinumfang knapp unter dem Knie.",
        "diagram": "Mp2",
        "full_name": "Bein: kleinster Knieumfang",
        "number": "M06"
    },
    "leg_thigh_mid_circ": {
        "description": "Oberschenkelumfang etwa auf halbem Weg zwischen Schritt und Knie.",
        "diagram": "Mp2",
        "full_name": "Bein: Oberschenkel-Mittelumfang",
        "number": "M04"
    },
    "leg_thigh_upper_circ": {
        "description": "Oberschenkelumfang an der breitesten Stelle des Oberschenkels in der Nähe des Schrittes.",
        "diagram": "Mp2",
        "full_name": "Bein: Oberschenkel-Umfang",
        "number": "M03"
    },
    "leg_waist_side_to_ankle": {
        "description": "Von der Taillenseite zum Knöchel messen. ('Bein: Taillenseite zum Boden' - 'Höhe: Knöchel')",
        "diagram": "Mp3",
        "full_name": "Bein: Taillenseite zum Knöchel",
        "number": "M13"
    },
    "leg_waist_side_to_floor": {
        "description": "Von der Taille entlang der Seite und Hüftkurve, ab Hüfthöhe gerade nach unten bis zum Boden .",
        "diagram": "Mp1",
        "full_name": "Bein: Taille an der Seite bis zum Boden",
        "number": "M02"
    },
    "leg_waist_side_to_knee": {
        "description": "Von der Taille entlang der Seite und Hüftkurve, ab Hüfthöhe gerade nach unten bis zum Knie. ('Bein: Taillenseite zum Boden' - 'Höhe: Knie')",
        "diagram": "Mp3",
        "full_name": "Bein: Seitliche Taille zu den Knien",
        "number": "M14"
    },
    "lowbust_arc_b": {
        "description": "Von Unterbrustseite bis Unterbrustseite über den Rücken.  ('Unterbrustumfang' - 'Unterbrustumfang, vorne').",
        "diagram": "Gp4",
        "full_name": "Unterbrustumfang, hinten",
        "number": "G29"
    },
    "lowbust_arc_f": {
        "description": "Von Unterbrustseite bis Unterbrustseite über die Vorderseite.",
        "diagram": "Gp2",
        "full_name": "Unterbrustumfang_vorne",
        "number": "G13"
    },
    "lowbust_arc_half_b": {
        "description": "Die Hälfte von 'Hoher Brustumfang, hinten'. ('Hoher Brustumfang, hinten' / 2).",
        "diagram": "Gp5",
        "full_name": "Die Hälfte des hinteren hohen Brustumfanges",
        "number": "G37"
    },
    "lowbust_arc_half_f": {
        "description": "Die Hälfte des vorderen Unterbrustumfanges. ('Vorderer Unterbrustumfang'/2).",
        "diagram": "Gp3",
        "full_name": "Hälfte Unterbrustumfang, vorne",
        "number": "G21"
    },
    "lowbust_circ": {
        "description": "Unterbrustumfang, unter den Brüsten, parallel zum Boden gemessen.",
        "diagram": "Gp1",
        "full_name": "Unterbrustumfang",
        "number": "G05"
    },
    "lowbust_to_waist_b": {
        "description": "Auf dem Rücken von der Unterbrusthöhe zur Taillenhöhe messen.",
        "diagram": "Hp7",
        "full_name": "Unterbrust hinten zur Taille hinten",
        "number": "H25"
    },
    "lowbust_to_waist_f": {
        "description": "Von der vorderen Unterbrust zur Taille messen.",
        "diagram": "Hp4",
        "full_name": "Unterbrust vorne zur Taille vorne",
        "number": "H11"
    },
    "neck_arc_b": {
        "description": "Von Halsseite zur anderen Halsseite über den Rücken gemessen('Halsumfang'-'Halsumfang vorne').",
        "diagram": "Gp4",
        "full_name": "Hinterer Halsumfang",
        "number": "G26"
    },
    "neck_arc_f": {
        "description": "Von Halsseite zur Halsseite über den vorderen Halsansatz.",
        "diagram": "Gp2",
        "full_name": "Halsumfang vorne",
        "number": "G10"
    },
    "neck_arc_half_b": {
        "description": "Die Hälfte des hinteren Halsumfanges. ('Halsumfang hinten'/2')",
        "diagram": "Gp5",
        "full_name": "Hälfte des hinteren Halsumfanges",
        "number": "G34"
    },
    "neck_arc_half_f": {
        "description": "Die Hälfte des vorderen Halsumfanges. ('Halsumfang vorne'/2')",
        "diagram": "Gp3",
        "full_name": "Hälfte des vorderen Halsumfanges",
        "number": "G18"
    },
    "neck_back_to_across_back": {
        "description": "Vom Nacken runter zur Höhe von 'Über den Rücken gemessen' messen.",
        "diagram": "Hp13",
        "full_name": "Nacken zu 'Über den Rücken gemessen'",
        "number": "H41"
    },
    "neck_back_to_armfold_front": {
        "description": "Vom Nacken über die Schulter bis zur Vorderseite der Achsel messen.",
        "diagram": "Pp2",
        "full_name": "Nacken zur Achsel vorne",
        "number": "P02"
    },
    "neck_back_to_armfold_front_to_highbust_back": {
        "description": "Vom Nacken über die Schulter bis zur Vorderseite der Achsel, dann unter dem Arm durch auf dem Rücken bis zur Oberbrusthöhe messen.",
        "diagram": "Pp8",
        "full_name": "Nacken zur Achsel vorne zur Oberbrust hinten",
        "number": "P08"
    },
    "neck_back_to_armfold_front_to_neck_back": {
        "description": "Vom Nacken über die Schulter bis zur Vorderseite der Achsel, dann unter dem Arm durch zurück zum Nacken messen.",
        "diagram": "Pp6",
        "full_name": "Nacken zur Achsel vorne zum Nacken",
        "number": "P06"
    },
    "neck_back_to_armfold_front_to_waist_side": {
        "description": "Vom Nacken über die Schulter zur Taillenseite messen.",
        "diagram": "Pp3",
        "full_name": "Nacken zur Taillenseite über Schulter",
        "number": "P03"
    },
    "neck_back_to_bust_b": {
        "description": "Vom Nacken zur Höhe der Brust auf dem Rücken messen.",
        "diagram": "Hp7",
        "full_name": "Nacken zum Rücken auf Brusthöhe",
        "number": "H23"
    },
    "neck_back_to_bust_front": {
        "description": "Vom Nacken über die Schulter bis zur Vorderseite der Brust messen.",
        "diagram": "Pp1",
        "full_name": "Hals hinten zur Brust vorne",
        "number": "P01"
    },
    "neck_back_to_highbust_b": {
        "description": "Vom Nacken zur Höhe der Oberbrust auf dem Rücken messen.",
        "diagram": "Hp7",
        "full_name": "Nacken zur Oberbrust hinten",
        "number": "H21"
    },
    "neck_back_to_shoulder_tip_b": {
        "description": "Vom Nacken zur Schulterspitze messen.",
        "diagram": "Ip6",
        "full_name": "Nacken zur Schulterspitze",
        "number": "I13"
    },
    "neck_back_to_waist_b": {
        "description": "Vom Nacken hinten runter bis Taille hinten",
        "diagram": "Hp6",
        "full_name": "Nacken hinten bis Taille hinten",
        "number": "H19"
    },
    "neck_back_to_waist_front": {
        "description": "Vom Nacken über die Seite des Halses runter zur Vorderseite der Taille messen.",
        "diagram": "Op1",
        "full_name": "Nacken zur Taille vorne",
        "number": "O01"
    },
    "neck_back_to_waist_side": {
        "description": "Vom Nacken diagonal über den Rücken zur Taillenseite messen.",
        "diagram": "Kp5",
        "full_name": "Nacken zur Taillenseite",
        "number": "K06"
    },
    "neck_circ": {
        "description": "Halsumfang am Halsansatz, berührend den Hals hinten, Halsseiten und vorderen Halsansatz.",
        "diagram": "Gp1",
        "full_name": "Halsumfang",
        "number": "G02"
    },
    "neck_front_to_bust_f": {
        "description": "Von der Vorderseite des Halses zur Brust messen. Dabei den Raum zwischen den Brustpunkten mit Band abdecken.",
        "diagram": "Hp4",
        "full_name": "Hals vorne zur Brust vorne",
        "number": "H09"
    },
    "neck_front_to_highbust_f": {
        "description": "Von der Vorderseite des Halses bis zur Oberbrust messen.",
        "diagram": "Hp4",
        "full_name": "Hals vorne zur Oberbrust vorne",
        "number": "H07"
    },
    "neck_front_to_shoulder_tip_f": {
        "description": "Von der Vorderseite des Halses bis zur Schulterspitze messen.",
        "diagram": "Ip5",
        "full_name": "Hals vorne zur Schulterspitze",
        "number": "I12"
    },
    "neck_front_to_waist_f": {
        "description": "Von der Vorderseite des Nackens über ein Band über den Brustpunkten runter zur Taille messen.",
        "diagram": "Hp1",
        "full_name": "Hals vorne zu Taille vorne",
        "number": "H01"
    },
    "neck_front_to_waist_flat_f": {
        "description": "Von der Vorderseite des Nackens zwischen den Brüsten runter zur Taille messen.",
        "diagram": "Hp2",
        "full_name": "Hals vorne zur Taille vorne, flach",
        "number": "H02"
    },
    "neck_front_to_waist_side": {
        "description": "Von der Vorderseite des Halses diagonal zur Seite der Taille messen.",
        "diagram": "Kp2",
        "full_name": "Hals vorne zur Taillenseite",
        "number": "K02"
    },
    "neck_mid_circ": {
        "description": "Halsumfang, auf halbem Weg zwischen Kiefer und Oberkörper.",
        "diagram": "Gp1",
        "full_name": "Halsumfang, in der Mitte",
        "number": "G01"
    },
    "neck_side_to_armfold_b": {
        "description": "Von der Halsseite diagonal zur Rückseite der Achsel messen.",
        "diagram": "Kp9",
        "full_name": "Halsseite zur Achsel, hinten",
        "number": "K11"
    },
    "neck_side_to_armfold_f": {
        "description": "Von der Halsseite diagonal zur Vorderseite der Achsel messen.",
        "diagram": "Kp6",
        "full_name": "Halsseite zur Achsel vorne",
        "number": "K08"
    },
    "neck_side_to_armpit_b": {
        "description": "Von der Seite des Halses diagonal über den Rücken zur Oberbrustseite (Achsel) messen.",
        "diagram": "Kp10",
        "full_name": "Halsseite zur Oberbrustseite, hinten",
        "number": "K12"
    },
    "neck_side_to_armpit_f": {
        "description": "Von der Seite des Halses diagonal über die Brust zur Oberbrustseite (Achsel) messen.",
        "diagram": "Kp7",
        "full_name": "Halsseite zur Oberbrustseite, vorne",
        "number": "K09"
    },
    "neck_side_to_bust_b": {
        "description": "Von der Seite des Halses gerade runter zur Brust messen.",
        "diagram": "Hp8",
        "full_name": "Halsseite zur Brust, hinten",
        "number": "H27"
    },
    "neck_side_to_bust_f": {
        "description": "Von der Halsseite gerade runter zur Vorderseite der Brust messen.",
        "diagram": "Hp5",
        "full_name": "Halsseite zur Brust, vorne",
        "number": "H14"
    },
    "neck_side_to_bust_side_b": {
        "description": "Von der Seite des Halses schräg über den Rücken zur Brustseite messen.",
        "diagram": "Kp11",
        "full_name": "Halsseite zur Brustseite, hinten",
        "number": "K13"
    },
    "neck_side_to_bust_side_f": {
        "description": "Von der Seite des Halses schräg über die Brust zur Brustseite messen.",
        "diagram": "Kp8",
        "full_name": "Halsseite zur Brustseite, vorne",
        "number": "K10"
    },
    "neck_side_to_highbust_b": {
        "description": "Von der Halsseite gerade den Rücken herunter bis zur Höhe der Oberbrust messen.",
        "diagram": "Hp8",
        "full_name": "Halsseite zur Oberbrusthöhe, hinten",
        "number": "H28"
    },
    "neck_side_to_highbust_f": {
        "description": "Von der Halsseite gerade runter zur Oberbrust vorne messen.",
        "diagram": "Hp5",
        "full_name": "Halsseite zur Oberbrust, vorne",
        "number": "H15"
    },
    "neck_side_to_waist_b": {
        "description": "Von der Halsseite gerade herunter zur Taille messen.",
        "diagram": "Hp6",
        "full_name": "Halsseite zur Taillenhöhe, hinten",
        "number": "H18"
    },
    "neck_side_to_waist_bustpoint_f": {
        "description": "In einer geraden Linie von der Seite des Halses über den Brustpunkt zur Taille messen.",
        "diagram": "Hp3",
        "full_name": "Von der Seite des Halses über den Brustpunkt zur Taille",
        "number": "H06"
    },
    "neck_side_to_waist_f": {
        "description": "Von der Halsseite gerade runter zur Taillenhöhe messen.",
        "diagram": "Hp3",
        "full_name": "Halsseite zur Taillenhöhe vorne",
        "number": "H05"
    },
    "neck_side_to_waist_scapula_b": {
        "description": "In einer geraden Linie von der Halsseite über das Schulterblatt herunter zur Taille messen.",
        "diagram": "Hp6",
        "full_name": "Halsseite zur Taille, über das Schulterblatt",
        "number": "H20"
    },
    "neck_side_to_waist_side_b": {
        "description": "Von der Halsseite diagonal über den Rücken zur Taillenseite messen.",
        "diagram": "Kp5",
        "full_name": "Halsseite zur Taillenseite, hinten",
        "number": "K07"
    },
    "neck_side_to_waist_side_f": {
        "description": "Von der Seite des Halses diagonal über die Brust zur Taillenseite messen.",
        "diagram": "Kp2",
        "full_name": "Halsseite zur Taillenseite, vorne",
        "number": "K03"
    },
    "neck_width": {
        "description": "Zwischen den Enden einer ungeschlossenen Kette, die um den Hals gehängt wurde, messen.",
        "diagram": "Ip7",
        "full_name": "Halsbreite",
        "number": "I14"
    },
    "rib_arc_b": {
        "description": "Von Rippenseite zur Rippenseite, gemessen über den Rücken. ('Rippenumfang' - 'Rippenumfang, vorne').",
        "diagram": "Gp4",
        "full_name": "Rippenumfang, hinten",
        "number": "G30"
    },
    "rib_arc_f": {
        "description": "Von Rippenseite zur Rippenseite, gemessen über die Front.",
        "diagram": "Gp2",
        "full_name": "Rippenumfang vorne",
        "number": "G14"
    },
    "rib_arc_half_b": {
        "description": "Die Hälfte von 'Rippenumfang, hinten'. ('Rippenumfang, hinten' / 2).",
        "diagram": "Gp5",
        "full_name": "Die Hälfte des hinteren Rippenumfanges",
        "number": "G38"
    },
    "rib_arc_half_f": {
        "description": "Die Hälfte des vorderen Rippenumfang. ('Rippenumfang vorne'/2).",
        "diagram": "Gp3",
        "full_name": "Hälfte Rippenumfang vorne",
        "number": "G22"
    },
    "rib_circ": {
        "description": "Umfang der Rippen auf der Ebene der untersten Rippe an der Seite, parallel zum Boden gemessen.",
        "diagram": "Gp1",
        "full_name": "Umfang der Rippen",
        "number": "G06"
    },
    "rib_to_waist_side": {
        "description": "Von der tiefsten Rippe auf der Seite runter zur Taillenseite messen.",
        "diagram": "Hp4",
        "full_name": "Rippenseite zur Taillenseite",
        "number": "H12"
    },
    "rise_length_b": {
        "description": "Senkrechter Abstand von der Taillenrückseite zur Schritthöhe. ('Höhe: Taille hinten' - 'Bein: Schritt zum Boden')",
        "diagram": "Np4",
        "full_name": "Rise length, back",
        "number": "N06"
    },
    "rise_length_diag": {
        "description": "Measure from Waist Side diagonally to a string tied at the top of the leg, seated on a hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, diagonal",
        "number": "N05"
    },
    "rise_length_f": {
        "description": "Senkrechter Abstand von der Taille vorne zur Schritthöhe. ('Höhe: Taille vorne' - 'Bein: Schritt zum Boden')",
        "diagram": "Np4",
        "full_name": "Rise length, front",
        "number": "N07"
    },
    "rise_length_side": {
        "description": "Vertikaler Abstand von der Taillenseite zur Schritthöhe. ('Höhe: Taillenseite' - 'Bein: Schritt zum Boden')",
        "diagram": "Np5",
        "full_name": "Rise length, side",
        "number": "N08"
    },
    "rise_length_side_sitting": {
        "description": "Beim Sitzen auf einer festen Oberfläche von der Taillenseite über die Kurve der Hüfte zur Oberfläche messen.",
        "diagram": "Np3",
        "full_name": "Rise length, side, sitting",
        "number": "N04"
    },
    "shoulder_center_to_highbust_b": {
        "description": "Von der Schulterhöhe hinten zwischen den Schulterblättern zur Höhe der Oberbrust messen.",
        "diagram": "Hp8",
        "full_name": "Schultermitte zur Oberbrusthöhe, hinten",
        "number": "H29"
    },
    "shoulder_center_to_highbust_f": {
        "description": "Von der Schulterhöhe vorne zur Höhe der Oberbrust messen.",
        "diagram": "Hp5",
        "full_name": "Schultermitte zur Oberbrusthöhe, vorne",
        "number": "H16"
    },
    "shoulder_length": {
        "description": "Von der Seite des Halses bis zur Schulterspitze messen.",
        "diagram": "Ip1",
        "full_name": "Schulterbreite",
        "number": "I01"
    },
    "shoulder_slope_neck_back_angle": {
        "description": "Der Winkel der Linie vom Nacken zur Schulterspitze und der Linie vom Nacken zum Boden.",
        "diagram": "Hp11",
        "full_name": "Neigungswinkel der Schulter ab dem Nacken",
        "number": "H38"
    },
    "shoulder_slope_neck_back_height": {
        "description": "Vertikaler Abstand zwischen dem Nacken und der Schulterspitze.",
        "diagram": "Hp11",
        "full_name": "Länge der Schulterneigung vom Nacken",
        "number": "H39"
    },
    "shoulder_slope_neck_side_angle": {
        "description": "Der Winkel der Linie von der Halsseite zur Schulterspitze und der Linie von der Halsseite zum Boden.",
        "diagram": "Hp11",
        "full_name": "Neigungswinkel der Schulter ab der Halsseite",
        "number": "H36"
    },
    "shoulder_slope_neck_side_length": {
        "description": "Vertikaler Abstand zwischen der Halsseite und der Schulterspitze.",
        "diagram": "Hp11",
        "full_name": "Länge der Schulterneigung ab der Halsseite",
        "number": "H37"
    },
    "shoulder_slope_shoulder_tip_angle": {
        "description": "Der Winkel der Linie von der Halsseite zur Schulterspitze und einer vertikalen Linie ab der Schulterspitze.",
        "diagram": "Hp12",
        "full_name": "Neigungswinkel der Schulter ab der Schulterspitze",
        "number": "H40"
    },
    "shoulder_tip_to_armfold_b": {
        "description": "Von der Schulterspitze um das Ärmelloch runter zur Achsel messen.",
        "diagram": "Hp8",
        "full_name": "Schulterspitze zur Achsel hinten",
        "number": "H26"
    },
    "shoulder_tip_to_armfold_f": {
        "description": "Von der Schulterspitze um das Ärmelloch runter zur Achsel messen.",
        "diagram": "Hp5",
        "full_name": "Schulterspitze zur Achsel, vorne",
        "number": "H13"
    },
    "shoulder_tip_to_shoulder_tip_b": {
        "description": "Horizontale Abmessung von Schulterspitze zu Schulterspitze, am Rücken.",
        "diagram": "Ip3",
        "full_name": "Horizontale Abmessung von Schulterspitze zu Schulterspitze, am Rücken",
        "number": "I07"
    },
    "shoulder_tip_to_shoulder_tip_f": {
        "description": "Von der einen Schulterspitze zur anderen auf der Vorderseite messen.",
        "diagram": "Ip1",
        "full_name": "Schulterspitze zur Schulterspitze, vorne",
        "number": "I02"
    },
    "shoulder_tip_to_shoulder_tip_half_b": {
        "description": "Die Hälfte von 'Schulterspitze zu Schulterspitze, hinten'. ('Schulterspitze zu Schulterspitze, hinten' / 2)",
        "diagram": "Ip4",
        "full_name": "Hälfte der horizontalen Abmessung von Schulterspitze zu Schulterspitze am Rücken",
        "number": "I10"
    },
    "shoulder_tip_to_shoulder_tip_half_f": {
        "description": "Die Hälfte von 'Schulterspitze zur Schulterspitze, vorne'.",
        "diagram": "Ip2",
        "full_name": "Hälfte der horizontalen Abmessung von Schulterspitze zu Schulterspitze vorne",
        "number": "I05"
    },
    "shoulder_tip_to_waist_b_1in_offset": {
        "description": "Einen Zoll (2,54 cm) nach außen auf Taillenhöhe von der Rückseite der Taille aus markieren. Von der Schulterspitze diagonal zu dieser Markierung messen.",
        "diagram": "Kp4",
        "full_name": "Schulterspitze zur Rückseite der Taille mit 1 Zoll (2,54 cm) Abweichung",
        "number": "K05"
    },
    "shoulder_tip_to_waist_back": {
        "description": "Von der Schulterspitze diagonal zur Rückseite der Taille messen.",
        "diagram": "Kp3",
        "full_name": "Schulterspitze zur Rückseite der Taille",
        "number": "K04"
    },
    "shoulder_tip_to_waist_front": {
        "description": "Von der Schulterspitze diagonal zur Vorderseite der Taille messen.",
        "diagram": "Kp1",
        "full_name": "Schulterspitze zur Taille vorne",
        "number": "K01"
    },
    "shoulder_tip_to_waist_side_b": {
        "description": "Von der Schulterspitze über den Rücken in einer Kurve um das Ärmelloch runter zur Taillenseite messen.",
        "diagram": "Hp6",
        "full_name": "Schulterspitze zur Taillenseite, hinten",
        "number": "H17"
    },
    "shoulder_tip_to_waist_side_f": {
        "description": "Von der Schulterspitze um die Vorderseite des Ärmellochs runter zur Taillenseite messen.",
        "diagram": "Hp3",
        "full_name": "Schulterspitze zur Taillenseite, vorne",
        "number": "H04"
    },
    "waist_arc_b": {
        "description": "Von Taillenseite zu Taillenseite über den Rücken. ('Taillenumfang' - 'Taillenumfang, vorne').",
        "diagram": "Gp4",
        "full_name": "Taillenumfang, hinten",
        "number": "G31"
    },
    "waist_arc_f": {
        "description": "Von Taillenseite zu Taillenseite über die Front",
        "diagram": "Gp2",
        "full_name": "Taillenumfang vorne",
        "number": "G15"
    },
    "waist_arc_half_b": {
        "description": "Die Hälfte von 'hinterer Taillenumfang'. ('hinterer Taillenumfang' / 2).",
        "diagram": "Gp5",
        "full_name": "Die Hälfte des hinteren Taillenumfanges",
        "number": "G39"
    },
    "waist_arc_half_f": {
        "description": "Die Hälfte des vorderen Taillenumfang. ('Taillenumfang vorne'/2).",
        "diagram": "Gp3",
        "full_name": "Hälfte Taillenumfang vorne",
        "number": "G23"
    },
    "waist_circ": {
        "description": "Umfang der Taille, folgen der natürlichen Form. Taillen sind in der Regel auf der Rückseite höher",
        "diagram": "Gp1",
        "full_name": "Taillenumfang",
        "number": "G07"
    },
    "waist_natural_arc_b": {
        "description": "Von Seite zu Seite auf der Höhe der natürlichen Taille über dem Rücken messen. ('Umfang natürliche Taille' - 'Bogen der natürlichen Taille, vorne')",
        "diagram": "Op5",
        "full_name": "Bogen der natürlichen Taille, hinten",
        "number": "O05"
    },
    "waist_natural_arc_f": {
        "description": "Von Seite zu Seite auf der Höhe der natürlichen Taille über dem Bauch messen.",
        "diagram": "Op4",
        "full_name": "Bogen der natürlichen Taille, vorne",
        "number": "O04"
    },
    "waist_natural_circ": {
        "description": "Rumpfumfang auf der Höhe der natürlichen Einbuchtung beim äußeren schrägen Bauchmuskel (bei Männern). Wenn die EInbuchtung nicht gefunden werden kann, misst man direkt unter dem Nabel.",
        "diagram": "Op3",
        "full_name": "Umfang der natürlichen Taille",
        "number": "O03"
    },
    "waist_to_highhip_b": {
        "description": "Auf dem Rücken von der Taillenhöhe zur Höhe der hohen Hüfte messen.",
        "diagram": "Hp10",
        "full_name": "Taille hinten zur hohen Hüfte hinten",
        "number": "H33"
    },
    "waist_to_highhip_f": {
        "description": "Von der Vroderseite der Taille zur Vorderseite der hohen Hüfte messen.",
        "diagram": "Hp9",
        "full_name": "Taille vorne zur hohen Hüfte vorne",
        "number": "H30"
    },
    "waist_to_highhip_side": {
        "description": "Von der Seite der Taille zur Seite der hohen Hüfte messen.",
        "diagram": "Hp9",
        "full_name": "Taillenseite zur Seite der hohen Hüfte",
        "number": "H32"
    },
    "waist_to_hip_b": {
        "description": "Auf dem Rücken von der Taillenhöhe zur Hüfthöhe messen. Bei Bedarf den Po mit Band überdecken.",
        "diagram": "Hp10",
        "full_name": "Taille hinten zur Hüfte hinten",
        "number": "H34"
    },
    "waist_to_hip_f": {
        "description": "Von der Vorderseite der Taille zur Vorderseite der Hüfte messen.",
        "diagram": "Hp9",
        "full_name": "Taille vorne zur Hüfte vorne",
        "number": "H31"
    },
    "waist_to_hip_side": {
        "description": "Von der Taillenseite zur Hüftseite messen.",
        "diagram": "Hp10",
        "full_name": "Taillenseite zur Hüftseite",
        "number": "H35"
    },
    "waist_to_natural_waist_b": {
        "description": "Von der Rückseite der Taille zur Rückseite der natürlichen Taille messen.",
        "diagram": "Op7",
        "full_name": "Taille zur natürlichen Taille, hinten",
        "number": "O07"
    },
    "waist_to_natural_waist_f": {
        "description": "Von der Vorderseite der Taille zur Vorderseite der natürlichen Taille messen.",
        "diagram": "Op6",
        "full_name": "Taillen zur natürlichen Taille, vorne",
        "number": "O06"
    },
    "waist_to_waist_halter": {
        "description": "Von der Taillenhöhe um den Nacken zurück zur Taille messen.",
        "diagram": "Op2",
        "full_name": "Von Taille über Nacken zur Taille",
        "number": "O02"
    },
    "width_abdomen_to_hip": {
        "description": "Horizontaler Abstand von der stärksten Stelle des vorderen Bauches bis zur stärksten Stelle des größten Hüftumfanges.",
        "diagram": "Bp2",
        "full_name": "Breite: Bauch zu der Hüfte",
        "number": "B05"
    },
    "width_bust": {
        "description": "Horizontale Abmessung von der Brustseite zur Brustseite.",
        "diagram": "Bp1",
        "full_name": "Breite: Brust",
        "number": "B02"
    },
    "width_hip": {
        "description": "Horizontale Abmessung von der einen Seite der Hüfte zur anderen Seite.",
        "diagram": "Bp1",
        "full_name": "Breite: Hüfte",
        "number": "B04"
    },
    "width_shoulder": {
        "description": "Horizontale Abmessung von Schulterspitze zu Schulterspitze",
        "diagram": "Bp1",
        "full_name": "Breite: Schulter",
        "number": "B01"
    },
    "width_waist": {
        "description": "Horizontale Abmessung von der einen Seite der Taillie zur anderen Seite.",
        "diagram": "Bp1",
        "full_name": "Breite: Taillie",
        "number": "B03"
    }
}
