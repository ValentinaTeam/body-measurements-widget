module.exports = {
    "across_back_b": {
        "description": "From Armscye to Armscye at the narrowest width of the back.",
        "diagram": "Ip3",
        "full_name": "Across Back",
        "number": "I08"
    },
    "across_back_center_to_armfold_front_to_across_back_center": {
        "description": "From center of Across Back, over Shoulder, under Arm, and return to start.",
        "diagram": "Pp7",
        "full_name": "Across Back Center, circled around Shoulder",
        "number": "P07"
    },
    "across_back_half_b": {
        "description": "Half of 'Across Back'. ('Across Back' / 2).",
        "diagram": "Ip4",
        "full_name": "Across Back, half",
        "number": "I11"
    },
    "across_back_to_waist_b": {
        "description": "From middle of Across Back down to Waist back.",
        "diagram": "Hp13",
        "full_name": "Across Back to Waist back",
        "number": "H42"
    },
    "across_chest_f": {
        "description": "From Armscye to Armscye at narrowest width across chest.",
        "diagram": "Ip1",
        "full_name": "Across Chest",
        "number": "I03"
    },
    "across_chest_half_f": {
        "description": "Half of 'Across Chest'. ('Across Chest' / 2).",
        "diagram": "Ip2",
        "full_name": "Across Chest, half",
        "number": "I06"
    },
    "arm_above_elbow_circ": {
        "description": "Arm circumference at Bicep level.",
        "diagram": "Lp4",
        "full_name": "Arm: Above Elbow circumference",
        "number": "L12"
    },
    "arm_across_back_center_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Middle of Back to Elbow Tip.",
        "diagram": "Op10",
        "full_name": "Arm: Across Back Center to Elbow, high bend",
        "number": "O12"
    },
    "arm_across_back_center_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Middle of Back to Elbow Tip to Wrist bone.",
        "diagram": "Op10",
        "full_name": "Arm: Across Back Center to Wrist, high bend",
        "number": "O13"
    },
    "arm_armpit_to_elbow": {
        "description": "From Armpit to inner Elbow, arm straight.",
        "diagram": "Lp3",
        "full_name": "Arm: Armpit to Elbow, inside",
        "number": "L09"
    },
    "arm_armpit_to_wrist": {
        "description": "From Armpit to ulna Wrist bone, arm straight.",
        "diagram": "Lp3",
        "full_name": "Arm: Armpit to Wrist, inside",
        "number": "L08"
    },
    "arm_armscye_back_center_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Armscye Back to Elbow Tip.",
        "diagram": "Op11",
        "full_name": "Arm: Armscye Back Center to Wrist, high bend",
        "number": "O14"
    },
    "arm_elbow_circ": {
        "description": "Elbow circumference, arm straight.",
        "diagram": "Lp4",
        "full_name": "Arm: Elbow circumference",
        "number": "L13"
    },
    "arm_elbow_circ_bent": {
        "description": "Elbow circumference, arm is bent.",
        "diagram": "Lp1",
        "full_name": "Arm: Elbow circumference, bent",
        "number": "L04"
    },
    "arm_elbow_to_wrist": {
        "description": "From Elbow to Wrist, arm straight. ('Arm: Shoulder Tip to Wrist' - 'Arm: Shoulder Tip to Elbow').",
        "diagram": "Lp2",
        "full_name": "Arm: Elbow to Wrist",
        "number": "L07"
    },
    "arm_elbow_to_wrist_bent": {
        "description": "Elbow tip to wrist. ('Arm: Shoulder Tip to Wrist, bent' - 'Arm: Shoulder Tip to Elbow, bent').",
        "diagram": "Lp1",
        "full_name": "Arm: Elbow to Wrist, bent",
        "number": "L03"
    },
    "arm_elbow_to_wrist_inside": {
        "description": "From inside Elbow to Wrist. ('Arm: Armpit to Wrist, inside' - 'Arm: Armpit to Elbow, inside').",
        "diagram": "Lp3",
        "full_name": "Arm: Elbow to Wrist, inside",
        "number": "L10"
    },
    "arm_lower_circ": {
        "description": "Arm circumference where lower arm is widest.",
        "diagram": "Lp4",
        "full_name": "Arm: Lower Arm circumference",
        "number": "L14"
    },
    "arm_neck_back_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Back to Elbow Tip.",
        "diagram": "Op8",
        "full_name": "Arm: Neck Back to Elbow, high bend",
        "number": "O08"
    },
    "arm_neck_back_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Back to Elbow Tip to Wrist bone.",
        "diagram": "Op8",
        "full_name": "Arm: Neck Back to Wrist, high bend",
        "number": "O09"
    },
    "arm_neck_side_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Side to Elbow Tip.",
        "diagram": "Op9",
        "full_name": "Arm: Neck Side to Elbow, high bend",
        "number": "O10"
    },
    "arm_neck_side_to_finger_tip": {
        "description": "From Neck Side down arm to tip of middle finger. ('Shoulder Length' + 'Arm: Shoulder Tip to Wrist' + 'Hand: Length').",
        "diagram": "Lp7",
        "full_name": "Arm: Neck Side to Finger Tip",
        "number": "L18"
    },
    "arm_neck_side_to_outer_elbow": {
        "description": "From Neck Side over Shoulder Tip down to Elbow. (Shoulder length + Arm: Shoulder Tip to Elbow).",
        "diagram": "Lp10",
        "full_name": "Arm: Neck side to Elbow",
        "number": "L22"
    },
    "arm_neck_side_to_wrist": {
        "description": "From Neck Side to Wrist. ('Shoulder Length' + 'Arm: Shoulder Tip to Wrist').",
        "diagram": "Lp6",
        "full_name": "Arm: Neck Side to Wrist",
        "number": "L17"
    },
    "arm_neck_side_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Side to Elbow Tip to Wrist bone.",
        "diagram": "Op9",
        "full_name": "Arm: Neck Side to Wrist, high bend",
        "number": "O11"
    },
    "arm_shoulder_tip_to_armfold_line": {
        "description": "From Shoulder Tip down to Armpit level.",
        "diagram": "Lp5",
        "full_name": "Arm: Shoulder Tip to Armfold line",
        "number": "L16"
    },
    "arm_shoulder_tip_to_elbow": {
        "description": "From Shoulder tip to Elbow Tip, arm straight.",
        "diagram": "Lp2",
        "full_name": "Arm: Shoulder Tip to Elbow",
        "number": "L06"
    },
    "arm_shoulder_tip_to_elbow_bent": {
        "description": "Bend Arm, measure from Shoulder Tip to Elbow Tip.",
        "diagram": "Lp1",
        "full_name": "Arm: Shoulder Tip to Elbow, bent",
        "number": "L02"
    },
    "arm_shoulder_tip_to_wrist": {
        "description": "From Shoulder Tip to Wrist bone, arm straight.",
        "diagram": "Lp2",
        "full_name": "Arm: Shoulder Tip to Wrist",
        "number": "L05"
    },
    "arm_shoulder_tip_to_wrist_bent": {
        "description": "Bend Arm, measure from Shoulder Tip around Elbow to radial Wrist bone.",
        "diagram": "Lp1",
        "full_name": "Arm: Shoulder Tip to Wrist, bent",
        "number": "L01"
    },
    "arm_upper_circ": {
        "description": "Arm circumference at Armpit level.",
        "diagram": "Lp4",
        "full_name": "Arm: Upper Arm circumference",
        "number": "L11"
    },
    "arm_wrist_circ": {
        "description": "Wrist circumference.",
        "diagram": "Lp4",
        "full_name": "Arm: Wrist circumference",
        "number": "L15"
    },
    "armfold_to_armfold_b": {
        "description": "From Armfold to Armfold across the back.",
        "diagram": "Ip3",
        "full_name": "Armfold to Armfold, back",
        "number": "I09"
    },
    "armfold_to_armfold_bust": {
        "description": "Measure in a curve from Armfold Left Front through Bust Front curved back up to Armfold Right Front.",
        "diagram": "Pp9",
        "full_name": "Armfold to Armfold, front, curved through Bust Front",
        "number": "P09"
    },
    "armfold_to_armfold_f": {
        "description": "From Armfold to Armfold, shortest distance between Armfolds, not parallel to floor.",
        "diagram": "Ip1",
        "full_name": "Armfold to Armfold, front",
        "number": "I04"
    },
    "armfold_to_bust_front": {
        "description": "Measure from Armfold Front to Bust Front, shortest distance between the two, as straight as possible.",
        "diagram": "Pp10",
        "full_name": "Armfold to Bust Front",
        "number": "P10"
    },
    "armpit_to_waist_side": {
        "description": "Van oksel naar beneden naar zijkant taille.",
        "diagram": "Hp3",
        "full_name": "Oksel naar zijkant TAille",
        "number": "H03"
    },
    "armscye_arc": {
        "description": "From Armscye at Across Chest over ShoulderTip  to Armscye at Across Back.",
        "diagram": "Pp12",
        "full_name": "Armscye: Arc",
        "number": "P12"
    },
    "armscye_circ": {
        "description": "Let arm hang at side. Measure Armscye circumference through Shoulder Tip and Armpit.",
        "diagram": "Lp8",
        "full_name": "Armscye: Circumference",
        "number": "L19"
    },
    "armscye_length": {
        "description": "Vertical distance from Shoulder Tip to Armpit.",
        "diagram": "Lp8",
        "full_name": "Armscye: Length",
        "number": "L20"
    },
    "armscye_width": {
        "description": "Horizontal distance between Armscye Front and Armscye Back.",
        "diagram": "Lp9",
        "full_name": "Armscye: Width",
        "number": "L21"
    },
    "body_armfold_circ": {
        "description": "Meten rond armen en torso op armvouw level.",
        "diagram": "Gp7",
        "full_name": "Romp omtrek op armvouw level",
        "number": "G43"
    },
    "body_bust_circ": {
        "description": "Meten rond armen en torso op buste level.",
        "diagram": "Gp7",
        "full_name": "Romp omtrek op buste level",
        "number": "G44"
    },
    "body_torso_circ": {
        "description": "Omtrek rond torso van midden schouder via kruis en weer terug naar midden schouder",
        "diagram": "Gp8",
        "full_name": "Romp omtrek van volledige torso",
        "number": "G45"
    },
    "bust_arc_b": {
        "description": "Van zijkant buste naar zijkant buste via rug. ('Buste omtrek' - 'Busteronding, voorkant').",
        "diagram": "Gp4",
        "full_name": "Busteronding, rug",
        "number": "G28"
    },
    "bust_arc_f": {
        "description": "Van zijkant buste naar zijkant buste via borstkas.",
        "diagram": "Gp2",
        "full_name": "Buste ronding, voorkant",
        "number": "G12"
    },
    "bust_arc_half_b": {
        "description": " Helft van 'busteronding, rug'. ('Busteronding, rug' / 2).",
        "diagram": "Gp5",
        "full_name": "Halve busteronding, rug",
        "number": "G36"
    },
    "bust_arc_half_f": {
        "description": "Helft van 'Busteronding, voorkant'. ( Busteronding, voorkant' / 2).",
        "diagram": "Gp3",
        "full_name": "Busteronding, voorkant, helft",
        "number": "G20"
    },
    "bust_circ": {
        "description": "Omtrek rondom de borst, parallel aan de vloer.",
        "diagram": "Gp1",
        "full_name": "Borst Omtrek",
        "number": "G04"
    },
    "bust_to_waist_b": {
        "description": "From Bust Back down to Waist level. ('Neck Back to Waist Back' - 'Neck Back to Bust Back').",
        "diagram": "Hp7",
        "full_name": "Bust Back to Waist Back",
        "number": "H24"
    },
    "bust_to_waist_f": {
        "description": "Van voorkant buste naar beneden naar taille level. ('voorkant nek naar voorkant taille' - 'Voorkant nek naar voorkant buste').",
        "diagram": "Hp4",
        "full_name": "Voorkant buste naar voorkant taille",
        "number": "H10"
    },
    "bustpoint_neck_side_to_waist": {
        "description": "From Neck Side to Bustpoint, then straight down to Waist level. ('Neck Side to Bustpoint' + 'Bustpoint to Waist level').",
        "diagram": "Jp3",
        "full_name": "Bustpoint, Neck Side to Waist level",
        "number": "J06"
    },
    "bustpoint_to_bustpoint": {
        "description": "From Bustpoint to Bustpoint.",
        "diagram": "Jp1",
        "full_name": "Bustpoint to Bustpoint",
        "number": "J01"
    },
    "bustpoint_to_bustpoint_half": {
        "description": "Half of 'Bustpoint to Bustpoint'. ('Bustpoint to Bustpoint' / 2).",
        "diagram": "Jp2",
        "full_name": "Bustpoint to Bustpoint, half",
        "number": "J05"
    },
    "bustpoint_to_bustpoint_halter": {
        "description": "From Bustpoint around Neck Back down to other Bustpoint.",
        "diagram": "Jp5",
        "full_name": "Bustpoint to Bustpoint Halter",
        "number": "J09"
    },
    "bustpoint_to_lowbust": {
        "description": "From Bustpoint down to Lowbust level, following curve of bust or chest.",
        "diagram": "Jp1",
        "full_name": "Bustpoint to Lowbust",
        "number": "J03"
    },
    "bustpoint_to_neck_side": {
        "description": "From Neck Side to Bustpoint.",
        "diagram": "Jp1",
        "full_name": "Bustpoint to Neck Side",
        "number": "J02"
    },
    "bustpoint_to_shoulder_center": {
        "description": "From center of Shoulder to Bustpoint.",
        "diagram": "Jp6",
        "full_name": "Bustpoint to Shoulder Center",
        "number": "J10"
    },
    "bustpoint_to_shoulder_tip": {
        "description": "From Bustpoint to Shoulder tip.",
        "diagram": "Jp4",
        "full_name": "Bustpoint to Shoulder Tip",
        "number": "J07"
    },
    "bustpoint_to_waist": {
        "description": "From Bustpoint to straight down to Waist level, forming a straight line (not curving along the body).",
        "diagram": "Jp1",
        "full_name": "Bustpoint to Waist level",
        "number": "J04"
    },
    "bustpoint_to_waist_front": {
        "description": "From Bustpoint to Waist Front, in a straight line, not following the curves of the body.",
        "diagram": "Jp4",
        "full_name": "Bustpoint to Waist Front",
        "number": "J08"
    },
    "crotch_length": {
        "description": "Put tape across gap between buttocks at Hip level. Measure from Waist Front down betwen legs and up to Waist Back.",
        "diagram": "Np1",
        "full_name": "Kruis lengte",
        "number": "N01"
    },
    "crotch_length_b": {
        "description": "Put tape across gap between buttocks at Hip level. Measure from Waist Back to mid-Crotch, either at the vagina or between testicles and anus).",
        "diagram": "Np2",
        "full_name": "Crotch length, back",
        "number": "N02"
    },
    "crotch_length_f": {
        "description": "From Waist Front to start of vagina or end of testicles. ('Crotch length' - 'Crotch length, back').",
        "diagram": "Np2",
        "full_name": "Crotch length, front",
        "number": "N03"
    },
    "dart_width_bust": {
        "description": "This information is pulled from pattern charts in some patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp2",
        "full_name": "Dart Width: Bust",
        "number": "Q02"
    },
    "dart_width_shoulder": {
        "description": "This information is pulled from pattern charts in some patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp1",
        "full_name": "Dart Width: Shoulder",
        "number": "Q01"
    },
    "dart_width_waist": {
        "description": "This information is pulled from pattern charts in some  patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp3",
        "full_name": "Dart Width: Waist",
        "number": "Q03"
    },
    "foot_circ": {
        "description": "Meet de omtrek rond het breedste deel van de voet.",
        "diagram": "Ep2",
        "full_name": "Voet: Omtrek",
        "number": "E03"
    },
    "foot_instep_circ": {
        "description": "Meet de omtrek op het grootste deel van de instap.",
        "diagram": "Ep2",
        "full_name": "Voet: Instap omtrek",
        "number": "E04"
    },
    "foot_length": {
        "description": "Meten vanaf de achterkant van de hiel tot aan het einde van de langste teen",
        "diagram": "Ep2",
        "full_name": "Voet: Lengte",
        "number": "E02"
    },
    "foot_width": {
        "description": "Meten over het breedste gedeelte van de voet.",
        "diagram": "Ep1",
        "full_name": "Voet: Wijdte",
        "number": "E01"
    },
    "hand_circ": {
        "description": "Plooi de duim naar de pink, houd de vingers strak tegen elkaar. Meet de omtrek rond het breedste deel van de hand.",
        "diagram": "Dp3",
        "full_name": "Hand: Omtrek",
        "number": "D05"
    },
    "hand_length": {
        "description": "Lengte vanaf midden pols tot aan het einde van de middelvinger.",
        "diagram": "Dp1",
        "full_name": "Hand: Lengte",
        "number": "D02"
    },
    "hand_palm_circ": {
        "description": "Omtrek van de palm van de hand waar het breedst is.",
        "diagram": "Dp2",
        "full_name": "Hand : Palm omtrek",
        "number": "D04"
    },
    "hand_palm_length": {
        "description": "Lengte vanaf midden pols tot aan basis van de middelvinger.",
        "diagram": "Dp1",
        "full_name": "Hand: Palm lengte",
        "number": "D01"
    },
    "hand_palm_width": {
        "description": "Meten waar de palm het breedst is.",
        "diagram": "Dp1",
        "full_name": "Hand: Palm wijdte",
        "number": "D03"
    },
    "head_chin_to_neck_back": {
        "description": "Verticale afstand van kin naar rugzijde van de nek.('Hoogte' -  'Hoogte: Rug Nek' - 'Hoofd: Lengte')",
        "diagram": "Fp3",
        "full_name": "Hoofd: Kin naar rug nek",
        "number": "F06"
    },
    "head_circ": {
        "description": "Meet de omtrek op het grootste deel van het hoofd.",
        "diagram": "Fp1",
        "full_name": "Hoofd: Omtrek",
        "number": "F01"
    },
    "head_crown_to_neck_back": {
        "description": "Verticale afstand vanaf Kruin naar de rugzijde van de nek. ('Hoogte: Total' - 'Hoogte: Rug nek').",
        "diagram": "Fp3",
        "full_name": "Hoofd: Kruin naar rug nek",
        "number": "F05"
    },
    "head_depth": {
        "description": "Horizontale afstand van voorkant tot aan de achterkant van het hoofd.",
        "diagram": "Fp1",
        "full_name": "Hoofd: Diepte",
        "number": "F03"
    },
    "head_length": {
        "description": "Verticale afstand vanaf hoofdkruin tot aan de onderkant van de kaak.",
        "diagram": "Fp1",
        "full_name": "Hoofd: Lengte",
        "number": "F02"
    },
    "head_width": {
        "description": "Horizontale afstand van zijkant hoofd tot aan zijkant van het hoofd, waar het hoofd het breedst is.",
        "diagram": "Fp2",
        "full_name": "Hoofd: Wijdte",
        "number": "F04"
    },
    "height": {
        "description": "Verticale afstand van kruin tot vloer",
        "diagram": "Ap1",
        "full_name": "Hoogte:Totaal",
        "number": "A01"
    },
    "height_ankle": {
        "description": "Verticale afstand vanaf het punt waar de voorkant van het been overgaat in de voet, tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Enkel",
        "number": "A11"
    },
    "height_ankle_high": {
        "description": "Verticale afstand vanaf het diepste gedeelte achter de achillespees tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Enkel Hoog",
        "number": "A10"
    },
    "height_armpit": {
        "description": "Verticale afstand vanaf oksel naar de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Oksel",
        "number": "A04"
    },
    "height_bustpoint": {
        "description": "Verticale afstand vanaf Bustepunt tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Bustepunt",
        "number": "A14"
    },
    "height_calf": {
        "description": "Verticale afstand vanaf het breedste gedeelte van de kuit tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Kuit",
        "number": "A09"
    },
    "height_gluteal_fold": {
        "description": "Verticale afstand vanaf de bilspierronding, waar de spier Gluteus samenkomt met de bovenkant van het dijbeen, naar de vloer. ",
        "diagram": "Ap1",
        "full_name": "Hoogte: Bilspier Ronding",
        "number": "A07"
    },
    "height_highhip": {
        "description": "Verticale afstand vanaf het hogere gedeelte van de heup, waar de buik het meest prominent is, tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Hogeheup",
        "number": "A12"
    },
    "height_hip": {
        "description": "Verticale afstand vanaf de heup naar de vloer ",
        "diagram": "Ap1",
        "full_name": "Hoogte: Heup",
        "number": "A06"
    },
    "height_knee": {
        "description": "Verticale afstand vanaf knieholte tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Knie",
        "number": "A08"
    },
    "height_knee_to_ankle": {
        "description": "Verticale afstand vanaf de vouw van de achterkant van de knie tot aan het punt waar de voorkant van het been en bovenkant van de voet in elkaar overgaan.",
        "diagram": "Ap2",
        "full_name": "Hoogte: Knie tot enkel",
        "number": "A21"
    },
    "height_neck_back": {
        "description": "Verticale afstand van rugnek(cervicale vertebra) naar vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Nek Achterkant",
        "number": "A02"
    },
    "height_neck_back_to_knee": {
        "description": "Verticale afstand vanaf de rugzijde van de nek( cervicale vertebra)tot aan vouw van de achterkant van de knie.",
        "diagram": "Ap2",
        "full_name": "Hoogte: Rugnek tot knie",
        "number": "A18"
    },
    "height_neck_back_to_waist_side": {
        "description": "Verticale afstand vanaf rugzijde van de nek tot aan zijkant van de taille.('Hoogte: Rugnek' - 'Hoogte: Zijkant taille').",
        "diagram": "Ap2",
        "full_name": "Hoogte: Rug nek tot zijkant taille",
        "number": "A22"
    },
    "height_neck_front": {
        "description": "Verticale afstand vanaf het kuiltje onderaan de keel tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Keelkuiltje",
        "number": "A16"
    },
    "height_neck_side": {
        "description": "Verticale afstand vanaf de zijkant van de nek tot aan de vloer",
        "diagram": "Ap1",
        "full_name": "Hoogte: Zijkant Nek",
        "number": "A17"
    },
    "height_scapula": {
        "description": "Verticale afstand vanaf de Scapula( schouderbladpunt) naar de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Scapula",
        "number": "A03"
    },
    "height_shoulder_tip": {
        "description": "Verticale afstand vanaf het puntje van de schouder tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Schouder Punt",
        "number": "A15"
    },
    "height_waist_back": {
        "description": "Verticale hoogte van Taille op Rug. ('Hoogte: Voorkant Taille\" - 'Been: Kruis tot aan de vloer\").",
        "diagram": "Ap2",
        "full_name": "Hoogte: Taille Rug",
        "number": "A23"
    },
    "height_waist_front": {
        "description": "Verticale afstand vanaf de voorkant van de taille tot aan de vloer.",
        "diagram": "Ap1",
        "full_name": "Hoogte: Taille Voorkant",
        "number": "A13"
    },
    "height_waist_side": {
        "description": "Verticale afstand vanaf de zijkant van de taille naar de vloer",
        "diagram": "Ap1",
        "full_name": "Hoogte: Zijkant Taille",
        "number": "A05"
    },
    "height_waist_side_to_hip": {
        "description": "Verticale afstand vanaf de zijkant van de taille tot aan heuphoogte.",
        "diagram": "Ap2",
        "full_name": "Hoogte:zijkant taille naar heup",
        "number": "A20"
    },
    "height_waist_side_to_knee": {
        "description": "Verticale afstand vanaf zijkant taille tot aan de vouw van de achterkant van de knie.",
        "diagram": "Ap2",
        "full_name": "Hoogte: Taille zij naar knie",
        "number": "A19"
    },
    "highbust_arc_b": {
        "description": "Van zijkant hogebuste naar zijkant hogebuste via rug. ('Hogebuste omtrek' - 'Hogebusteronding, voorkant').",
        "diagram": "Gp4",
        "full_name": "Hogebusteronding, rug",
        "number": "G27"
    },
    "highbust_arc_f": {
        "description": "Van hoge borst zijde( oksel) tot andere hoge borst zijkant ( oksel) via borstkas.",
        "diagram": "Gp2",
        "full_name": "Hogeborst ronding, voorkant",
        "number": "G11"
    },
    "highbust_arc_half_b": {
        "description": "Helft van 'Hogebust ronding, rug'. Van hogebust rug naar hogebust zij. ('Hogebust ronding, rug' / 2).",
        "diagram": "Gp5",
        "full_name": "Helft hogebust ronding, rug",
        "number": "G35"
    },
    "highbust_arc_half_f": {
        "description": "Helft van 'hogeborstronding, voorkant'. Van hogeborst voorkant naar hogeborst zijkant. (Hogeborst ronding, voorkant' / 2).",
        "diagram": "Gp3",
        "full_name": "Hogeborstronding, voorkant, helft",
        "number": "G19"
    },
    "highbust_b_over_shoulder_to_highbust_f": {
        "description": "From Highbust Back, over Shoulder, then aim at Bustpoint, stopping measurement at Highbust level.",
        "diagram": "Pp11",
        "full_name": "Highbust Back, over Shoulder, to Highbust level",
        "number": "P11"
    },
    "highbust_back_over_shoulder_to_armfold_front": {
        "description": "From Highbust Back over Shoulder to Armfold Front.",
        "diagram": "Pp4",
        "full_name": "Highbust Back, over Shoulder, to Armfold Front",
        "number": "P04"
    },
    "highbust_back_over_shoulder_to_waist_front": {
        "description": "From Highbust Back, over Shoulder touching  Neck Side, to Waist Front.",
        "diagram": "Pp5",
        "full_name": "Highbust Back, over Shoulder, to Waist Front",
        "number": "P05"
    },
    "highbust_circ": {
        "description": "Omtrek van het hoge gedeelte van de borstkas, langs de kortste afstand kruislings langs de hoge borst, hoog onder de oksels.",
        "diagram": "Gp1",
        "full_name": "Hoge buste omtrek",
        "number": "G03"
    },
    "highbust_to_waist_b": {
        "description": "From Highbust Back down to Waist Back. ('Neck Back to Waist Back' - 'Neck Back to Highbust Back').",
        "diagram": "Hp7",
        "full_name": "Highbust Back to Waist Back",
        "number": "H22"
    },
    "highbust_to_waist_f": {
        "description": "Van voorkant hogebuste naar voorkant taille. Gebruik meetlint om het gat tussen bustepunten te overbruggen.('Voorkant nek naar voorkant taille' - 'Voorkant nek naar voorkant hogebuste').",
        "diagram": "Hp4",
        "full_name": "Voorkant hogebuste naar voorkant taille",
        "number": "H08"
    },
    "highhip_arc_b": {
        "description": "Van hogeheupzijde naar hogeheupzijde via rug. ('Hogeheup omtrek\" - 'Hogeheup ronding, voorkant').",
        "diagram": "Gp4",
        "full_name": "Hogeheup ronding, rug",
        "number": "G32"
    },
    "highhip_arc_f": {
        "description": "Van Hogeheup zijkant tot aan hogeheup zijkant via voorkant.",
        "diagram": "Gp2",
        "full_name": "Hogeheup ronding, voorkant",
        "number": "G16"
    },
    "highhip_arc_half_b": {
        "description": "Helft van 'Hogeheupronding, rug'. Van hogeheup rug naar hogebuste zijde. ('Hogeheupronding, rug' / 2).",
        "diagram": "Gp5",
        "full_name": "Halve hogeheupronding, rug",
        "number": "G40"
    },
    "highhip_arc_half_f": {
        "description": "Helft van 'hogeheupronding, voorkant'. ('Hogeheupronding, voorkant' / 2).",
        "diagram": "Gp3",
        "full_name": "Hogeheupronding, voorkant, helft",
        "number": "G24"
    },
    "highhip_circ": {
        "description": " Omtrek rond de Hogeheup, waar buik het meest vooruitsteekt, parallel aan de vloer.",
        "diagram": "Gp1",
        "full_name": "Hogeheup omtrek",
        "number": "G08"
    },
    "hip_arc_b": {
        "description": "Van heupzijde naar heup zijde via rug. ('Heup omtrek' - 'Heup ronding, voorkant').",
        "diagram": "Gp4",
        "full_name": "Heup ronding, rug",
        "number": "G33"
    },
    "hip_arc_f": {
        "description": "Van zijkant heup naar zijkant heup via voorkant.",
        "diagram": "Gp2",
        "full_name": "Heupronding, voorkant",
        "number": "G17"
    },
    "hip_arc_half_b": {
        "description": "Helft van 'Heupronding, rug'. ('Heupronding, rug' / 2).",
        "diagram": "Gp5",
        "full_name": "Halve heupronding, rug",
        "number": "G41"
    },
    "hip_arc_half_f": {
        "description": "Helft van 'Heupronding, voorkant'. ('Heupronding, voorkant' / 2).",
        "diagram": "Gp3",
        "full_name": "Heupronding, voorkant, helft",
        "number": "G25"
    },
    "hip_circ": {
        "description": "Omtrek rond Heup waar Heupbolling het meest uitsteekt, parallel aan de vloer.",
        "diagram": "Gp1",
        "full_name": "Heup Omtrek",
        "number": "G09"
    },
    "hip_circ_with_abdomen": {
        "description": "Meten op heup level, inclusief de diepte( lees uitsteken) van de onderbuik.( Heup ronding, rug + heup ronding met onderbuik, voorkant).",
        "diagram": "Gp9",
        "full_name": "Heup omtrek, inclusief onderbuik",
        "number": "G46"
    },
    "hip_with_abdomen_arc_f": {
        "description": "Plak stevig papier rond voorkant van de onderbuik aan beide zijde. Meet van heup zijde naar heup zijde over het papier langs de voorkant.",
        "diagram": "Gp6",
        "full_name": "Heupronding met onderbuik, voorkant",
        "number": "G42"
    },
    "indent_ankle_high": {
        "description": "Horizontale afstand op een platte lat welke loodrecht tegen de hiel en de grootste inham van de enkel geplaatst is.",
        "diagram": "Cp2",
        "full_name": "Inham: Enkel hoog",
        "number": "C03"
    },
    "indent_neck_back": {
        "description": "Horizontale afstand van het schouderblad( blad punt) tot aan de rugzijde van de nek",
        "diagram": "Cp1",
        "full_name": "Inham: rug nek",
        "number": "C01"
    },
    "indent_waist_back": {
        "description": "Horizontale afstand op een platte lat welke tegen de heup, schouderblad en taille aan de rugzijde geplaatst is.",
        "diagram": "Cp2",
        "full_name": "Inham: Rug taille",
        "number": "C02"
    },
    "leg_ankle_circ": {
        "description": "Ankle circumference where front of leg meets the top of the foot.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle circumference",
        "number": "M09"
    },
    "leg_ankle_diag_circ": {
        "description": "Ankle circumference diagonal from top of foot to bottom of heel.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle diagonal circumference",
        "number": "M11"
    },
    "leg_ankle_high_circ": {
        "description": "Ankle circumference where the indentation at the back of the ankle is the deepest.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle High circumference",
        "number": "M08"
    },
    "leg_calf_circ": {
        "description": "Calf circumference at the largest part of lower leg.",
        "diagram": "Mp2",
        "full_name": "Leg: Calf circumference",
        "number": "M07"
    },
    "leg_crotch_to_ankle": {
        "description": "From Crotch to Ankle. ('Leg: Crotch to Floor' - 'Height: Ankle').",
        "diagram": "Mp3",
        "full_name": "Leg: Crotch to Ankle",
        "number": "M12"
    },
    "leg_crotch_to_floor": {
        "description": "Stand feet close together. Measure from crotch level (touching body, no extra space) down to floor.",
        "diagram": "Mp1",
        "full_name": "Leg: Crotch to floor",
        "number": "M01"
    },
    "leg_knee_circ": {
        "description": "Knee circumference with straight leg.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee circumference",
        "number": "M05"
    },
    "leg_knee_circ_bent": {
        "description": "Knee circumference with leg bent.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee circumference, bent",
        "number": "M10"
    },
    "leg_knee_small_circ": {
        "description": "Leg circumference just below the knee.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee Small circumference",
        "number": "M06"
    },
    "leg_thigh_mid_circ": {
        "description": "Thigh circumference about halfway between Crotch and Knee.",
        "diagram": "Mp2",
        "full_name": "Leg: Thigh Middle circumference",
        "number": "M04"
    },
    "leg_thigh_upper_circ": {
        "description": "Thigh circumference at the fullest part of the upper Thigh near the Crotch.",
        "diagram": "Mp2",
        "full_name": "Leg: Thigh Upper circumference",
        "number": "M03"
    },
    "leg_waist_side_to_ankle": {
        "description": "From Waist Side to Ankle. ('Leg: Waist Side to Floor' - 'Height: Ankle').",
        "diagram": "Mp3",
        "full_name": "Leg: Waist Side to Ankle",
        "number": "M13"
    },
    "leg_waist_side_to_floor": {
        "description": "From Waist Side along curve to Hip level then straight down to floor.",
        "diagram": "Mp1",
        "full_name": "Leg: Waist Side to floor",
        "number": "M02"
    },
    "leg_waist_side_to_knee": {
        "description": "From Waist Side along curve to Hip level then straight down to  Knee level. ('Leg: Waist Side to Floor' - 'Height Knee').",
        "diagram": "Mp3",
        "full_name": "Leg: Waist Side to Knee",
        "number": "M14"
    },
    "lowbust_arc_b": {
        "description": "Van zijkant lagebuste naar zijkant lagebuste via rug. ('Lagebuste omtrek' - 'Lagebusteronding, voorkant').",
        "diagram": "Gp4",
        "full_name": "Lagebusteronding, rug",
        "number": "G29"
    },
    "lowbust_arc_f": {
        "description": "Van lagebustezijkant naar lagebustezijkant via voorkant.",
        "diagram": "Gp2",
        "full_name": "Lage busteronding, voorkant",
        "number": "G13"
    },
    "lowbust_arc_half_b": {
        "description": "Helft van 'Lagebusteronding, rug'. ('Lagebusteronding, rug' / 2).",
        "diagram": "Gp5",
        "full_name": "Halve lagbusteronding, rug",
        "number": "G37"
    },
    "lowbust_arc_half_f": {
        "description": "Helft van 'Lagebusteronding, voorkant'. ('Lagebusteronding, voorkant' / 2).",
        "diagram": "Gp3",
        "full_name": "Lagebusteronding, voorkant, voorkant",
        "number": "G21"
    },
    "lowbust_circ": {
        "description": "Omtrek rondom het lage gedeelte van de borst onder de borsten, parallel aan de vloer.",
        "diagram": "Gp1",
        "full_name": "Lage borst omtrek",
        "number": "G05"
    },
    "lowbust_to_waist_b": {
        "description": "From Lowbust Back down to Waist Back.",
        "diagram": "Hp7",
        "full_name": "Lowbust Back to Waist Back",
        "number": "H25"
    },
    "lowbust_to_waist_f": {
        "description": "Van voorkant lagebuste naar beneden naar voorkant taille.",
        "diagram": "Hp4",
        "full_name": "Voorkant lagebuste naar voorkant taille",
        "number": "H11"
    },
    "neck_arc_b": {
        "description": "Van zijkant nek naar zijkant nek via rug. ('Nek omtrek' - 'Nekronding, voorkant').",
        "diagram": "Gp4",
        "full_name": "Nekronding, rug",
        "number": "G26"
    },
    "neck_arc_f": {
        "description": "Van zijkant nek tot aan andere zijkant nek via voorkant hals.",
        "diagram": "Gp2",
        "full_name": "Nek ronding, voorkant",
        "number": "G10"
    },
    "neck_arc_half_b": {
        "description": "Helft van 'Nek ronding, rug'. ('Nekronding, rug' / 2).",
        "diagram": "Gp5",
        "full_name": "Halve nekronding rug",
        "number": "G34"
    },
    "neck_arc_half_f": {
        "description": "Helft van 'Nekronding, voorkant'. ('Nek ronding, voorkant' / 2).",
        "diagram": "Gp3",
        "full_name": "Nekronding, voorkant, helft",
        "number": "G18"
    },
    "neck_back_to_across_back": {
        "description": "From neck back, down to level of Across Back measurement.",
        "diagram": "Hp13",
        "full_name": "Neck Back to Across Back",
        "number": "H41"
    },
    "neck_back_to_armfold_front": {
        "description": "From Neck Back over Shoulder to Armfold Front.",
        "diagram": "Pp2",
        "full_name": "Neck Back to Armfold Front",
        "number": "P02"
    },
    "neck_back_to_armfold_front_to_highbust_back": {
        "description": "From Neck Back over Shoulder to Armfold Front, under arm to Highbust Back.",
        "diagram": "Pp8",
        "full_name": "Neck Back, to Armfold Front, to Highbust Back",
        "number": "P08"
    },
    "neck_back_to_armfold_front_to_neck_back": {
        "description": "From Neck Back, over Shoulder to Armfold Front, under arm and return to start.",
        "diagram": "Pp6",
        "full_name": "Neck Back, to Armfold Front, to Neck Back",
        "number": "P06"
    },
    "neck_back_to_armfold_front_to_waist_side": {
        "description": "From Neck Back, over Shoulder, down chest to Waist Side.",
        "diagram": "Pp3",
        "full_name": "Neck Back, over Shoulder, to Waist Side",
        "number": "P03"
    },
    "neck_back_to_bust_b": {
        "description": "From Neck Back down to Bust Back.",
        "diagram": "Hp7",
        "full_name": "Neck Back to Bust Back",
        "number": "H23"
    },
    "neck_back_to_bust_front": {
        "description": "From Neck Back, over Shoulder, to Bust Front.",
        "diagram": "Pp1",
        "full_name": "Neck Back to Bust Front",
        "number": "P01"
    },
    "neck_back_to_highbust_b": {
        "description": "From Neck Back down to Highbust Back.",
        "diagram": "Hp7",
        "full_name": "Neck Back to Highbust Back",
        "number": "H21"
    },
    "neck_back_to_shoulder_tip_b": {
        "description": "From Neck Back to Shoulder Tip.",
        "diagram": "Ip6",
        "full_name": "Neck Back to Shoulder Tip",
        "number": "I13"
    },
    "neck_back_to_waist_b": {
        "description": "From Neck Back down to Waist Back.",
        "diagram": "Hp6",
        "full_name": "Neck Back to Waist Back",
        "number": "H19"
    },
    "neck_back_to_waist_front": {
        "description": "From Neck Back around Neck Side down to Waist Front.",
        "diagram": "Op1",
        "full_name": "Neck Back to Waist Front",
        "number": "O01"
    },
    "neck_back_to_waist_side": {
        "description": "From Neck Back diagonal across back to Waist Side.",
        "diagram": "Kp5",
        "full_name": "Neck Back to Waist Side",
        "number": "K06"
    },
    "neck_circ": {
        "description": "Nek omtrek aan de basis van de nek, langs rugzijde van de nek, zijkant nek en voorkant van de nek.",
        "diagram": "Gp1",
        "full_name": "Nek Omtrek",
        "number": "G02"
    },
    "neck_front_to_bust_f": {
        "description": "Van voorkant naar beneden naar voorkant buste. Vergt meetlint om het gat tussen bustepunten te dekken.",
        "diagram": "Hp4",
        "full_name": "Voorkant nek naar voorkant buste.",
        "number": "H09"
    },
    "neck_front_to_highbust_f": {
        "description": "Voorkant hogebuste naar beneden naar voorkant hogebuste.",
        "diagram": "Hp4",
        "full_name": "Voorkant nek naar voorkant hogebuste",
        "number": "H07"
    },
    "neck_front_to_shoulder_tip_f": {
        "description": "From Neck Front to Shoulder Tip.",
        "diagram": "Ip5",
        "full_name": "Neck Front to Shoulder Tip",
        "number": "I12"
    },
    "neck_front_to_waist_f": {
        "description": "Van voorkant nek, over meetlint tussen borstpunten, naar beneden naar voorkant taille.",
        "diagram": "Hp1",
        "full_name": "Nek voorkant naar taille voorkant",
        "number": "H01"
    },
    "neck_front_to_waist_flat_f": {
        "description": "Van voorkant nek naar beneden tussen borsten naar voorkant taille.",
        "diagram": "Hp2",
        "full_name": "Voorkant nek naar platte voorkant taille",
        "number": "H02"
    },
    "neck_front_to_waist_side": {
        "description": "From Neck Front diagonal to Waist Side.",
        "diagram": "Kp2",
        "full_name": "Neck Front to Waist Side",
        "number": "K02"
    },
    "neck_mid_circ": {
        "description": "Omtrek vanaf het middengedeelte van de nek, tot ongeveer halverwege kaak en torso.",
        "diagram": "Gp1",
        "full_name": "Nek omtrek, midden gedeelte.",
        "number": "G01"
    },
    "neck_side_to_armfold_b": {
        "description": "From Neck Side diagonal to Armfold Back.",
        "diagram": "Kp9",
        "full_name": "Neck Side to Armfold Back",
        "number": "K11"
    },
    "neck_side_to_armfold_f": {
        "description": "From Neck Side diagonal to Armfold Front.",
        "diagram": "Kp6",
        "full_name": "Neck Side to Armfold Front",
        "number": "K08"
    },
    "neck_side_to_armpit_b": {
        "description": "From Neck Side diagonal across back to Highbust Side (Armpit).",
        "diagram": "Kp10",
        "full_name": "Neck Side to Highbust Side, back",
        "number": "K12"
    },
    "neck_side_to_armpit_f": {
        "description": "From Neck Side diagonal across front to Highbust Side (Armpit).",
        "diagram": "Kp7",
        "full_name": "Neck Side to Highbust Side, front",
        "number": "K09"
    },
    "neck_side_to_bust_b": {
        "description": "From Neck Side straight down back to Bust level.",
        "diagram": "Hp8",
        "full_name": "Neck Side to Bust level, back",
        "number": "H27"
    },
    "neck_side_to_bust_f": {
        "description": "From Neck Side straight down front to Bust level.",
        "diagram": "Hp5",
        "full_name": "Neck Side to Bust level, front",
        "number": "H14"
    },
    "neck_side_to_bust_side_b": {
        "description": "Neck Side diagonal across back to Bust Side.",
        "diagram": "Kp11",
        "full_name": "Neck Side to Bust Side, back",
        "number": "K13"
    },
    "neck_side_to_bust_side_f": {
        "description": "Neck Side diagonal across front to Bust Side.",
        "diagram": "Kp8",
        "full_name": "Neck Side to Bust Side, front",
        "number": "K10"
    },
    "neck_side_to_highbust_b": {
        "description": "From Neck Side straight down back to Highbust level.",
        "diagram": "Hp8",
        "full_name": "Neck Side to Highbust level, back",
        "number": "H28"
    },
    "neck_side_to_highbust_f": {
        "description": "From Neck Side straight down front to Highbust level.",
        "diagram": "Hp5",
        "full_name": "Neck Side to Highbust level, front",
        "number": "H15"
    },
    "neck_side_to_waist_b": {
        "description": "From Neck Side straight down back to Waist level.",
        "diagram": "Hp6",
        "full_name": "Neck Side to Waist level, back",
        "number": "H18"
    },
    "neck_side_to_waist_bustpoint_f": {
        "description": "Van zijkant nek over bustepunt naar taillelevel, een rechte lijn vormend",
        "diagram": "Hp3",
        "full_name": "Zijkant nek naar taille level, via bustepunt",
        "number": "H06"
    },
    "neck_side_to_waist_f": {
        "description": "Van zijkant nek direct naar beneden voorkant taille leve.",
        "diagram": "Hp3",
        "full_name": "Zijkant nek naar taille level, voorkant",
        "number": "H05"
    },
    "neck_side_to_waist_scapula_b": {
        "description": "From Neck Side across Scapula down to Waist level, forming a straight line.",
        "diagram": "Hp6",
        "full_name": "Neck Side to Waist level, through Scapula",
        "number": "H20"
    },
    "neck_side_to_waist_side_b": {
        "description": "From Neck Side diagonal across back to Waist Side.",
        "diagram": "Kp5",
        "full_name": "Neck Side to Waist Side, back",
        "number": "K07"
    },
    "neck_side_to_waist_side_f": {
        "description": "From Neck Side diagonal across front to Waist Side.",
        "diagram": "Kp2",
        "full_name": "Neck Side to Waist Side, front",
        "number": "K03"
    },
    "neck_width": {
        "description": "Measure between the 'legs' of an unclosed necklace or chain draped around the neck.",
        "diagram": "Ip7",
        "full_name": "Neck Width",
        "number": "I14"
    },
    "rib_arc_b": {
        "description": "Van zijkant rib naar zijkant rib via rug. ('Rib omtrek' - 'Ribronding, voorkant').",
        "diagram": "Gp4",
        "full_name": "Ribronding, rug",
        "number": "G30"
    },
    "rib_arc_f": {
        "description": "Van Ribzijkant naar Ribzijkant, via voorkant",
        "diagram": "Gp2",
        "full_name": "Ribronding, Voorkant",
        "number": "G14"
    },
    "rib_arc_half_b": {
        "description": "Helft van 'Ribronding, rug'. ('Ribronding, rug' / 2).",
        "diagram": "Gp5",
        "full_name": "Halve ribronding, rug",
        "number": "G38"
    },
    "rib_arc_half_f": {
        "description": "Helft van 'Ribronding, voorkant'. ('Ribronding, voorkant' / 2).",
        "diagram": "Gp3",
        "full_name": "Ribronding, voorkant, helft",
        "number": "G22"
    },
    "rib_circ": {
        "description": "Omtrek van de ribbenkast op het laagste gedeelte van de ribbenkast aan de zijkant, parallel aan de vloer.",
        "diagram": "Gp1",
        "full_name": "Ribbenkast omtrek",
        "number": "G06"
    },
    "rib_to_waist_side": {
        "description": "Van onderste rib aan zijkant naar beneden naar zijkant taille.",
        "diagram": "Hp4",
        "full_name": "Zijkant rib naar zijkant taille",
        "number": "H12"
    },
    "rise_length_b": {
        "description": "Vertical distance from Waist Back to Crotch level. ('Height: Waist Back' - 'Leg: Crotch to Floor')",
        "diagram": "Np4",
        "full_name": "Rise length, back",
        "number": "N06"
    },
    "rise_length_diag": {
        "description": "Measure from Waist Side diagonally to a string tied at the top of the leg, seated on a hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, diagonal",
        "number": "N05"
    },
    "rise_length_f": {
        "description": "Vertical Distance from Waist Front to Crotch level. ('Height: Waist Front' - 'Leg: Crotch to Floor')",
        "diagram": "Np4",
        "full_name": "Rise length, front",
        "number": "N07"
    },
    "rise_length_side": {
        "description": "Vertical distance from Waist side down to Crotch level. Use formula (Height: Waist side - Leg: Crotch to floor).",
        "diagram": "Np5",
        "full_name": "Rise length, side",
        "number": "N08"
    },
    "rise_length_side_sitting": {
        "description": "From Waist Side around hp curve down to surface, while seated on hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, side, sitting",
        "number": "N04"
    },
    "shoulder_center_to_highbust_b": {
        "description": "From mid-Shoulder down back to Highbust level, aimed through Scapula.",
        "diagram": "Hp8",
        "full_name": "Shoulder center to Highbust level, back",
        "number": "H29"
    },
    "shoulder_center_to_highbust_f": {
        "description": "From mid-Shoulder down front to Highbust level, aimed at Bustpoint.",
        "diagram": "Hp5",
        "full_name": "Shoulder center to Highbust level, front",
        "number": "H16"
    },
    "shoulder_length": {
        "description": "From Neck Side to Shoulder Tip.",
        "diagram": "Ip1",
        "full_name": "Schouder lengte",
        "number": "I01"
    },
    "shoulder_slope_neck_back_angle": {
        "description": "Angle formed by  line from Neck Back to Shoulder Tip and line from Neck Back parallel to floor.",
        "diagram": "Hp11",
        "full_name": "Shoulder Slope Angle from Neck Back",
        "number": "H38"
    },
    "shoulder_slope_neck_back_height": {
        "description": "Vertical distance between Neck Back and Shoulder Tip.",
        "diagram": "Hp11",
        "full_name": "Shoulder Slope length from Neck Back",
        "number": "H39"
    },
    "shoulder_slope_neck_side_angle": {
        "description": "Angle formed by line from Neck Side to Shoulder Tip and line from Neck Side parallel to floor.",
        "diagram": "Hp11",
        "full_name": "Shoulder Slope Angle from Neck Side",
        "number": "H36"
    },
    "shoulder_slope_neck_side_length": {
        "description": "Vertical distance between Neck Side and Shoulder Tip.",
        "diagram": "Hp11",
        "full_name": "Shoulder Slope length from Neck Side",
        "number": "H37"
    },
    "shoulder_slope_shoulder_tip_angle": {
        "description": "Angle formed by line from Neck Side to Shoulder Tip and vertical line at Shoulder Tip.",
        "diagram": "Hp12",
        "full_name": "Shoulder Slope Angle from Shoulder Tip",
        "number": "H40"
    },
    "shoulder_tip_to_armfold_b": {
        "description": "From Shoulder Tip around Armscye down to Armfold Back.",
        "diagram": "Hp8",
        "full_name": "Shoulder Tip to Armfold Back",
        "number": "H26"
    },
    "shoulder_tip_to_armfold_f": {
        "description": "From Shoulder Tip around Armscye down to Armfold Front.",
        "diagram": "Hp5",
        "full_name": "Schouderpunt naar voorkant armvouw",
        "number": "H13"
    },
    "shoulder_tip_to_shoulder_tip_b": {
        "description": "From Shoulder Tip to Shoulder Tip, across the back.",
        "diagram": "Ip3",
        "full_name": "Shoulder Tip to Shoulder Tip, back",
        "number": "I07"
    },
    "shoulder_tip_to_shoulder_tip_f": {
        "description": "From Shoulder Tip to Shoulder Tip, across front.",
        "diagram": "Ip1",
        "full_name": "Shoulder Tip to Shoulder Tip, front",
        "number": "I02"
    },
    "shoulder_tip_to_shoulder_tip_half_b": {
        "description": "Half of 'Shoulder Tip to Shoulder Tip, back'. ('Shoulder Tip to Shoulder Tip,  back' / 2).",
        "diagram": "Ip4",
        "full_name": "Shoulder Tip to Shoulder Tip, back, half",
        "number": "I10"
    },
    "shoulder_tip_to_shoulder_tip_half_f": {
        "description": "Half of' Shoulder Tip to Shoulder tip, front'. ('Shoulder Tip to Shoulder Tip, front' / 2).",
        "diagram": "Ip2",
        "full_name": "Shoulder Tip to Shoulder Tip, front, half",
        "number": "I05"
    },
    "shoulder_tip_to_waist_b_1in_offset": {
        "description": "Mark 1in (2.54cm) outward from Waist Back along Waist level. Measure from Shoulder Tip diagonal to mark.",
        "diagram": "Kp4",
        "full_name": "Shoulder Tip to Waist Back, with 1in (2.54cm) offset",
        "number": "K05"
    },
    "shoulder_tip_to_waist_back": {
        "description": "From Shoulder Tip diagonal to Waist Back.",
        "diagram": "Kp3",
        "full_name": "Shoulder Tip to Waist Back",
        "number": "K04"
    },
    "shoulder_tip_to_waist_front": {
        "description": "From Shoulder Tip diagonal to Waist Front.",
        "diagram": "Kp1",
        "full_name": "Shoulder Tip to Waist Front",
        "number": "K01"
    },
    "shoulder_tip_to_waist_side_b": {
        "description": "From Shoulder Tip, curving around Armscye Back, then down to Waist Side.",
        "diagram": "Hp6",
        "full_name": "Shoulder Tip to Waist Side, back",
        "number": "H17"
    },
    "shoulder_tip_to_waist_side_f": {
        "description": "Van schouderpunt, om de voorkant armvouw gedraaid, daarna naar beneden naar zijkant taille.",
        "diagram": "Hp3",
        "full_name": "Schouderpunt naar zijkant taille, voorkant",
        "number": "H04"
    },
    "waist_arc_b": {
        "description": "Van taillezijde naar taillezijde via rug. ('Taille omtrek' - 'Taille ronding, voorkant').",
        "diagram": "Gp4",
        "full_name": "Tailleronding, rug",
        "number": "G31"
    },
    "waist_arc_f": {
        "description": "Van zijkant taille naar zijkant taille via voorkant.",
        "diagram": "Gp2",
        "full_name": "Taille ronding, voorkant",
        "number": "G15"
    },
    "waist_arc_half_b": {
        "description": "Helft van 'Tailleronding, rug'. ('Tailleronding, rug' / 2).",
        "diagram": "Gp5",
        "full_name": "Halve tailleronding, rug",
        "number": "G39"
    },
    "waist_arc_half_f": {
        "description": "Helft van 'Tailleronding, voorkant'. ('Tailleronding, voorkant' / 2).",
        "diagram": "Gp3",
        "full_name": "Tailleronding, voorkant, helft",
        "number": "G23"
    },
    "waist_circ": {
        "description": "Omtrek rond de taille, de natuurlijke contouren volgend. Tailles zijn vrijwel altijd hoger op de rug.",
        "diagram": "Gp1",
        "full_name": "Taille omtrek",
        "number": "G07"
    },
    "waist_natural_arc_b": {
        "description": "From Side to Side at Natural Waist level, across the back. Calculate as ( Natural Waist circumference  - Natural Waist arc (front) ).",
        "diagram": "Op5",
        "full_name": "Natural Waist arc, back",
        "number": "O05"
    },
    "waist_natural_arc_f": {
        "description": "From Side to Side at the Natural Waist level, across the front.",
        "diagram": "Op4",
        "full_name": "Natural Waist arc, front",
        "number": "O04"
    },
    "waist_natural_circ": {
        "description": "Torso circumference at men's natural side Abdominal Obliques indentation, if Oblique indentation isn't found then just below the Navel level.",
        "diagram": "Op3",
        "full_name": "Natural Waist circumference",
        "number": "O03"
    },
    "waist_to_highhip_b": {
        "description": "From Waist Back down to Highhip Back.",
        "diagram": "Hp10",
        "full_name": "Waist Back to Highhip Back",
        "number": "H33"
    },
    "waist_to_highhip_f": {
        "description": "From Waist Front to Highhip Front.",
        "diagram": "Hp9",
        "full_name": "Waist Front to Highhip Front",
        "number": "H30"
    },
    "waist_to_highhip_side": {
        "description": "From Waist Side to Highhip Side.",
        "diagram": "Hp9",
        "full_name": "Waist Side to Highhip Side",
        "number": "H32"
    },
    "waist_to_hip_b": {
        "description": "From Waist Back down to Hip Back. Requires tape to cover the gap between buttocks.",
        "diagram": "Hp10",
        "full_name": "Waist Back to Hip Back",
        "number": "H34"
    },
    "waist_to_hip_f": {
        "description": "From Waist Front to Hip Front.",
        "diagram": "Hp9",
        "full_name": "Waist Front to Hip Front",
        "number": "H31"
    },
    "waist_to_hip_side": {
        "description": "From Waist Side to Hip Side.",
        "diagram": "Hp10",
        "full_name": "Waist Side to Hip Side",
        "number": "H35"
    },
    "waist_to_natural_waist_b": {
        "description": "Length from Waist Back to Natural Waist Back.",
        "diagram": "Op7",
        "full_name": "Waist Back to Natural Waist Back",
        "number": "O07"
    },
    "waist_to_natural_waist_f": {
        "description": "Length from Waist Front to Natural Waist Front.",
        "diagram": "Op6",
        "full_name": "Waist Front to Natural Waist Front",
        "number": "O06"
    },
    "waist_to_waist_halter": {
        "description": "From Waist level around Neck Back to Waist level.",
        "diagram": "Op2",
        "full_name": "Waist to Waist Halter, around Neck Back",
        "number": "O02"
    },
    "width_abdomen_to_hip": {
        "description": "Horizontale afstand van het dikste deel van de buik tot aan dikste deel van de heup.",
        "diagram": "Bp2",
        "full_name": "Wijdte: Buik tot heup",
        "number": "B05"
    },
    "width_bust": {
        "description": "Horizontale afstand van zijkantbuste tot zijkantbuste.",
        "diagram": "Bp1",
        "full_name": "Wijdte: Buste",
        "number": "B02"
    },
    "width_hip": {
        "description": "Horizontale afstand vanaf zijkant heup tot aan zijkant heup.",
        "diagram": "Bp1",
        "full_name": "Wijdte: Heup",
        "number": "B04"
    },
    "width_shoulder": {
        "description": "Horizontale afstand van schouderpunt tot schouderpunt.",
        "diagram": "Bp1",
        "full_name": "Breedte: schouder",
        "number": "B01"
    },
    "width_waist": {
        "description": "Horizontale afstand vanaf zijkant taile tot aan zijkant taille.",
        "diagram": "Bp1",
        "full_name": "Wijdte: Taille",
        "number": "B03"
    }
}
