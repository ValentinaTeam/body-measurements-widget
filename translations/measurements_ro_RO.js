module.exports = {
    "across_back_b": {
        "description": "De la subraț la subraț prin cea mai scurta lațime la spate",
        "diagram": "Ip3",
        "full_name": "Spate Peste",
        "number": "I08"
    },
    "across_back_center_to_armfold_front_to_across_back_center": {
        "description": "From center of Across Back, over Shoulder, under Arm, and return to start.",
        "diagram": "Pp7",
        "full_name": "Across Back Center, circled around Shoulder",
        "number": "P07"
    },
    "across_back_half_b": {
        "description": "Jumătate din 'Spate Peste'. ('Spate Peste' / 2).",
        "diagram": "Ip4",
        "full_name": "Spate Peste, jumătate",
        "number": "I11"
    },
    "across_back_to_waist_b": {
        "description": "De la mijlocul măsurii Peste Spate în jos la Talie Spate",
        "diagram": "Hp13",
        "full_name": "Peste Spate la Talie Spate",
        "number": "H42"
    },
    "across_chest_f": {
        "description": "De la subraț la subraț, prin cea mai scurtă lățime ",
        "diagram": "Ip1",
        "full_name": "Piept peste",
        "number": "I03"
    },
    "across_chest_half_f": {
        "description": "Jumătate din 'Piept Peste'. ('Piept Peste' / 2).",
        "diagram": "Ip2",
        "full_name": "Piept Peste, jumătate",
        "number": "I06"
    },
    "arm_above_elbow_circ": {
        "description": "Arm circumference at Bicep level.",
        "diagram": "Lp4",
        "full_name": "Arm: Above Elbow circumference",
        "number": "L12"
    },
    "arm_across_back_center_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Middle of Back to Elbow Tip.",
        "diagram": "Op10",
        "full_name": "Arm: Across Back Center to Elbow, high bend",
        "number": "O12"
    },
    "arm_across_back_center_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Middle of Back to Elbow Tip to Wrist bone.",
        "diagram": "Op10",
        "full_name": "Arm: Across Back Center to Wrist, high bend",
        "number": "O13"
    },
    "arm_armpit_to_elbow": {
        "description": "From Armpit to inner Elbow, arm straight.",
        "diagram": "Lp3",
        "full_name": "Arm: Armpit to Elbow, inside",
        "number": "L09"
    },
    "arm_armpit_to_wrist": {
        "description": "From Armpit to ulna Wrist bone, arm straight.",
        "diagram": "Lp3",
        "full_name": "Arm: Armpit to Wrist, inside",
        "number": "L08"
    },
    "arm_armscye_back_center_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Armscye Back to Elbow Tip.",
        "diagram": "Op11",
        "full_name": "Arm: Armscye Back Center to Wrist, high bend",
        "number": "O14"
    },
    "arm_elbow_circ": {
        "description": "Elbow circumference, arm straight.",
        "diagram": "Lp4",
        "full_name": "Arm: Elbow circumference",
        "number": "L13"
    },
    "arm_elbow_circ_bent": {
        "description": "Elbow circumference, arm is bent.",
        "diagram": "Lp1",
        "full_name": "Arm: Elbow circumference, bent",
        "number": "L04"
    },
    "arm_elbow_to_wrist": {
        "description": "From Elbow to Wrist, arm straight. ('Arm: Shoulder Tip to Wrist' - 'Arm: Shoulder Tip to Elbow').",
        "diagram": "Lp2",
        "full_name": "Arm: Elbow to Wrist",
        "number": "L07"
    },
    "arm_elbow_to_wrist_bent": {
        "description": "Elbow tip to wrist. ('Arm: Shoulder Tip to Wrist, bent' - 'Arm: Shoulder Tip to Elbow, bent').",
        "diagram": "Lp1",
        "full_name": "Arm: Elbow to Wrist, bent",
        "number": "L03"
    },
    "arm_elbow_to_wrist_inside": {
        "description": "From inside Elbow to Wrist. ('Arm: Armpit to Wrist, inside' - 'Arm: Armpit to Elbow, inside').",
        "diagram": "Lp3",
        "full_name": "Arm: Elbow to Wrist, inside",
        "number": "L10"
    },
    "arm_lower_circ": {
        "description": "Arm circumference where lower arm is widest.",
        "diagram": "Lp4",
        "full_name": "Arm: Lower Arm circumference",
        "number": "L14"
    },
    "arm_neck_back_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Back to Elbow Tip.",
        "diagram": "Op8",
        "full_name": "Arm: Neck Back to Elbow, high bend",
        "number": "O08"
    },
    "arm_neck_back_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Back to Elbow Tip to Wrist bone.",
        "diagram": "Op8",
        "full_name": "Arm: Neck Back to Wrist, high bend",
        "number": "O09"
    },
    "arm_neck_side_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Side to Elbow Tip.",
        "diagram": "Op9",
        "full_name": "Arm: Neck Side to Elbow, high bend",
        "number": "O10"
    },
    "arm_neck_side_to_finger_tip": {
        "description": "From Neck Side down arm to tip of middle finger. ('Shoulder Length' + 'Arm: Shoulder Tip to Wrist' + 'Hand: Length').",
        "diagram": "Lp7",
        "full_name": "Arm: Neck Side to Finger Tip",
        "number": "L18"
    },
    "arm_neck_side_to_outer_elbow": {
        "description": "From Neck Side over Shoulder Tip down to Elbow. (Shoulder length + Arm: Shoulder Tip to Elbow).",
        "diagram": "Lp10",
        "full_name": "Arm: Neck side to Elbow",
        "number": "L22"
    },
    "arm_neck_side_to_wrist": {
        "description": "From Neck Side to Wrist. ('Shoulder Length' + 'Arm: Shoulder Tip to Wrist').",
        "diagram": "Lp6",
        "full_name": "Arm: Neck Side to Wrist",
        "number": "L17"
    },
    "arm_neck_side_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Side to Elbow Tip to Wrist bone.",
        "diagram": "Op9",
        "full_name": "Arm: Neck Side to Wrist, high bend",
        "number": "O11"
    },
    "arm_shoulder_tip_to_armfold_line": {
        "description": "From Shoulder Tip down to Armpit level.",
        "diagram": "Lp5",
        "full_name": "Arm: Shoulder Tip to Armfold line",
        "number": "L16"
    },
    "arm_shoulder_tip_to_elbow": {
        "description": "From Shoulder tip to Elbow Tip, arm straight.",
        "diagram": "Lp2",
        "full_name": "Arm: Shoulder Tip to Elbow",
        "number": "L06"
    },
    "arm_shoulder_tip_to_elbow_bent": {
        "description": "Bend Arm, measure from Shoulder Tip to Elbow Tip.",
        "diagram": "Lp1",
        "full_name": "Arm: Shoulder Tip to Elbow, bent",
        "number": "L02"
    },
    "arm_shoulder_tip_to_wrist": {
        "description": "From Shoulder Tip to Wrist bone, arm straight.",
        "diagram": "Lp2",
        "full_name": "Arm: Shoulder Tip to Wrist",
        "number": "L05"
    },
    "arm_shoulder_tip_to_wrist_bent": {
        "description": "Bend Arm, measure from Shoulder Tip around Elbow to radial Wrist bone.",
        "diagram": "Lp1",
        "full_name": "Arm: Shoulder Tip to Wrist, bent",
        "number": "L01"
    },
    "arm_upper_circ": {
        "description": "Arm circumference at Armpit level.",
        "diagram": "Lp4",
        "full_name": "Arm: Upper Arm circumference",
        "number": "L11"
    },
    "arm_wrist_circ": {
        "description": "Wrist circumference.",
        "diagram": "Lp4",
        "full_name": "Arm: Wrist circumference",
        "number": "L15"
    },
    "armfold_to_armfold_b": {
        "description": "De la îndoitura Mâinii la îndoitura Mâinii peste spate.",
        "diagram": "Ip3",
        "full_name": "Mână îndoitură la Mână îndoitură, spate",
        "number": "I09"
    },
    "armfold_to_armfold_bust": {
        "description": "Measure in a curve from Armfold Left Front through Bust Front curved back up to Armfold Right Front.",
        "diagram": "Pp9",
        "full_name": "Armfold to Armfold, front, curved through Bust Front",
        "number": "P09"
    },
    "armfold_to_armfold_f": {
        "description": "De la îndoitura Mâinii la îndoitura Mâinii, cea mai scurtă distanță între îndoiturile Mâinilor, nefiind paralel cu podeaua. ",
        "diagram": "Ip1",
        "full_name": "Mână răscroitură la Mână răscroitură față",
        "number": "I04"
    },
    "armfold_to_bust_front": {
        "description": "Measure from Armfold Front to Bust Front, shortest distance between the two, as straight as possible.",
        "diagram": "Pp10",
        "full_name": "Armfold to Bust Front",
        "number": "P10"
    },
    "armpit_to_waist_side": {
        "description": "De la Subraț până la talie lateral.",
        "diagram": "Hp3",
        "full_name": "De la subraț la talie laterală",
        "number": "H03"
    },
    "armscye_arc": {
        "description": "From Armscye at Across Chest over ShoulderTip  to Armscye at Across Back.",
        "diagram": "Pp12",
        "full_name": "Armscye: Arc",
        "number": "P12"
    },
    "armscye_circ": {
        "description": "Let arm hang at side. Measure Armscye circumference through Shoulder Tip and Armpit.",
        "diagram": "Lp8",
        "full_name": "Armscye: Circumference",
        "number": "L19"
    },
    "armscye_length": {
        "description": "Vertical distance from Shoulder Tip to Armpit.",
        "diagram": "Lp8",
        "full_name": "Armscye: Length",
        "number": "L20"
    },
    "armscye_width": {
        "description": "Horizontal distance between Armscye Front and Armscye Back.",
        "diagram": "Lp9",
        "full_name": "Armscye: Width",
        "number": "L21"
    },
    "body_armfold_circ": {
        "description": "Măsură în jurul brațelor și a trunchiului la nivelul îndoiturii măinilor.",
        "diagram": "Gp7",
        "full_name": "Circumferința Corpului la nivelul îndoiturii mâinii (nivelul cotului)",
        "number": "G43"
    },
    "body_bust_circ": {
        "description": "Măsura în jurul brațelor și a trunchiului la nivelul pieptului.",
        "diagram": "Gp7",
        "full_name": "Circumferința corpului la nivelul Pieptului",
        "number": "G44"
    },
    "body_torso_circ": {
        "description": "Circumferința în jurul trunchiului de la mijlocul umărului în jos printre picioare înapoi până la mijlocul umărului.",
        "diagram": "Gp8",
        "full_name": "Circumferința corpului unde trunchiul este cel mai mare",
        "number": "G45"
    },
    "bust_arc_b": {
        "description": "Dintr-o parte in cealaltă a Pieptului prin spate. ('Piept circumferință' - 'Piept curbură, față').",
        "diagram": "Gp4",
        "full_name": "Piept curbură, spate",
        "number": "G28"
    },
    "bust_arc_f": {
        "description": "De la subraț peste Piept la subraț.",
        "diagram": "Gp2",
        "full_name": "Piept curbura, față",
        "number": "G12"
    },
    "bust_arc_half_b": {
        "description": "Jumătate din 'Piept curbură, spate'. ('Piept curbură, spate' / 2).",
        "diagram": "Gp5",
        "full_name": "Piept curbură, jumătate, spate",
        "number": "G36"
    },
    "bust_arc_half_f": {
        "description": "Jumătate din 'Piept curbură, față'. ('Piept curbură, față' / 2).",
        "diagram": "Gp3",
        "full_name": "Piept curbură, jumătate, față",
        "number": "G20"
    },
    "bust_circ": {
        "description": "Circumferința peste piept, paralel cu podeaua.",
        "diagram": "Gp1",
        "full_name": "Piept circumferință",
        "number": "G04"
    },
    "bust_to_waist_b": {
        "description": "De la Piept spate în jos la nivelul Taliei.\n('Gât spate la Talie spate' - 'Gât spate la Piept spate').",
        "diagram": "Hp7",
        "full_name": "Piept spate la Talie spate",
        "number": "H24"
    },
    "bust_to_waist_f": {
        "description": "De la Piept față în jos la nivelul Taliei. ('Gât față la Talie față' - 'Gât față la Piept față').",
        "diagram": "Hp4",
        "full_name": "Piept față la Talie față",
        "number": "H10"
    },
    "bustpoint_neck_side_to_waist": {
        "description": "From Neck Side to Bustpoint, then straight down to Waist level. ('Neck Side to Bustpoint' + 'Bustpoint to Waist level').",
        "diagram": "Jp3",
        "full_name": "Bustpoint, Neck Side to Waist level",
        "number": "J06"
    },
    "bustpoint_to_bustpoint": {
        "description": "De la Piept vârf la Piept vârf",
        "diagram": "Jp1",
        "full_name": "Piept vârf la Piept vârf",
        "number": "J01"
    },
    "bustpoint_to_bustpoint_half": {
        "description": "Half of 'Bustpoint to Bustpoint'. ('Bustpoint to Bustpoint' / 2).",
        "diagram": "Jp2",
        "full_name": "Bustpoint to Bustpoint, half",
        "number": "J05"
    },
    "bustpoint_to_bustpoint_halter": {
        "description": "From Bustpoint around Neck Back down to other Bustpoint.",
        "diagram": "Jp5",
        "full_name": "Bustpoint to Bustpoint Halter",
        "number": "J09"
    },
    "bustpoint_to_lowbust": {
        "description": "De la Piept vârf în jos la nivel Piept inferior, urmând curba pieptului.",
        "diagram": "Jp1",
        "full_name": "Piept vârf la Piept Inferior",
        "number": "J03"
    },
    "bustpoint_to_neck_side": {
        "description": "De la Gât lateral la Piept Vârf.",
        "diagram": "Jp1",
        "full_name": "Piept vârf la Gât lateral",
        "number": "J02"
    },
    "bustpoint_to_shoulder_center": {
        "description": "From center of Shoulder to Bustpoint.",
        "diagram": "Jp6",
        "full_name": "Bustpoint to Shoulder Center",
        "number": "J10"
    },
    "bustpoint_to_shoulder_tip": {
        "description": "From Bustpoint to Shoulder tip.",
        "diagram": "Jp4",
        "full_name": "Bustpoint to Shoulder Tip",
        "number": "J07"
    },
    "bustpoint_to_waist": {
        "description": "De la Piept vârf drept în jos la nivelul Taliei, formând o linie dreaptă (nu o linie curbata după corp).",
        "diagram": "Jp1",
        "full_name": "Piept vârf la nivel Talie",
        "number": "J04"
    },
    "bustpoint_to_waist_front": {
        "description": "From Bustpoint to Waist Front, in a straight line, not following the curves of the body.",
        "diagram": "Jp4",
        "full_name": "Bustpoint to Waist Front",
        "number": "J08"
    },
    "crotch_length": {
        "description": "Put tape across gap between buttocks at Hip level. Measure from Waist Front down betwen legs and up to Waist Back.",
        "diagram": "Np1",
        "full_name": "Crotch length",
        "number": "N01"
    },
    "crotch_length_b": {
        "description": "Put tape across gap between buttocks at Hip level. Measure from Waist Back to mid-Crotch, either at the vagina or between testicles and anus).",
        "diagram": "Np2",
        "full_name": "Crotch length, back",
        "number": "N02"
    },
    "crotch_length_f": {
        "description": "From Waist Front to start of vagina or end of testicles. ('Crotch length' - 'Crotch length, back').",
        "diagram": "Np2",
        "full_name": "Crotch length, front",
        "number": "N03"
    },
    "dart_width_bust": {
        "description": "This information is pulled from pattern charts in some patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp2",
        "full_name": "Dart Width: Bust",
        "number": "Q02"
    },
    "dart_width_shoulder": {
        "description": "This information is pulled from pattern charts in some patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp1",
        "full_name": "Dart Width: Shoulder",
        "number": "Q01"
    },
    "dart_width_waist": {
        "description": "This information is pulled from pattern charts in some  patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp3",
        "full_name": "Dart Width: Waist",
        "number": "Q03"
    },
    "foot_circ": {
        "description": "Măsura circumferinței in jurul celei mai mari parți a labei piciorului.",
        "diagram": "Ep2",
        "full_name": "Labă Picior: Circumferință",
        "number": "E03"
    },
    "foot_instep_circ": {
        "description": "Măsura Circumferinței celei mai înalte parți a scobiturii gleznei.",
        "diagram": "Ep2",
        "full_name": "Labă Picior: Înălțime Circumferință",
        "number": "E04"
    },
    "foot_length": {
        "description": "Măsură de la spatele călcâiului până la vârful celui mai lung deget de la picior.",
        "diagram": "Ep2",
        "full_name": "Labă Picior: Lungime",
        "number": "E02"
    },
    "foot_width": {
        "description": "Măsura celei mai mari parți a labei piciorului.",
        "diagram": "Ep1",
        "full_name": "Labă Picior: Lățime",
        "number": "E01"
    },
    "hand_circ": {
        "description": "Îndreaptă degetul mare spre degetul mic, adu degetele aproape împreună.\nMăsura circumferinta in jurul celei mai mare parți ale mâinii.",
        "diagram": "Dp3",
        "full_name": "Mână: Circumferința",
        "number": "D05"
    },
    "hand_length": {
        "description": "Lungimea de la linia de încheietura a măinii până la vârful degetului mijlociu.",
        "diagram": "Dp1",
        "full_name": "Mână: Lungime",
        "number": "D02"
    },
    "hand_palm_circ": {
        "description": "Circumferința Palmei în cel mai lat loc.",
        "diagram": "Dp2",
        "full_name": "Mâna: Circumferința Palmei",
        "number": "D04"
    },
    "hand_palm_length": {
        "description": "Lungimea de la linia de încheietura a mâinii până la baza degetului mijlociu.",
        "diagram": "Dp1",
        "full_name": "Mâna: lungime Palmă",
        "number": "D01"
    },
    "hand_palm_width": {
        "description": "Măsura Palmei unde este cea mai lată.",
        "diagram": "Dp1",
        "full_name": "Mâna: lățime Palma",
        "number": "D03"
    },
    "head_chin_to_neck_back": {
        "description": "Distanța verticală de la bărbie până la spatele gâtului.\n('Înălțime' - 'Înălțime: Spate Gât' - 'Cap: Lungime')",
        "diagram": "Fp3",
        "full_name": "Cap: Bărbie la Spate Gât",
        "number": "F06"
    },
    "head_circ": {
        "description": "Măsura cele mai mari Circumferințe a capului.",
        "diagram": "Fp1",
        "full_name": "Cap: Circumerință",
        "number": "F01"
    },
    "head_crown_to_neck_back": {
        "description": "Distanța verticală de la Creștet la spatele gâtului.\n('Înălțime: Total' - 'Înălțime: Spate Gât').",
        "diagram": "Fp3",
        "full_name": "Cap: Creștet până la Spatele Gâtului",
        "number": "F05"
    },
    "head_depth": {
        "description": "Distanța orizontală de la frunte la ceafă.",
        "diagram": "Fp1",
        "full_name": "Cap: Adâncime",
        "number": "F03"
    },
    "head_length": {
        "description": "Distanța verticală de la creștetul capului până la partea de jos a maxilarului.",
        "diagram": "Fp1",
        "full_name": "Cap: Lungime",
        "number": "F02"
    },
    "head_width": {
        "description": "Distanța orizontală (lățimea) a capului dintr-o parte in cealaltă în partea cea mai largă.",
        "diagram": "Fp2",
        "full_name": "Cap: Lățime",
        "number": "F04"
    },
    "height": {
        "description": "Distanța pe verticală de la creștetul capului la podea.",
        "diagram": "Ap1",
        "full_name": "Înăltime: Totală",
        "number": "A01"
    },
    "height_ankle": {
        "description": "Distanța verticală de la punctul unde partea frontală a piciorului se întâlnește cu laba piciorului până la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Gleznă",
        "number": "A11"
    },
    "height_ankle_high": {
        "description": "Distanta verticala de la cea mai mare adâncitură din spatele gleznei la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Înălțime Gleznă",
        "number": "A10"
    },
    "height_armpit": {
        "description": "Distanța verticală de la subraț la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Subraț",
        "number": "A04"
    },
    "height_bustpoint": {
        "description": "Distanța verticală de la Punctul Pieptului până la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Punctul Pieptului",
        "number": "A14"
    },
    "height_calf": {
        "description": "Distanța verticală de la cel mai gros punct al gambei la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Gambă",
        "number": "A09"
    },
    "height_gluteal_fold": {
        "description": "Distanța verticală de la faldul fesei, unde mușchiul fesei întâlnește partea de sus a spatelui coapsei pâna la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Fald Fesă",
        "number": "A07"
    },
    "height_highhip": {
        "description": "Distanța verticală de la nivelul Șoldului Înalt, unde parte de abdomen din față este cea mai proeminentă, până la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Șold Înalt",
        "number": "A12"
    },
    "height_hip": {
        "description": "Distanța verticală de la Șold la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Șold",
        "number": "A06"
    },
    "height_knee": {
        "description": "Distanța verticală de la partea din spate a genunchiului la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Genunchi",
        "number": "A08"
    },
    "height_knee_to_ankle": {
        "description": "Distanta verticala de la faldul din partea din spate a genunchiului până la punctul unde piciorul din față întâlnește partea de sus a labei piciorului.",
        "diagram": "Ap2",
        "full_name": "Înălțime: Genunchi la Gleznă",
        "number": "A21"
    },
    "height_neck_back": {
        "description": "Distanța verticală de la Ceafă (vertebra cervicală) la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Gât Spate",
        "number": "A02"
    },
    "height_neck_back_to_knee": {
        "description": "Distanța verticală de la Spatele Gătului (vertebre cervicale) până la faldul din partea din spate a genunchiului.",
        "diagram": "Ap2",
        "full_name": "Înălțime: Spate Gât pâna la Genunchi",
        "number": "A18"
    },
    "height_neck_back_to_waist_side": {
        "description": "Distanța verticală de la Gât la Talie\n('Înălțime: Gât' - 'Înălțime: Talie').",
        "diagram": "Ap2",
        "full_name": "Înălțime: Spate Gât la Talia Laterală",
        "number": "A22"
    },
    "height_neck_front": {
        "description": "Distanța verticală de la Gât Față la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Gât Față",
        "number": "A16"
    },
    "height_neck_side": {
        "description": "Distanța verticală de la laterala gâtului la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime:  Laterală Gât",
        "number": "A17"
    },
    "height_scapula": {
        "description": "Distanța verticală de la omoplat (punctul placii omoplatului) la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Omoplat",
        "number": "A03"
    },
    "height_shoulder_tip": {
        "description": "Distanța verticală de la vîrful umărului la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Vîrf Umăr",
        "number": "A15"
    },
    "height_waist_back": {
        "description": "Vertical height from Waist Back to floor. ('Height: Waist Front'' - 'Leg: Crotch to floor'').",
        "diagram": "Ap2",
        "full_name": "Height: Waist Back",
        "number": "A23"
    },
    "height_waist_front": {
        "description": "Distanța verticală de la talie în față până la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Talie Față",
        "number": "A13"
    },
    "height_waist_side": {
        "description": "Distanța verticală de la Talia Laterală la podea.",
        "diagram": "Ap1",
        "full_name": "Înălțime: Talie Lateral",
        "number": "A05"
    },
    "height_waist_side_to_hip": {
        "description": "Distanța verticală de la Talia Laterală până la nivelul Șoldului.",
        "diagram": "Ap2",
        "full_name": "Înălțime: Talie Laterală la Șold",
        "number": "A20"
    },
    "height_waist_side_to_knee": {
        "description": "Distanța verticală a Taliei Laterale la faldul din partea din spate a genunchiului.",
        "diagram": "Ap2",
        "full_name": "Înălțime: Talie Laterală la Genunchi",
        "number": "A19"
    },
    "highbust_arc_b": {
        "description": "Dintr-o parte in cealalta a Pieptului Superior pe la spate. ('Piept superior circumferința' - 'Piept superior curbură, față')",
        "diagram": "Gp4",
        "full_name": "Piept superior curbură, spate",
        "number": "G27"
    },
    "highbust_arc_f": {
        "description": "De la subraț peste Pieptul Superior la subraț.",
        "diagram": "Gp2",
        "full_name": "Piept superior curbură, prin față",
        "number": "G11"
    },
    "highbust_arc_half_b": {
        "description": "Jumătate din 'Piept superior curbură, spate'. ('Piept superior curbură, spate' / 2).",
        "diagram": "Gp5",
        "full_name": "Piept superior curbură, jumătate, spate",
        "number": "G35"
    },
    "highbust_arc_half_f": {
        "description": "Jumătate de masură a 'Piept superior curbură, față'. De la Pieptul superior față la Piept superior lateral. ('Piept superior curbură, față' / 2)",
        "diagram": "Gp3",
        "full_name": "Piept superior curbură, jumătate, față",
        "number": "G19"
    },
    "highbust_b_over_shoulder_to_highbust_f": {
        "description": "From Highbust Back, over Shoulder, then aim at Bustpoint, stopping measurement at Highbust level.",
        "diagram": "Pp11",
        "full_name": "Highbust Back, over Shoulder, to Highbust level",
        "number": "P11"
    },
    "highbust_back_over_shoulder_to_armfold_front": {
        "description": "From Highbust Back over Shoulder to Armfold Front.",
        "diagram": "Pp4",
        "full_name": "Highbust Back, over Shoulder, to Armfold Front",
        "number": "P04"
    },
    "highbust_back_over_shoulder_to_waist_front": {
        "description": "From Highbust Back, over Shoulder touching  Neck Side, to Waist Front.",
        "diagram": "Pp5",
        "full_name": "Highbust Back, over Shoulder, to Waist Front",
        "number": "P05"
    },
    "highbust_circ": {
        "description": "Circumferinta Pieptului Superior, se măsoară distanța cea mai scurtă pe sub brațe peste piept, pe la subsuori.",
        "diagram": "Gp1",
        "full_name": "Piept superior Circumferință",
        "number": "G03"
    },
    "highbust_to_waist_b": {
        "description": "De la Piept superior spate la Talie spate.\n('Gât spate la Talie spate' - 'Gât spate la Piept superior spate').",
        "diagram": "Hp7",
        "full_name": "Piept superior Spate la Talie Spate",
        "number": "H22"
    },
    "highbust_to_waist_f": {
        "description": "De la Piept superior față la Talie Față. Se folosește o banda de legatură intre Punctele Pieptului. ('Gât față la Talie față' - 'Gât față la Piept superior față').",
        "diagram": "Hp4",
        "full_name": "Piept superior față la Talie față",
        "number": "H08"
    },
    "highhip_arc_b": {
        "description": "Dintr-o parte in cealaltă a Șoldului înalt prin spate. ('Șold înalt circumferință' - 'Șold înalt curbură, față').",
        "diagram": "Gp4",
        "full_name": "Șold înalt, curbură, spate",
        "number": "G32"
    },
    "highhip_arc_f": {
        "description": "Dintr-o parte a Șoldului înalt in cealaltă parte, prin față.",
        "diagram": "Gp2",
        "full_name": "Șold înalt, curbură, față",
        "number": "G16"
    },
    "highhip_arc_half_b": {
        "description": "Jumătate din 'Șold înalt curbură, spate'. De la Șold înalt spate pâna la Piept înalt lateral ('Șold înalt curbură, spate' / 2).",
        "diagram": "Gp5",
        "full_name": "Șold înalt curbură, jumătate, spate",
        "number": "G40"
    },
    "highhip_arc_half_f": {
        "description": "Jumătate din 'Șold înalt curbură, față'. ('Șold înalt curbură, față' / 2).",
        "diagram": "Gp3",
        "full_name": "Șold înalt, curbură, jumătate, față",
        "number": "G24"
    },
    "highhip_circ": {
        "description": "Circumferinta în jurul Șoldului înalt, unde proeminența Abdomenului este cea mai mare, paralel cu podeaua.",
        "diagram": "Gp1",
        "full_name": "Șold înalt circumferința",
        "number": "G08"
    },
    "hip_arc_b": {
        "description": "Dintr-o parte in cealaltă a Șoldului prin spate. ('Șold circumferință' - 'Șold curbură, față').",
        "diagram": "Gp4",
        "full_name": "Șold curbură, spate",
        "number": "G33"
    },
    "hip_arc_f": {
        "description": "Dintr-o parte a Șoldului in cealaltă parte, prin față.",
        "diagram": "Gp2",
        "full_name": "Șold curbură, față",
        "number": "G17"
    },
    "hip_arc_half_b": {
        "description": "Jumătate din 'Șold curbură, spate'. (Șold curbură, spate'' / 2)",
        "diagram": "Gp5",
        "full_name": "Șold curbură, jumătate, spate",
        "number": "G41"
    },
    "hip_arc_half_f": {
        "description": "Jumătate din 'Șold curbură, față'. ('Șold curbură, față' / 2).",
        "diagram": "Gp3",
        "full_name": "Șold curbură, jumătate, față",
        "number": "G25"
    },
    "hip_circ": {
        "description": "Circumferința în jurul Șoldului unde proeminența Șoldului este cea mai mare, paralel cu podeaua.",
        "diagram": "Gp1",
        "full_name": "Șold circumferința",
        "number": "G09"
    },
    "hip_circ_with_abdomen": {
        "description": "Measurement at Hip level, including the depth of the Abdomen. (Hip arc, back + Hip arc with abdomen, front).",
        "diagram": "Gp9",
        "full_name": "Hip circumference, including Abdomen",
        "number": "G46"
    },
    "hip_with_abdomen_arc_f": {
        "description": "Curbează o bucată de hârtie rigidă în jurul abdomenului în partea din fața, și se fixează lateral.\nMăsoară dintr-o parte pâna in cealaltă peste hârtie.",
        "diagram": "Gp6",
        "full_name": "Șold curbură cu Abdomenul, față",
        "number": "G42"
    },
    "indent_ankle_high": {
        "description": "Distanța orizontală dintre un băț drept, plasat perpendicular pe toc, și cea mai mare proeminență a gleznei.",
        "diagram": "Cp2",
        "full_name": "Marcare: înălțime Gleznă",
        "number": "C03"
    },
    "indent_neck_back": {
        "description": "Distanța orizontală de la Omoplat (punctul placa omoplat) la spatele gâtului.",
        "diagram": "Cp1",
        "full_name": "Marcare: Gât Spate",
        "number": "C01"
    },
    "indent_waist_back": {
        "description": "Distanța orizontală dintre un băț drept, plasat să atingă Șoldul și Omoplatiul și Talia la spate.",
        "diagram": "Cp2",
        "full_name": "Marcare: Talie Spate",
        "number": "C02"
    },
    "leg_ankle_circ": {
        "description": "Ankle circumference where front of leg meets the top of the foot.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle circumference",
        "number": "M09"
    },
    "leg_ankle_diag_circ": {
        "description": "Ankle circumference diagonal from top of foot to bottom of heel.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle diagonal circumference",
        "number": "M11"
    },
    "leg_ankle_high_circ": {
        "description": "Ankle circumference where the indentation at the back of the ankle is the deepest.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle High circumference",
        "number": "M08"
    },
    "leg_calf_circ": {
        "description": "Calf circumference at the largest part of lower leg.",
        "diagram": "Mp2",
        "full_name": "Leg: Calf circumference",
        "number": "M07"
    },
    "leg_crotch_to_ankle": {
        "description": "From Crotch to Ankle. ('Leg: Crotch to Floor' - 'Height: Ankle').",
        "diagram": "Mp3",
        "full_name": "Leg: Crotch to Ankle",
        "number": "M12"
    },
    "leg_crotch_to_floor": {
        "description": "Stand feet close together. Measure from crotch level (touching body, no extra space) down to floor.",
        "diagram": "Mp1",
        "full_name": "Leg: Crotch to floor",
        "number": "M01"
    },
    "leg_knee_circ": {
        "description": "Knee circumference with straight leg.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee circumference",
        "number": "M05"
    },
    "leg_knee_circ_bent": {
        "description": "Knee circumference with leg bent.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee circumference, bent",
        "number": "M10"
    },
    "leg_knee_small_circ": {
        "description": "Leg circumference just below the knee.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee Small circumference",
        "number": "M06"
    },
    "leg_thigh_mid_circ": {
        "description": "Thigh circumference about halfway between Crotch and Knee.",
        "diagram": "Mp2",
        "full_name": "Leg: Thigh Middle circumference",
        "number": "M04"
    },
    "leg_thigh_upper_circ": {
        "description": "Thigh circumference at the fullest part of the upper Thigh near the Crotch.",
        "diagram": "Mp2",
        "full_name": "Leg: Thigh Upper circumference",
        "number": "M03"
    },
    "leg_waist_side_to_ankle": {
        "description": "From Waist Side to Ankle. ('Leg: Waist Side to Floor' - 'Height: Ankle').",
        "diagram": "Mp3",
        "full_name": "Leg: Waist Side to Ankle",
        "number": "M13"
    },
    "leg_waist_side_to_floor": {
        "description": "From Waist Side along curve to Hip level then straight down to floor.",
        "diagram": "Mp1",
        "full_name": "Leg: Waist Side to floor",
        "number": "M02"
    },
    "leg_waist_side_to_knee": {
        "description": "From Waist Side along curve to Hip level then straight down to  Knee level. ('Leg: Waist Side to Floor' - 'Height Knee').",
        "diagram": "Mp3",
        "full_name": "Leg: Waist Side to Knee",
        "number": "M14"
    },
    "lowbust_arc_b": {
        "description": "Dintr-o parte in cealaltă a Pieptului inferior prin spate. ('Piept inferior circumferință' - 'Piept inferior curbură, față').",
        "diagram": "Gp4",
        "full_name": "Piept inferior curbură, spate",
        "number": "G29"
    },
    "lowbust_arc_f": {
        "description": "De la subraț peste Pieptul inferior (pe sub piept) la subraț.",
        "diagram": "Gp2",
        "full_name": "Piept inferior curbură, față",
        "number": "G13"
    },
    "lowbust_arc_half_b": {
        "description": "Jumătate din 'Piept inferior curbură, spate'. ('Piept inferior curbură, spate' / 2).",
        "diagram": "Gp5",
        "full_name": "Piept inferior curbură, jumătate, spate",
        "number": "G37"
    },
    "lowbust_arc_half_f": {
        "description": "Jumătate din 'Piept inferior curbură, față'. ('Piept inferior curbură, față' / 2).",
        "diagram": "Gp3",
        "full_name": "Piept inferior, curbură, jumătate, față",
        "number": "G21"
    },
    "lowbust_circ": {
        "description": "Circumferința pe sub piept, paralel cu podeaua.",
        "diagram": "Gp1",
        "full_name": "Piept inferior circumferința",
        "number": "G05"
    },
    "lowbust_to_waist_b": {
        "description": "De la Piept inferior spate în jos la Talie spate.",
        "diagram": "Hp7",
        "full_name": "Piept inferior spate la Talie spate",
        "number": "H25"
    },
    "lowbust_to_waist_f": {
        "description": "De la Piept inferior față în jos la Talie față",
        "diagram": "Hp4",
        "full_name": "Piept inferior față la Talie față",
        "number": "H11"
    },
    "neck_arc_b": {
        "description": "Dintr-o parte a gâtului in cealaltă parte prin spate. ('Gât circumferință' - 'Gât curbură, față').",
        "diagram": "Gp4",
        "full_name": "Gât curbură, spate",
        "number": "G26"
    },
    "neck_arc_f": {
        "description": " Dintr-o parte laterală a gâtului in cealaltă parte laterală prin față.",
        "diagram": "Gp2",
        "full_name": "Gât curbura, în față",
        "number": "G10"
    },
    "neck_arc_half_b": {
        "description": "Jumătate din 'Gât curbură, spate'. ('Gât curbură, spate' / 2)",
        "diagram": "Gp5",
        "full_name": "Găt curbură, jumătate, spate",
        "number": "G34"
    },
    "neck_arc_half_f": {
        "description": "Jumătate de masura a curburii Gâtului, prin față. ('Gât curbură, față' / 2).",
        "diagram": "Gp3",
        "full_name": "Gât curbură, jumătate, față",
        "number": "G18"
    },
    "neck_back_to_across_back": {
        "description": "De la Gât spate, în jos la nivelul măsurii Peste Spate",
        "diagram": "Hp13",
        "full_name": "Gât Spate la Peste Spate",
        "number": "H41"
    },
    "neck_back_to_armfold_front": {
        "description": "From Neck Back over Shoulder to Armfold Front.",
        "diagram": "Pp2",
        "full_name": "Neck Back to Armfold Front",
        "number": "P02"
    },
    "neck_back_to_armfold_front_to_highbust_back": {
        "description": "From Neck Back over Shoulder to Armfold Front, under arm to Highbust Back.",
        "diagram": "Pp8",
        "full_name": "Neck Back, to Armfold Front, to Highbust Back",
        "number": "P08"
    },
    "neck_back_to_armfold_front_to_neck_back": {
        "description": "From Neck Back, over Shoulder to Armfold Front, under arm and return to start.",
        "diagram": "Pp6",
        "full_name": "Neck Back, to Armfold Front, to Neck Back",
        "number": "P06"
    },
    "neck_back_to_armfold_front_to_waist_side": {
        "description": "From Neck Back, over Shoulder, down chest to Waist Side.",
        "diagram": "Pp3",
        "full_name": "Neck Back, over Shoulder, to Waist Side",
        "number": "P03"
    },
    "neck_back_to_bust_b": {
        "description": "De la Gât spate în jos la Piept spate.",
        "diagram": "Hp7",
        "full_name": "Gât spate la Piept spate",
        "number": "H23"
    },
    "neck_back_to_bust_front": {
        "description": "From Neck Back, over Shoulder, to Bust Front.",
        "diagram": "Pp1",
        "full_name": "Neck Back to Bust Front",
        "number": "P01"
    },
    "neck_back_to_highbust_b": {
        "description": "De la Gât spate la Piept superior Spate.",
        "diagram": "Hp7",
        "full_name": "Gât spate la Piept superior Spate",
        "number": "H21"
    },
    "neck_back_to_shoulder_tip_b": {
        "description": "De la Gât Spate la Umăr Vârf",
        "diagram": "Ip6",
        "full_name": "Gât Spate la Umăr Vârf",
        "number": "I13"
    },
    "neck_back_to_waist_b": {
        "description": "De la Gât spate în ​​jos la Talie spate.",
        "diagram": "Hp6",
        "full_name": "Gât spate la Talie spate",
        "number": "H19"
    },
    "neck_back_to_waist_front": {
        "description": "From Neck Back around Neck Side down to Waist Front.",
        "diagram": "Op1",
        "full_name": "Neck Back to Waist Front",
        "number": "O01"
    },
    "neck_back_to_waist_side": {
        "description": "From Neck Back diagonal across back to Waist Side.",
        "diagram": "Kp5",
        "full_name": "Neck Back to Waist Side",
        "number": "K06"
    },
    "neck_circ": {
        "description": "Circumferința Gâtului la baza gâtului, atingând spatele gâtului, lateralele gâtului și partea din față a gâtului.",
        "diagram": "Gp1",
        "full_name": "Circumferința Gât",
        "number": "G02"
    },
    "neck_front_to_bust_f": {
        "description": "De la Gât față în jos pâna la Piept față. Necesar o banda de a face legătura între Punctele Pieptului.",
        "diagram": "Hp4",
        "full_name": "Gât față la Piept față",
        "number": "H09"
    },
    "neck_front_to_highbust_f": {
        "description": "Gât anterior în jos până la Piept superior față.",
        "diagram": "Hp4",
        "full_name": "Gât anterior la Piept superior față",
        "number": "H07"
    },
    "neck_front_to_shoulder_tip_f": {
        "description": "De la Gâr Față la Umăr Vârf.",
        "diagram": "Ip5",
        "full_name": "Gâr Față la Umăr Vârf",
        "number": "I12"
    },
    "neck_front_to_waist_f": {
        "description": "De la gât anterior, peste bandă printre sâni, până la talie față.",
        "diagram": "Hp1",
        "full_name": "Gât anterior până la talie anterior",
        "number": "H01"
    },
    "neck_front_to_waist_flat_f": {
        "description": "De la gât în ​​jos prin față între sâni la talie față.",
        "diagram": "Hp2",
        "full_name": "Gât anterior la talie anterior întins",
        "number": "H02"
    },
    "neck_front_to_waist_side": {
        "description": "From Neck Front diagonal to Waist Side.",
        "diagram": "Kp2",
        "full_name": "Neck Front to Waist Side",
        "number": "K02"
    },
    "neck_mid_circ": {
        "description": "Circumferința Gâtului la mijloc, aproximativ jumatatea distanței intre maxilar și bust.",
        "diagram": "Gp1",
        "full_name": "Gât la mijloc, circumferința",
        "number": "G01"
    },
    "neck_side_to_armfold_b": {
        "description": "De la laterala Gâtului pe diagonală la îndoitura Mâinii spate.",
        "diagram": "Kp9",
        "full_name": "Gât lateral la Mână îndoitură spate",
        "number": "K11"
    },
    "neck_side_to_armfold_f": {
        "description": "De la laterala Gâtului pe diagonală la îndoitura Mâinii prin față.",
        "diagram": "Kp6",
        "full_name": "Gât lateral la Mână îndoitură față",
        "number": "K08"
    },
    "neck_side_to_armpit_b": {
        "description": "From Neck Side diagonal across back to Highbust Side (Armpit).",
        "diagram": "Kp10",
        "full_name": "Neck Side to Highbust Side, back",
        "number": "K12"
    },
    "neck_side_to_armpit_f": {
        "description": "From Neck Side diagonal across front to Highbust Side (Armpit).",
        "diagram": "Kp7",
        "full_name": "Neck Side to Highbust Side, front",
        "number": "K09"
    },
    "neck_side_to_bust_b": {
        "description": "De la gât lateral drept în jos la nivelul Pieptului.",
        "diagram": "Hp8",
        "full_name": "Gât lateral la nivel Piept, spate",
        "number": "H27"
    },
    "neck_side_to_bust_f": {
        "description": "De la gât lateral în ​​jos prin față direct la nivelul Pieptului.",
        "diagram": "Hp5",
        "full_name": "Gât lateral la piept, față",
        "number": "H14"
    },
    "neck_side_to_bust_side_b": {
        "description": "Neck Side diagonal across back to Bust Side.",
        "diagram": "Kp11",
        "full_name": "Neck Side to Bust Side, back",
        "number": "K13"
    },
    "neck_side_to_bust_side_f": {
        "description": "Neck Side diagonal across front to Bust Side.",
        "diagram": "Kp8",
        "full_name": "Neck Side to Bust Side, front",
        "number": "K10"
    },
    "neck_side_to_highbust_b": {
        "description": "De la gât lateral direct în ​​jos prin spate la nivelul Pieptului superior.",
        "diagram": "Hp8",
        "full_name": "Gât lateral la nivel piept superior, spate",
        "number": "H28"
    },
    "neck_side_to_highbust_f": {
        "description": "De la gât lateral în ​​jos față direct la nivelul Pieptului superior.",
        "diagram": "Hp5",
        "full_name": "Gât lateral la nivel piept superior, față",
        "number": "H15"
    },
    "neck_side_to_waist_b": {
        "description": "De la gât lateral drept în jos prin spate la talie.",
        "diagram": "Hp6",
        "full_name": "Gât lateral la talie, spate",
        "number": "H18"
    },
    "neck_side_to_waist_bustpoint_f": {
        "description": "From Neck Side over Bustpoint to Waist level, forming a straight line.",
        "diagram": "Hp3",
        "full_name": "Gâtul lateral la nivelul taliei, prin punctul Pieptului",
        "number": "H06"
    },
    "neck_side_to_waist_f": {
        "description": "De la gât direct în ​​jos prin față până la nivelul taliei.",
        "diagram": "Hp3",
        "full_name": "Gât lateral până la nivelul taliei, față",
        "number": "H05"
    },
    "neck_side_to_waist_scapula_b": {
        "description": "De la Gât lateral peste Omoplat până la nivelul Taliei, formând o linie dreaptă.",
        "diagram": "Hp6",
        "full_name": "Gât lateral la Talie, peste Omoplat",
        "number": "H20"
    },
    "neck_side_to_waist_side_b": {
        "description": "From Neck Side diagonal across back to Waist Side.",
        "diagram": "Kp5",
        "full_name": "Neck Side to Waist Side, back",
        "number": "K07"
    },
    "neck_side_to_waist_side_f": {
        "description": "From Neck Side diagonal across front to Waist Side.",
        "diagram": "Kp2",
        "full_name": "Neck Side to Waist Side, front",
        "number": "K03"
    },
    "neck_width": {
        "description": "Se măsoară lățimea între părțile atârnânde a unui șnur atârnat după gât.",
        "diagram": "Ip7",
        "full_name": "Gât Lățime",
        "number": "I14"
    },
    "rib_arc_b": {
        "description": "Dintr-o parte in cealaltă a Toracelui prin spate. ('Torace circumferință' - 'Torace curbură, față').",
        "diagram": "Gp4",
        "full_name": "Torace curbură, spate",
        "number": "G30"
    },
    "rib_arc_f": {
        "description": "Dintr-o parte a toracelui in cealalta parte prin față.",
        "diagram": "Gp2",
        "full_name": "Torace curbură, față",
        "number": "G14"
    },
    "rib_arc_half_b": {
        "description": "Jumătate din 'Torace curbură, spate'. ('Torace curbură, spate' / 2).",
        "diagram": "Gp5",
        "full_name": "Torace curbură, jumătate, spate",
        "number": "G38"
    },
    "rib_arc_half_f": {
        "description": "Jumătate din 'Torace curbură, față'. ('Torace curbură, față' / 2).",
        "diagram": "Gp3",
        "full_name": "Torace curbură, jumătate, față",
        "number": "G22"
    },
    "rib_circ": {
        "description": "Circumferința peste torace în partea cea mai inferioara a coastelor,  paralel cu podeaua.",
        "diagram": "Gp1",
        "full_name": "Torace circumferința",
        "number": "G06"
    },
    "rib_to_waist_side": {
        "description": "De la cel mai jos punct al Toracelui lateral în jos la Talie",
        "diagram": "Hp4",
        "full_name": "Torace lateral la Talie lateral",
        "number": "H12"
    },
    "rise_length_b": {
        "description": "Vertical distance from Waist Back to Crotch level. ('Height: Waist Back' - 'Leg: Crotch to Floor')",
        "diagram": "Np4",
        "full_name": "Rise length, back",
        "number": "N06"
    },
    "rise_length_diag": {
        "description": "Measure from Waist Side diagonally to a string tied at the top of the leg, seated on a hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, diagonal",
        "number": "N05"
    },
    "rise_length_f": {
        "description": "Vertical Distance from Waist Front to Crotch level. ('Height: Waist Front' - 'Leg: Crotch to Floor')",
        "diagram": "Np4",
        "full_name": "Rise length, front",
        "number": "N07"
    },
    "rise_length_side": {
        "description": "Vertical distance from Waist side down to Crotch level. Use formula (Height: Waist side - Leg: Crotch to floor).",
        "diagram": "Np5",
        "full_name": "Rise length, side",
        "number": "N08"
    },
    "rise_length_side_sitting": {
        "description": "From Waist Side around hp curve down to surface, while seated on hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, side, sitting",
        "number": "N04"
    },
    "shoulder_center_to_highbust_b": {
        "description": "De la mijlocul Umărului în jos prin spate de nivelul Pieptului superior, peste Omoplat.",
        "diagram": "Hp8",
        "full_name": "Umăr centru la nivel Piept superior, spate",
        "number": "H29"
    },
    "shoulder_center_to_highbust_f": {
        "description": "De la mijlocul umărului în jos prin față de nivelul Pieptului superior, se vizează punctul Pieptului.",
        "diagram": "Hp5",
        "full_name": "Umăr mijloc la nivel Piept superior, față",
        "number": "H16"
    },
    "shoulder_length": {
        "description": "De la Umăr lateral la Umăr Vârf",
        "diagram": "Ip1",
        "full_name": "Umăr lungime",
        "number": "I01"
    },
    "shoulder_slope_neck_back_angle": {
        "description": "Unghiul format de linia de la gât spate la vârful umărului și linia de la gât spate paralel cu podeaua.",
        "diagram": "Hp11",
        "full_name": "Umăr înclinare unghi de la Gât spate",
        "number": "H38"
    },
    "shoulder_slope_neck_back_height": {
        "description": "Distanța verticală între Gât spate și vârful Umărului.",
        "diagram": "Hp11",
        "full_name": "Umăr lungime înclinare de la Gât Spate ",
        "number": "H39"
    },
    "shoulder_slope_neck_side_angle": {
        "description": "Unghiul format de linia de la Gât lateral la vârful Umărului și linia de la Gât lateral paralel cu podeaua.",
        "diagram": "Hp11",
        "full_name": "Umăr la Gât înclinare unghi lateral",
        "number": "H36"
    },
    "shoulder_slope_neck_side_length": {
        "description": "Distanța verticală între Gât lateral și vârful Umărului.",
        "diagram": "Hp11",
        "full_name": "Umăr lungime înclinare de la Gâr lateral ",
        "number": "H37"
    },
    "shoulder_slope_shoulder_tip_angle": {
        "description": "Unghi format de linia de la Gât lateral la Umăr Vârf si linia  verticală la Umăr Vârf",
        "diagram": "Hp12",
        "full_name": "Umăr Unghi Înclinare de la Umăr Vârf",
        "number": "H40"
    },
    "shoulder_tip_to_armfold_b": {
        "description": "De la vârful Umărului pe lânga răscroitura brațului în jos la îndoitura Mâinii spate",
        "diagram": "Hp8",
        "full_name": "Umăr vârf la Mână îndoitura spate",
        "number": "H26"
    },
    "shoulder_tip_to_armfold_f": {
        "description": "De la Vârful Umarului pe lângă răscroitura brațului în jos la îndoitura Mânei față",
        "diagram": "Hp5",
        "full_name": "Umăr vârf la Mâna îndoitura față",
        "number": "H13"
    },
    "shoulder_tip_to_shoulder_tip_b": {
        "description": "De la Umăr Vârf la Umăr Vârf, peste Spate",
        "diagram": "Ip3",
        "full_name": "Umăr Vârf la Umăr Vârf, spate",
        "number": "I07"
    },
    "shoulder_tip_to_shoulder_tip_f": {
        "description": "De la Umăr Vârf la Umăr Vârf, prin față",
        "diagram": "Ip1",
        "full_name": "Umăr Vârf la Umăr Vârf, față",
        "number": "I02"
    },
    "shoulder_tip_to_shoulder_tip_half_b": {
        "description": "Jumătate din 'Umăr Vârf la Umăr Vârf, spate'. ('Umăr Vârf la Umăr Vârf, spate' / 2).",
        "diagram": "Ip4",
        "full_name": "Umăr Vârf la Umăr Vârf, jumătate, spate",
        "number": "I10"
    },
    "shoulder_tip_to_shoulder_tip_half_f": {
        "description": "Jumătate din 'Umăr Vârf la Umăr Vârf, față'. ('Umăr Vârf la Umăr Vârf, față' / 2).",
        "diagram": "Ip2",
        "full_name": "Umăr Vârf la Umăr Vârf, jumătate, față",
        "number": "I05"
    },
    "shoulder_tip_to_waist_b_1in_offset": {
        "description": "Mark 1in (2.54cm) outward from Waist Back along Waist level. Measure from Shoulder Tip diagonal to mark.",
        "diagram": "Kp4",
        "full_name": "Shoulder Tip to Waist Back, with 1in (2.54cm) offset",
        "number": "K05"
    },
    "shoulder_tip_to_waist_back": {
        "description": "From Shoulder Tip diagonal to Waist Back.",
        "diagram": "Kp3",
        "full_name": "Shoulder Tip to Waist Back",
        "number": "K04"
    },
    "shoulder_tip_to_waist_front": {
        "description": "From Shoulder Tip diagonal to Waist Front.",
        "diagram": "Kp1",
        "full_name": "Shoulder Tip to Waist Front",
        "number": "K01"
    },
    "shoulder_tip_to_waist_side_b": {
        "description": "De la vârful umărului, curbat în jurul răscroiturii brațului prin spate, apoi în jos la talie lateral.",
        "diagram": "Hp6",
        "full_name": "Umăr vârf la talie lateral, spate",
        "number": "H17"
    },
    "shoulder_tip_to_waist_side_f": {
        "description": "De la vârful umărului, curbat în jurul răscroiturii mâinii prin față în jos până la talie lateral.",
        "diagram": "Hp3",
        "full_name": "Umăr vârf la talie lateral, prin față",
        "number": "H04"
    },
    "waist_arc_b": {
        "description": "Dintr-o parte in cealaltă a Taliei prin spate. ('Talie circumferință' - 'Talie curbură, față').",
        "diagram": "Gp4",
        "full_name": "Talie curbură, spate",
        "number": "G31"
    },
    "waist_arc_f": {
        "description": "Dintr-o parte a Taliei in cealaltă parte prin față.",
        "diagram": "Gp2",
        "full_name": "Talia curbură, prin față",
        "number": "G15"
    },
    "waist_arc_half_b": {
        "description": "Jumătate din 'Talie curbură, spate'. ('Talie curbură, spate' / 2).",
        "diagram": "Gp5",
        "full_name": "Talie curbură, jumătate, spate",
        "number": "G39"
    },
    "waist_arc_half_f": {
        "description": "Jumătate din 'Talie curbură, față'. ('Talie curbură, față / 2').",
        "diagram": "Gp3",
        "full_name": "Talie curbură, jumătate, față",
        "number": "G23"
    },
    "waist_circ": {
        "description": "Circumferința în jurul taliei, urmând conturul natural al taliei. Talia la spate este de obicei mai înaltă.",
        "diagram": "Gp1",
        "full_name": "Talie circumferința",
        "number": "G07"
    },
    "waist_natural_arc_b": {
        "description": "From Side to Side at Natural Waist level, across the back. Calculate as ( Natural Waist circumference  - Natural Waist arc (front) ).",
        "diagram": "Op5",
        "full_name": "Natural Waist arc, back",
        "number": "O05"
    },
    "waist_natural_arc_f": {
        "description": "From Side to Side at the Natural Waist level, across the front.",
        "diagram": "Op4",
        "full_name": "Natural Waist arc, front",
        "number": "O04"
    },
    "waist_natural_circ": {
        "description": "Torso circumference at men's natural side Abdominal Obliques indentation, if Oblique indentation isn't found then just below the Navel level.",
        "diagram": "Op3",
        "full_name": "Natural Waist circumference",
        "number": "O03"
    },
    "waist_to_highhip_b": {
        "description": "De la Talie spate la Șold înalt spate",
        "diagram": "Hp10",
        "full_name": "Talie spate la Șold înalt spate",
        "number": "H33"
    },
    "waist_to_highhip_f": {
        "description": "De la Talie față la Șold înalt față",
        "diagram": "Hp9",
        "full_name": "Talie față la Șold înalt față",
        "number": "H30"
    },
    "waist_to_highhip_side": {
        "description": "De la Talie lateral la Șold înalt lateral",
        "diagram": "Hp9",
        "full_name": "Talie lateral la Șold înalt lateral",
        "number": "H32"
    },
    "waist_to_hip_b": {
        "description": "De la Talie spate în jos pâna la Șold spate. Necesar o banda de a face legătura între Punctele șezutului.",
        "diagram": "Hp10",
        "full_name": "Talie spate la Șold spate",
        "number": "H34"
    },
    "waist_to_hip_f": {
        "description": "De la Talie față la Șold față",
        "diagram": "Hp9",
        "full_name": "Talie față la Șold față",
        "number": "H31"
    },
    "waist_to_hip_side": {
        "description": "De la Talie lateral la Șold lateral",
        "diagram": "Hp10",
        "full_name": "Talie lateral la Șold lateral",
        "number": "H35"
    },
    "waist_to_natural_waist_b": {
        "description": "Length from Waist Back to Natural Waist Back.",
        "diagram": "Op7",
        "full_name": "Waist Back to Natural Waist Back",
        "number": "O07"
    },
    "waist_to_natural_waist_f": {
        "description": "Length from Waist Front to Natural Waist Front.",
        "diagram": "Op6",
        "full_name": "Waist Front to Natural Waist Front",
        "number": "O06"
    },
    "waist_to_waist_halter": {
        "description": "From Waist level around Neck Back to Waist level.",
        "diagram": "Op2",
        "full_name": "Waist to Waist Halter, around Neck Back",
        "number": "O02"
    },
    "width_abdomen_to_hip": {
        "description": "Distanța orizontală de la cea mai mare proeminență a abdomenului la cea mai mare proeminență a șoldului.",
        "diagram": "Bp2",
        "full_name": "Lățime: Abdomen la Șold",
        "number": "B05"
    },
    "width_bust": {
        "description": "Distanța orizontală din partea stângă la Piept în partea dreaptă la Piept.",
        "diagram": "Bp1",
        "full_name": "Lățime: Piept",
        "number": "B02"
    },
    "width_hip": {
        "description": "Distanța orizontală de la Șold la Șold.",
        "diagram": "Bp1",
        "full_name": "Lățime: Șold",
        "number": "B04"
    },
    "width_shoulder": {
        "description": "Distanța orizontală de la extremitate umăr la extremitate umăr.",
        "diagram": "Bp1",
        "full_name": "Lățime: Umăr",
        "number": "B01"
    },
    "width_waist": {
        "description": "Distanța orizontală dintr-o parte în cealaltă a Taliei.",
        "diagram": "Bp1",
        "full_name": "Lățime: Talie",
        "number": "B03"
    }
}
