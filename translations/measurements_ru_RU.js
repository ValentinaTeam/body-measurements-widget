module.exports = {
    "across_back_b": {
        "description": "From Armscye to Armscye at the narrowest width of the back.",
        "diagram": "Ip3",
        "full_name": "Across Back",
        "number": "I08"
    },
    "across_back_center_to_armfold_front_to_across_back_center": {
        "description": "From center of Across Back, over Shoulder, under Arm, and return to start.",
        "diagram": "Pp7",
        "full_name": "Across Back Center, circled around Shoulder",
        "number": "P07"
    },
    "across_back_half_b": {
        "description": "Half of 'Across Back'. ('Across Back' / 2).",
        "diagram": "Ip4",
        "full_name": "Across Back, half",
        "number": "I11"
    },
    "across_back_to_waist_b": {
        "description": "From middle of Across Back down to Waist back.",
        "diagram": "Hp13",
        "full_name": "Across Back to Waist back",
        "number": "H42"
    },
    "across_chest_f": {
        "description": "From Armscye to Armscye at narrowest width across chest.",
        "diagram": "Ip1",
        "full_name": "Ширина груди первая",
        "number": "I03"
    },
    "across_chest_half_f": {
        "description": "Half of 'Across Chest'. ('Across Chest' / 2).",
        "diagram": "Ip2",
        "full_name": "Across Chest, half",
        "number": "I06"
    },
    "arm_above_elbow_circ": {
        "description": "Arm circumference at Bicep level.",
        "diagram": "Lp4",
        "full_name": "Arm: Above Elbow circumference",
        "number": "L12"
    },
    "arm_across_back_center_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Middle of Back to Elbow Tip.",
        "diagram": "Op10",
        "full_name": "Arm: Across Back Center to Elbow, high bend",
        "number": "O12"
    },
    "arm_across_back_center_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Middle of Back to Elbow Tip to Wrist bone.",
        "diagram": "Op10",
        "full_name": "Arm: Across Back Center to Wrist, high bend",
        "number": "O13"
    },
    "arm_armpit_to_elbow": {
        "description": "From Armpit to inner Elbow, arm straight.",
        "diagram": "Lp3",
        "full_name": "Arm: Armpit to Elbow, inside",
        "number": "L09"
    },
    "arm_armpit_to_wrist": {
        "description": "From Armpit to ulna Wrist bone, arm straight.",
        "diagram": "Lp3",
        "full_name": "Внутренняя длина рукава (с окатом)",
        "number": "L08"
    },
    "arm_armscye_back_center_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Armscye Back to Elbow Tip.",
        "diagram": "Op11",
        "full_name": "Arm: Armscye Back Center to Wrist, high bend",
        "number": "O14"
    },
    "arm_elbow_circ": {
        "description": "Elbow circumference, arm straight.",
        "diagram": "Lp4",
        "full_name": "Обхват локтя",
        "number": "L13"
    },
    "arm_elbow_circ_bent": {
        "description": "Elbow circumference, arm is bent.",
        "diagram": "Lp1",
        "full_name": "Arm: Elbow circumference, bent",
        "number": "L04"
    },
    "arm_elbow_to_wrist": {
        "description": "From Elbow to Wrist, arm straight. ('Arm: Shoulder Tip to Wrist' - 'Arm: Shoulder Tip to Elbow').",
        "diagram": "Lp2",
        "full_name": "Arm: Elbow to Wrist",
        "number": "L07"
    },
    "arm_elbow_to_wrist_bent": {
        "description": "Elbow tip to wrist. ('Arm: Shoulder Tip to Wrist, bent' - 'Arm: Shoulder Tip to Elbow, bent').",
        "diagram": "Lp1",
        "full_name": "Arm: Elbow to Wrist, bent",
        "number": "L03"
    },
    "arm_elbow_to_wrist_inside": {
        "description": "From inside Elbow to Wrist. ('Arm: Armpit to Wrist, inside' - 'Arm: Armpit to Elbow, inside').",
        "diagram": "Lp3",
        "full_name": "Arm: Elbow to Wrist, inside",
        "number": "L10"
    },
    "arm_lower_circ": {
        "description": "Arm circumference where lower arm is widest.",
        "diagram": "Lp4",
        "full_name": "Arm: Lower Arm circumference",
        "number": "L14"
    },
    "arm_neck_back_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Back to Elbow Tip.",
        "diagram": "Op8",
        "full_name": "Arm: Neck Back to Elbow, high bend",
        "number": "O08"
    },
    "arm_neck_back_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Back to Elbow Tip to Wrist bone.",
        "diagram": "Op8",
        "full_name": "Arm: Neck Back to Wrist, high bend",
        "number": "O09"
    },
    "arm_neck_side_to_elbow_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Side to Elbow Tip.",
        "diagram": "Op9",
        "full_name": "Arm: Neck Side to Elbow, high bend",
        "number": "O10"
    },
    "arm_neck_side_to_finger_tip": {
        "description": "From Neck Side down arm to tip of middle finger. ('Shoulder Length' + 'Arm: Shoulder Tip to Wrist' + 'Hand: Length').",
        "diagram": "Lp7",
        "full_name": "Расстояние от точки основания шеи сбоку до конца третьего пальца",
        "number": "L18"
    },
    "arm_neck_side_to_outer_elbow": {
        "description": "From Neck Side over Shoulder Tip down to Elbow. (Shoulder length + Arm: Shoulder Tip to Elbow).",
        "diagram": "Lp10",
        "full_name": "Arm: Neck side to Elbow",
        "number": "L22"
    },
    "arm_neck_side_to_wrist": {
        "description": "Номер размерного признака в стандарте №33. Измеряют от точки основания шеи сбоку через плечевую точку до линии обхвата запястья.",
        "diagram": "Lp6",
        "full_name": "Расстояние от точки основания шеи сбоку до лучевой точки",
        "number": "L17"
    },
    "arm_neck_side_to_wrist_bent": {
        "description": "Bend Arm with Elbow out, hand in front. Measure from Neck Side to Elbow Tip to Wrist bone.",
        "diagram": "Op9",
        "full_name": "Arm: Neck Side to Wrist, high bend",
        "number": "O11"
    },
    "arm_shoulder_tip_to_armfold_line": {
        "description": "From Shoulder Tip down to Armpit level.",
        "diagram": "Lp5",
        "full_name": "Arm: Shoulder Tip to Armfold line",
        "number": "L16"
    },
    "arm_shoulder_tip_to_elbow": {
        "description": "Измеряют расстояние от плечевой точки до лучевой точки.",
        "diagram": "Lp2",
        "full_name": "Длина руки до локтя",
        "number": "L06"
    },
    "arm_shoulder_tip_to_elbow_bent": {
        "description": "Bend Arm, measure from Shoulder Tip to Elbow Tip.",
        "diagram": "Lp1",
        "full_name": "Arm: Shoulder Tip to Elbow, bent",
        "number": "L02"
    },
    "arm_shoulder_tip_to_wrist": {
        "description": "Измеряют расстояние от плечевой точки до линии обхвата запястья.",
        "diagram": "Lp2",
        "full_name": "Длина рукава от плеча до линии обхвата запястья",
        "number": "L05"
    },
    "arm_shoulder_tip_to_wrist_bent": {
        "description": "Bend Arm, measure from Shoulder Tip around Elbow to radial Wrist bone.",
        "diagram": "Lp1",
        "full_name": "Arm: Shoulder Tip to Wrist, bent",
        "number": "L01"
    },
    "arm_upper_circ": {
        "description": "Номер размерного признака в стандарте №28. Измеряют перпендикулярно оси плеча. Лента Верхним краем должна касаться заднего угла подмышечной впадины и замыкаться на наружной поверхности руки. Размер читают по верхнему краю ленты.",
        "diagram": "Lp4",
        "full_name": "Обхват плеча",
        "number": "L11"
    },
    "arm_wrist_circ": {
        "description": "Номер размерного признака в стандарте №29. Измеряют перпендикулярно оси предплечья по лучезапястному суставу через головку локтевой кости. Ленту замыкают на наружной поверхности руки. Размер читают по нижнему краю ленты.",
        "diagram": "Lp4",
        "full_name": "Обхват запястья",
        "number": "L15"
    },
    "armfold_to_armfold_b": {
        "description": "Номер размерного признака в стандарте №47. Измеряют по поверхности тела ресстояние между задними углами подмышечных впадин. Нижний край ленты должен быть расположен на уровне отметки заднего угла подмышечной впадины на позвоночнике.",
        "diagram": "Ip3",
        "full_name": "Ширина спины",
        "number": "I09"
    },
    "armfold_to_armfold_bust": {
        "description": "Measure in a curve from Armfold Left Front through Bust Front curved back up to Armfold Right Front.",
        "diagram": "Pp9",
        "full_name": "Armfold to Armfold, front, curved through Bust Front",
        "number": "P09"
    },
    "armfold_to_armfold_f": {
        "description": "Номер размерного признака в стандарте №45. Измеряют по поверхности тела расстояние между передними углами подмышечных впадин. Нижний край ленты должен касаться отметки, сделанной при измерении обхвата груди первого.",
        "diagram": "Ip1",
        "full_name": "Ширина груди",
        "number": "I04"
    },
    "armfold_to_bust_front": {
        "description": "Measure from Armfold Front to Bust Front, shortest distance between the two, as straight as possible.",
        "diagram": "Pp10",
        "full_name": "Armfold to Bust Front",
        "number": "P10"
    },
    "armpit_to_waist_side": {
        "description": "From Armpit down to Waist Side.",
        "diagram": "Hp3",
        "full_name": "Длина боковой части",
        "number": "H03"
    },
    "armscye_arc": {
        "description": "Номер размерного признака в стандарте №38. Измеряют в вертикальной плоскости расстояние от заднего угла подмышечной впадины через высшую точку плечевого сустава до переднего угла подмышечной впадины.",
        "diagram": "Pp12",
        "full_name": "Дуга через высшую точку плечевого сустава",
        "number": "P12"
    },
    "armscye_circ": {
        "description": "Let arm hang at side. Measure Armscye circumference through Shoulder Tip and Armpit.",
        "diagram": "Lp8",
        "full_name": "Armscye: Circumference",
        "number": "L19"
    },
    "armscye_length": {
        "description": "Vertical distance from Shoulder Tip to Armpit.",
        "diagram": "Lp8",
        "full_name": "Armscye: Length",
        "number": "L20"
    },
    "armscye_width": {
        "description": "Измеряют горизонтально на уровне заднего угла подмышечной впадины.",
        "diagram": "Lp9",
        "full_name": "Передне-задний диаметр руки",
        "number": "L21"
    },
    "body_armfold_circ": {
        "description": "Measure around arms and torso at Armfold level.",
        "diagram": "Gp7",
        "full_name": "Body circumference at Armfold level",
        "number": "G43"
    },
    "body_bust_circ": {
        "description": "Measure around arms and torso at Bust level.",
        "diagram": "Gp7",
        "full_name": "Body circumference at Bust level",
        "number": "G44"
    },
    "body_torso_circ": {
        "description": "Circumference around torso from mid-shoulder around crotch back up to mid-shoulder.",
        "diagram": "Gp8",
        "full_name": "Body circumference of full torso",
        "number": "G45"
    },
    "bust_arc_b": {
        "description": "From Bust Side to Bust Side across back. ('Bust circumference' - 'Bust arc, front').",
        "diagram": "Gp4",
        "full_name": "Bust arc, back",
        "number": "G28"
    },
    "bust_arc_f": {
        "description": "Ленту накладывают на лопатки. По спине лента должна проходить горизонтально, касаясь верхним краем задних углов подмышечных впадин, затем по подмышечным впадинам в плоскости косого сечения, спереди — через сосковые точки и замыкаться на правой стороне груди. Мерка записывается в половинном размере.",
        "diagram": "Gp2",
        "full_name": "Полуобхват груди второй",
        "number": "G12"
    },
    "bust_arc_half_b": {
        "description": "Half of 'Bust arc, back'. ('Bust arc, back' / 2).",
        "diagram": "Gp5",
        "full_name": "Bust arc, back, half",
        "number": "G36"
    },
    "bust_arc_half_f": {
        "description": "Half of 'Bust arc, front'. ('Bust arc, front'/2).",
        "diagram": "Gp3",
        "full_name": "Bust arc, front, half",
        "number": "G20"
    },
    "bust_circ": {
        "description": "Номер размерного признака в стандарте №15. Ленту накладывают на лопатки. По спине лента должна проходить горизонтально, касаясь верхним краем задних углов подмышечных впадин, затем по подмышечным впадинам в плоскости косого сечения, спереди — через сосковые точки и замыкаться на правой стороне груди.",
        "diagram": "Gp1",
        "full_name": "Обхват груди второй",
        "number": "G04"
    },
    "bust_to_waist_b": {
        "description": "From Bust Back down to Waist level. ('Neck Back to Waist Back' - 'Neck Back to Bust Back').",
        "diagram": "Hp7",
        "full_name": "Bust Back to Waist Back",
        "number": "H24"
    },
    "bust_to_waist_f": {
        "description": "From Bust Front down to Waist level. ('Neck Front to Waist Front' - 'Neck Front to Bust Front').",
        "diagram": "Hp4",
        "full_name": "Bust Front to Waist Front",
        "number": "H10"
    },
    "bustpoint_neck_side_to_waist": {
        "description": "From Neck Side to Bustpoint, then straight down to Waist level. ('Neck Side to Bustpoint' + 'Bustpoint to Waist level').",
        "diagram": "Jp3",
        "full_name": "Bustpoint, Neck Side to Waist level",
        "number": "J06"
    },
    "bustpoint_to_bustpoint": {
        "description": "Номер размерного признака в стандарте №46. Измеряют между сосковыми точками в горизонтальной плоскости. Размер читают по верхнему краю ленты.",
        "diagram": "Jp1",
        "full_name": "Расстояние между сосковыми точками",
        "number": "J01"
    },
    "bustpoint_to_bustpoint_half": {
        "description": "Half of 'Bustpoint to Bustpoint'. ('Bustpoint to Bustpoint' / 2).",
        "diagram": "Jp2",
        "full_name": "Bustpoint to Bustpoint, half",
        "number": "J05"
    },
    "bustpoint_to_bustpoint_halter": {
        "description": "From Bustpoint around Neck Back down to other Bustpoint.",
        "diagram": "Jp5",
        "full_name": "Bustpoint to Bustpoint Halter",
        "number": "J09"
    },
    "bustpoint_to_lowbust": {
        "description": "From Bustpoint down to Lowbust level, following curve of bust or chest.",
        "diagram": "Jp1",
        "full_name": "Bustpoint to Lowbust",
        "number": "J03"
    },
    "bustpoint_to_neck_side": {
        "description": "Измеряют от точки  основания шеи до выступающей точки грудной железы.",
        "diagram": "Jp1",
        "full_name": "Расстояние от точки основания шеи сбоку до сосковой точки (высота груди)",
        "number": "J02"
    },
    "bustpoint_to_shoulder_center": {
        "description": "From center of Shoulder to Bustpoint.",
        "diagram": "Jp6",
        "full_name": "Bustpoint to Shoulder Center",
        "number": "J10"
    },
    "bustpoint_to_shoulder_tip": {
        "description": "From Bustpoint to Shoulder tip.",
        "diagram": "Jp4",
        "full_name": "Bustpoint to Shoulder Tip",
        "number": "J07"
    },
    "bustpoint_to_waist": {
        "description": "From Bustpoint to straight down to Waist level, forming a straight line (not curving along the body).",
        "diagram": "Jp1",
        "full_name": "Bustpoint to Waist level",
        "number": "J04"
    },
    "bustpoint_to_waist_front": {
        "description": "From Bustpoint to Waist Front, in a straight line, not following the curves of the body.",
        "diagram": "Jp4",
        "full_name": "Bustpoint to Waist Front",
        "number": "J08"
    },
    "crotch_length": {
        "description": "Номер размерного признака в стандарте №77. Измеряют в вертикальной плоскости от линии талии спереди через выступающую точку живота, левую сторону паховой области и тонкую пластину шириной до 2 см, наложенную на ягодичные точки, до линии талии сзади.",
        "diagram": "Np1",
        "full_name": "Длина промежности",
        "number": "N01"
    },
    "crotch_length_b": {
        "description": "Put tape across gap between buttocks at Hip level. Measure from Waist Back to mid-Crotch, either at the vagina or between testicles and anus).",
        "diagram": "Np2",
        "full_name": "Crotch length, back",
        "number": "N02"
    },
    "crotch_length_f": {
        "description": "From Waist Front to start of vagina or end of testicles. ('Crotch length' - 'Crotch length, back').",
        "diagram": "Np2",
        "full_name": "Crotch length, front",
        "number": "N03"
    },
    "dart_width_bust": {
        "description": "This information is pulled from pattern charts in some patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp2",
        "full_name": "Dart Width: Bust",
        "number": "Q02"
    },
    "dart_width_shoulder": {
        "description": "This information is pulled from pattern charts in some patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp1",
        "full_name": "Dart Width: Shoulder",
        "number": "Q01"
    },
    "dart_width_waist": {
        "description": "This information is pulled from pattern charts in some  patternmaking systems, e.g. Winifred P. Aldrich's \"Metric Pattern Cutting\".",
        "diagram": "Qp3",
        "full_name": "Dart Width: Waist",
        "number": "Q03"
    },
    "foot_circ": {
        "description": "Measure circumference around widest part of foot.",
        "diagram": "Ep2",
        "full_name": "Foot: Circumference",
        "number": "E03"
    },
    "foot_instep_circ": {
        "description": "Номер размерного признака в стандарте №51. Ленту накладывают через заднюю наиболее выступающую вниз область пятки и высшую точку стопы, замыкают спереди. Размер читают по верхнему краю ленты.",
        "diagram": "Ep2",
        "full_name": "Обхват подъема стопы",
        "number": "E04"
    },
    "foot_length": {
        "description": "Measure from back of heel to end of longest toe.",
        "diagram": "Ep2",
        "full_name": "Foot: Length",
        "number": "E02"
    },
    "foot_width": {
        "description": "Measure at widest part of foot.",
        "diagram": "Ep1",
        "full_name": "Foot: Width",
        "number": "E01"
    },
    "hand_circ": {
        "description": "Tuck thumb toward smallest finger, bring fingers close together. Measure circumference around widest part of hand.",
        "diagram": "Dp3",
        "full_name": "Обхват кисти",
        "number": "D05"
    },
    "hand_length": {
        "description": "Length from Wrist line to end of middle finger.",
        "diagram": "Dp1",
        "full_name": "Вертикальный диаметр руки",
        "number": "D02"
    },
    "hand_palm_circ": {
        "description": "Circumference where Palm is widest.",
        "diagram": "Dp2",
        "full_name": "Hand: Palm circumference",
        "number": "D04"
    },
    "hand_palm_length": {
        "description": "Length from Wrist line to base of middle finger.",
        "diagram": "Dp1",
        "full_name": "Hand: Palm length",
        "number": "D01"
    },
    "hand_palm_width": {
        "description": "Measure where Palm is widest.",
        "diagram": "Dp1",
        "full_name": "Hand: Palm width",
        "number": "D03"
    },
    "head_chin_to_neck_back": {
        "description": "Vertical distance from Chin to Neck Back. ('Height' - 'Height: Neck Back' - 'Head: Length')",
        "diagram": "Fp3",
        "full_name": "Расстояние от шейной точки до точки основания шеи сбоку по линии измерения обхвата шеи",
        "number": "F06"
    },
    "head_circ": {
        "description": "Номер размерного признака в стандарте №48. Измеряют через точку инион и центры лобных бугров. Ленту замыкают на лбу.",
        "diagram": "Fp1",
        "full_name": "Обхват головы",
        "number": "F01"
    },
    "head_crown_to_neck_back": {
        "description": "Vertical distance from Crown to Neck Back. ('Height: Total' - 'Height: Neck Back').",
        "diagram": "Fp3",
        "full_name": "Head: Crown to Neck Back",
        "number": "F05"
    },
    "head_depth": {
        "description": "Horizontal distance from front of forehead to back of head.",
        "diagram": "Fp1",
        "full_name": "Head: Depth",
        "number": "F03"
    },
    "head_length": {
        "description": "Vertical distance from Head Crown to bottom of jaw.",
        "diagram": "Fp1",
        "full_name": "Высота головы",
        "number": "F02"
    },
    "head_width": {
        "description": "Horizontal distance from Head Side to Head Side, where Head is widest.",
        "diagram": "Fp2",
        "full_name": "Head: Width",
        "number": "F04"
    },
    "height": {
        "description": "Вертикальное расстояние от темени до пола.",
        "diagram": "Ap1",
        "full_name": "Рост",
        "number": "A01"
    },
    "height_ankle": {
        "description": "Вертикальное расстояние от точки переда ноги до уровня пола.",
        "diagram": "Ap1",
        "full_name": "Высота: лодыжка",
        "number": "A11"
    },
    "height_ankle_high": {
        "description": "Вертикальное расстояние от глубокой выемки сзади лодыжки до пола.",
        "diagram": "Ap1",
        "full_name": "Высота: щиколотка",
        "number": "A10"
    },
    "height_armpit": {
        "description": "Номер размерного признака в стандарте №11. Расстояние по вертикали от пола до заднего угла подмышечной впадины. Уровень заднего угла подмышечной впадины переносят отметкой на позвоночник с помощью антропометра.",
        "diagram": "Ap1",
        "full_name": "Высота заднего угла подмышечной впадины",
        "number": "A04"
    },
    "height_bustpoint": {
        "description": "Расстояние по вертикали от сосковой точки до пола.",
        "diagram": "Ap1",
        "full_name": "Высота сосковой точки",
        "number": "A14"
    },
    "height_calf": {
        "description": "Вертикальное расстояние от широкой части икры ноги до пола",
        "diagram": "Ap1",
        "full_name": "Высота: икры",
        "number": "A09"
    },
    "height_gluteal_fold": {
        "description": "Номер размерного признака в стандарте №12. Расстояние по вертикали от пола до середины подъягодичной складки.",
        "diagram": "Ap1",
        "full_name": "Высота подъягодичной складки",
        "number": "A07"
    },
    "height_highhip": {
        "description": "Vertical distance from the Highhip level, where front abdomen is most prominent, to the floor.",
        "diagram": "Ap1",
        "full_name": "Height: Highhip",
        "number": "A12"
    },
    "height_hip": {
        "description": "Вертикальное расстояние от бедра до пола.",
        "diagram": "Ap1",
        "full_name": "Высота: бедро",
        "number": "A06"
    },
    "height_knee": {
        "description": "Вертикальное расстояние от сгиба колена сзади до пола.",
        "diagram": "Ap1",
        "full_name": "Высота коленной точки",
        "number": "A08"
    },
    "height_knee_to_ankle": {
        "description": "Vertical distance from the fold at the back of the knee to the point where the front leg meets the top of the foot.",
        "diagram": "Ap2",
        "full_name": "Height: Knee to Ankle",
        "number": "A21"
    },
    "height_neck_back": {
        "description": "Номер размерного признака в стандарте №10. Расстояние по вертикали от пола до точки основания шеи сзади.",
        "diagram": "Ap1",
        "full_name": "Высота точки основания шеи сзади",
        "number": "A02"
    },
    "height_neck_back_to_knee": {
        "description": "Определяют как разность размерных признаков №9 и №10.",
        "diagram": "Ap2",
        "full_name": "Расстояние от шейной точки до колена",
        "number": "A18"
    },
    "height_neck_back_to_waist_side": {
        "description": "Vertical distance from Neck Back to Waist Side. ('Height: Neck Back' - 'Height: Waist Side').",
        "diagram": "Ap2",
        "full_name": "Height: Neck Back to Waist Side",
        "number": "A22"
    },
    "height_neck_front": {
        "description": "Вертикальное расстояние от шеи спереди до пола.",
        "diagram": "Ap1",
        "full_name": "Высота точки основания шеи спереди до пола",
        "number": "A16"
    },
    "height_neck_side": {
        "description": "Вертикальное расстояние от шеи сбоку до пола.",
        "diagram": "Ap1",
        "full_name": "Высота точки основания шеи сбоку",
        "number": "A17"
    },
    "height_scapula": {
        "description": "Номер размерного признака в стандарте №87. Расстояние по вертикали от пола до лопаточной точки.",
        "diagram": "Ap1",
        "full_name": "Высота лопаточной точки",
        "number": "A03"
    },
    "height_shoulder_tip": {
        "description": "Расстояние по вертикали от плечевой точки до пола.",
        "diagram": "Ap1",
        "full_name": "Высота плечевой точки",
        "number": "A15"
    },
    "height_waist_back": {
        "description": "Vertical height from Waist Back to floor. ('Height: Waist Front'' - 'Leg: Crotch to floor'').",
        "diagram": "Ap2",
        "full_name": "Height: Waist Back",
        "number": "A23"
    },
    "height_waist_front": {
        "description": "Вертикальное расстояние от талии спереди до пола.",
        "diagram": "Ap1",
        "full_name": "Расстояние от линии талии до пола спереди",
        "number": "A13"
    },
    "height_waist_side": {
        "description": "Номер размерного признака в стандарте №7. Расстояние по вертикали от пола до точки уровня талии. Уровень талии переносят отметками с помощью антропометра на позвоночник и середину передней поверхности тела.",
        "diagram": "Ap1",
        "full_name": "Высота линии талии",
        "number": "A05"
    },
    "height_waist_side_to_hip": {
        "description": "Vertical distance from the Waist Side to the Hip level.",
        "diagram": "Ap2",
        "full_name": "Height: Waist Side to Hip",
        "number": "A20"
    },
    "height_waist_side_to_knee": {
        "description": "Vertical distance from the Waist Side to the fold at the back of the knee.",
        "diagram": "Ap2",
        "full_name": "Высота: Талия Сбоку до Колена",
        "number": "A19"
    },
    "highbust_arc_b": {
        "description": "From Highbust Side  to Highbust Side across back. ('Highbust circumference' - 'Highbust arc, front').",
        "diagram": "Gp4",
        "full_name": "Highbust arc, back",
        "number": "G27"
    },
    "highbust_arc_f": {
        "description": "Ленту накладывают на лопатки. По спине лента должна проходить горизонтально, касаясь верхним краем задних углов подмышечной впадин, затем по подмышечным впадинам. Спереди лента должна проходить на уровне передних углов подмышечных впадин и замыкаться на правой стороне груди. По верхнему краю ленты делают отметку спереди над правой сосковой точкой.\nМерка записывается в половинном размере.",
        "diagram": "Gp2",
        "full_name": "Полуобхват груди первый",
        "number": "G11"
    },
    "highbust_arc_half_b": {
        "description": "Half of 'Highbust arc, back'. From Highbust Back to Highbust Side. ('Highbust arc, back' / 2).",
        "diagram": "Gp5",
        "full_name": "Highbust arc, back, half",
        "number": "G35"
    },
    "highbust_arc_half_f": {
        "description": "Half of 'Highbust arc, front'. From Highbust Front to Highbust Side. ('Highbust arc,  front' / 2).",
        "diagram": "Gp3",
        "full_name": "Highbust arc, front, half",
        "number": "G19"
    },
    "highbust_b_over_shoulder_to_highbust_f": {
        "description": "From Highbust Back, over Shoulder, then aim at Bustpoint, stopping measurement at Highbust level.",
        "diagram": "Pp11",
        "full_name": "Длина бретели от выступающего уголка лопатки вертикально до начала грудной железы",
        "number": "P11"
    },
    "highbust_back_over_shoulder_to_armfold_front": {
        "description": "From Highbust Back over Shoulder to Armfold Front.",
        "diagram": "Pp4",
        "full_name": "Highbust Back, over Shoulder, to Armfold Front",
        "number": "P04"
    },
    "highbust_back_over_shoulder_to_waist_front": {
        "description": "From Highbust Back, over Shoulder touching  Neck Side, to Waist Front.",
        "diagram": "Pp5",
        "full_name": "Highbust Back, over Shoulder, to Waist Front",
        "number": "P05"
    },
    "highbust_circ": {
        "description": "Номер размерного признака в стандарте №14. Ленту накладывают на лопатки. По спине лента должна проходить горизонтально, касаясь верхним краем задних углов подмышечной впадин, затем по подмышечным впадинам. Спереди лента должна проходить на уровне передних углов подмышечных впадин и замыкаться на правой стороне груди. По верхнему краю ленты делают отметку спереди над правой сосковой точкой.",
        "diagram": "Gp1",
        "full_name": "Обхват груди первый",
        "number": "G03"
    },
    "highbust_to_waist_b": {
        "description": "From Highbust Back down to Waist Back. ('Neck Back to Waist Back' - 'Neck Back to Highbust Back').",
        "diagram": "Hp7",
        "full_name": "Highbust Back to Waist Back",
        "number": "H22"
    },
    "highbust_to_waist_f": {
        "description": "From Highbust Front to Waist Front. Use tape to bridge gap between Bustpoints. ('Neck Front to Waist Front' - 'Neck Front to Highbust Front').",
        "diagram": "Hp4",
        "full_name": "Длина переда от линии талии вверх по центру до начала ложбины",
        "number": "H08"
    },
    "highhip_arc_b": {
        "description": "From Highhip Side to Highhip Side across back. ('Highhip circumference' - 'Highhip arc, front').",
        "diagram": "Gp4",
        "full_name": "Highhip arc, back",
        "number": "G32"
    },
    "highhip_arc_f": {
        "description": "From Highhip Side to Highhip Side across front.",
        "diagram": "Gp2",
        "full_name": "Highhip arc, front",
        "number": "G16"
    },
    "highhip_arc_half_b": {
        "description": "Half of 'Highhip arc, back'. From Highhip Back to Highbust Side. ('Highhip arc, back'/ 2).",
        "diagram": "Gp5",
        "full_name": "Highhip arc, back, half",
        "number": "G40"
    },
    "highhip_arc_half_f": {
        "description": "Half of 'Highhip arc, front'.  ('Highhip arc, front' / 2).",
        "diagram": "Gp3",
        "full_name": "Highhip arc, front, half",
        "number": "G24"
    },
    "highhip_circ": {
        "description": "Circumference around Highhip, where Abdomen protrusion is  greatest, parallel to floor.",
        "diagram": "Gp1",
        "full_name": "Highhip circumference",
        "number": "G08"
    },
    "hip_arc_b": {
        "description": "From Hip Side to Hip Side across back. ('Hip circumference' - 'Hip arc, front').",
        "diagram": "Gp4",
        "full_name": "Hip arc, back",
        "number": "G33"
    },
    "hip_arc_f": {
        "description": "Размерный признак №20. Ленту накладывают на ягодичные точки. Лента должна проходить горизонтально вокруг туловища и замыкаться на правой стороне туловища",
        "diagram": "Gp2",
        "full_name": "Полуобхват бедер без учета выступания живота",
        "number": "G17"
    },
    "hip_arc_half_b": {
        "description": "Half of 'Hip arc, back'. ('Hip arc, back' / 2).",
        "diagram": "Gp5",
        "full_name": "Hip arc, back, half",
        "number": "G41"
    },
    "hip_arc_half_f": {
        "description": "Half of 'Hip arc, front'. ('Hip arc, front' / 2).",
        "diagram": "Gp3",
        "full_name": "Hip arc, front, half",
        "number": "G25"
    },
    "hip_circ": {
        "description": "Номер размерного признака в стандарте №20. Ленту накладывают горизонтально вокруг туловища на уровне ягодичных точек и замыкают на правой стороне туловища.",
        "diagram": "Gp1",
        "full_name": "Обхват бедер без учета выступания живота",
        "number": "G09"
    },
    "hip_circ_with_abdomen": {
        "description": "Measurement at Hip level, including the depth of the Abdomen. (Hip arc, back + Hip arc with abdomen, front).",
        "diagram": "Gp9",
        "full_name": "Hip circumference, including Abdomen",
        "number": "G46"
    },
    "hip_with_abdomen_arc_f": {
        "description": "Ленту накладывают на ягодичные точки. Лента должна проходить горизонтально вокруг туловища, спереди - по гибкой пластине, приложенной вертикально к животу для учета выступа живота, и замыкаться на правой стороне туловища\n",
        "diagram": "Gp6",
        "full_name": "Полуобхват бедер с учетом выступания живота",
        "number": "G42"
    },
    "indent_ankle_high": {
        "description": "Horizontal Distance betwee a  flat stick, placed perpendicular to Heel, and the greatest indentation of Ankle.",
        "diagram": "Cp2",
        "full_name": "Indent: Ankle High",
        "number": "C03"
    },
    "indent_neck_back": {
        "description": "Размерный признак в стандарте №74.Измеряют по горизонтали расстояние от шейной точки до вертикальной плоскости, касательной паиболее выступающих назад точек обеих лопаток.",
        "diagram": "Cp1",
        "full_name": "Положение корпуса",
        "number": "C01"
    },
    "indent_waist_back": {
        "description": "Horizontal distance between a flat stick, placed to touch Hip and Scapula, and Waist Back.",
        "diagram": "Cp2",
        "full_name": "Indent: Waist Back",
        "number": "C02"
    },
    "leg_ankle_circ": {
        "description": "Ankle circumference where front of leg meets the top of the foot.",
        "diagram": "Mp2",
        "full_name": "Обхват щиколотки",
        "number": "M09"
    },
    "leg_ankle_diag_circ": {
        "description": "Ankle circumference diagonal from top of foot to bottom of heel.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle diagonal circumference",
        "number": "M11"
    },
    "leg_ankle_high_circ": {
        "description": "Ankle circumference where the indentation at the back of the ankle is the deepest.",
        "diagram": "Mp2",
        "full_name": "Leg: Ankle High circumference",
        "number": "M08"
    },
    "leg_calf_circ": {
        "description": "Номер размерного признака в стандарте №23. Ленту накладывают горизонтально вокруг ноги в области максимального развития икроножной мышцы и замыкают на наружной поверхности голени.",
        "diagram": "Mp2",
        "full_name": "Нога: обхват икры",
        "number": "M07"
    },
    "leg_crotch_to_ankle": {
        "description": "From Crotch to Ankle. ('Leg: Crotch to Floor' - 'Height: Ankle').",
        "diagram": "Mp3",
        "full_name": "Leg: Crotch to Ankle",
        "number": "M12"
    },
    "leg_crotch_to_floor": {
        "description": "Номер размерного признака в стандарте №27. Измеряют по внутренней поверхности ноги расстояние от промежности до пола при слегка раздвинутых ногах. Для измерения используют сантиметровую ленту, верхний край которой укреплен на жесткой пластине.",
        "diagram": "Mp1",
        "full_name": "Длина ноги по внутренней поверхности",
        "number": "M01"
    },
    "leg_knee_circ": {
        "description": "Knee circumference with straight leg.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee circumference",
        "number": "M05"
    },
    "leg_knee_circ_bent": {
        "description": "Knee circumference with leg bent.",
        "diagram": "Mp2",
        "full_name": "Обхват колена в согнутом положении ноги",
        "number": "M10"
    },
    "leg_knee_small_circ": {
        "description": "Leg circumference just below the knee.",
        "diagram": "Mp2",
        "full_name": "Leg: Knee Small circumference",
        "number": "M06"
    },
    "leg_thigh_mid_circ": {
        "description": "Thigh circumference about halfway between Crotch and Knee.",
        "diagram": "Mp2",
        "full_name": "Leg: Thigh Middle circumference",
        "number": "M04"
    },
    "leg_thigh_upper_circ": {
        "description": "Номер размерного признака в стандарте №21. Ленту накладывают горизонтально вокруг бедра, касаясь верхним краем подъягодичной складки, и замыкают на наружной поверхности бедра.",
        "diagram": "Mp2",
        "full_name": "Обхват бедра",
        "number": "M03"
    },
    "leg_waist_side_to_ankle": {
        "description": "From Waist Side to Ankle. ('Leg: Waist Side to Floor' - 'Height: Ankle').",
        "diagram": "Mp3",
        "full_name": "Leg: Waist Side to Ankle",
        "number": "M13"
    },
    "leg_waist_side_to_floor": {
        "description": "Номер размерного признака в стандарте №25. Измеряют от точки уровня талии по боковой поверхности бедра вертикально до пола.",
        "diagram": "Mp1",
        "full_name": "Расстояние от линии талии до пола сбоку (длина брюк)",
        "number": "M02"
    },
    "leg_waist_side_to_knee": {
        "description": "From Waist Side along curve to Hip level then straight down to  Knee level. ('Leg: Waist Side to Floor' - 'Height Knee').",
        "diagram": "Mp3",
        "full_name": "Расстояние от линии талии до колена",
        "number": "M14"
    },
    "lowbust_arc_b": {
        "description": "From Lowbust Side to Lowbust Side across back.  ('Lowbust circumference' - 'Lowbust arc, front').",
        "diagram": "Gp4",
        "full_name": "Lowbust arc, back",
        "number": "G29"
    },
    "lowbust_arc_f": {
        "description": "Ленту накладывают горизонтально вокруг туловища через сосковые точки и замыкают на правой стороне груди. Мерка записывается в половинном размере.",
        "diagram": "Gp2",
        "full_name": "Полуобхват груди третий",
        "number": "G13"
    },
    "lowbust_arc_half_b": {
        "description": "Half of 'Lowbust Arc, back'. ('Lowbust arc, back' / 2).",
        "diagram": "Gp5",
        "full_name": "Lowbust arc, back, half",
        "number": "G37"
    },
    "lowbust_arc_half_f": {
        "description": "Half of 'Lowbust arc, front'.  ('Lowbust Arc, front' / 2).",
        "diagram": "Gp3",
        "full_name": "Lowbust arc, front, half",
        "number": "G21"
    },
    "lowbust_circ": {
        "description": "Номер размерного признака в стандарте №16. Ленту накладывают горизонтально вокруг туловища через сосковые точки и замыкают на правой стороне груди.",
        "diagram": "Gp1",
        "full_name": "Обхват груди третий",
        "number": "G05"
    },
    "lowbust_to_waist_b": {
        "description": "From Lowbust Back down to Waist Back.",
        "diagram": "Hp7",
        "full_name": "Lowbust Back to Waist Back",
        "number": "H25"
    },
    "lowbust_to_waist_f": {
        "description": "From Lowbust Front down to Waist Front.",
        "diagram": "Hp4",
        "full_name": "Длина от талии до основания грудных желез",
        "number": "H11"
    },
    "neck_arc_b": {
        "description": "From Neck Side to Neck Side across back. ('Neck circumference' - 'Neck arc, front').",
        "diagram": "Gp4",
        "full_name": "Neck arc, back",
        "number": "G26"
    },
    "neck_arc_f": {
        "description": "Ленту накладывают на основание шеи так, чтобы оба края ленты плотно прилегали к ее поверхности, и замыкают спереди над яремной вырезкой. Размер читают по нижнему краю ленты. Мерка записывается в половинном размере.",
        "diagram": "Gp2",
        "full_name": "Полуобхват шеи",
        "number": "G10"
    },
    "neck_arc_half_b": {
        "description": "Half of 'Neck arc, back'. ('Neck arc, back' / 2).",
        "diagram": "Gp5",
        "full_name": "Neck arc, back, half",
        "number": "G34"
    },
    "neck_arc_half_f": {
        "description": "Half of 'Neck arc, front'. ('Neck arc, front' / 2).",
        "diagram": "Gp3",
        "full_name": "Neck arc, front, half",
        "number": "G18"
    },
    "neck_back_to_across_back": {
        "description": "From neck back, down to level of Across Back measurement.",
        "diagram": "Hp13",
        "full_name": "Neck Back to Across Back",
        "number": "H41"
    },
    "neck_back_to_armfold_front": {
        "description": "From Neck Back over Shoulder to Armfold Front.",
        "diagram": "Pp2",
        "full_name": "Neck Back to Armfold Front",
        "number": "P02"
    },
    "neck_back_to_armfold_front_to_highbust_back": {
        "description": "From Neck Back over Shoulder to Armfold Front, under arm to Highbust Back.",
        "diagram": "Pp8",
        "full_name": "Neck Back, to Armfold Front, to Highbust Back",
        "number": "P08"
    },
    "neck_back_to_armfold_front_to_neck_back": {
        "description": "From Neck Back, over Shoulder to Armfold Front, under arm and return to start.",
        "diagram": "Pp6",
        "full_name": "Neck Back, to Armfold Front, to Neck Back",
        "number": "P06"
    },
    "neck_back_to_armfold_front_to_waist_side": {
        "description": "From Neck Back, over Shoulder, down chest to Waist Side.",
        "diagram": "Pp3",
        "full_name": "Neck Back, over Shoulder, to Waist Side",
        "number": "P03"
    },
    "neck_back_to_bust_b": {
        "description": "From Neck Back down to Bust Back.",
        "diagram": "Hp7",
        "full_name": "Neck Back to Bust Back",
        "number": "H23"
    },
    "neck_back_to_bust_front": {
        "description": "From Neck Back, over Shoulder, to Bust Front.",
        "diagram": "Pp1",
        "full_name": "Neck Back to Bust Front",
        "number": "P01"
    },
    "neck_back_to_highbust_b": {
        "description": "From Neck Back down to Highbust Back.",
        "diagram": "Hp7",
        "full_name": "Расстояние от точки основания шеи сзади до линии обхватов груди первого и второго с учетом выступа лопаток",
        "number": "H21"
    },
    "neck_back_to_shoulder_tip_b": {
        "description": "From Neck Back to Shoulder Tip.",
        "diagram": "Ip6",
        "full_name": "Neck Back to Shoulder Tip",
        "number": "I13"
    },
    "neck_back_to_waist_b": {
        "description": "Номер размерного признака в стандарте №40. Измеряют от точки основания шеи сзади до отметки уровня талии на позвоночнике через пластину.",
        "diagram": "Hp6",
        "full_name": "Длина спины до талии с учетом выступа лопаток",
        "number": "H19"
    },
    "neck_back_to_waist_front": {
        "description": "From Neck Back around Neck Side down to Waist Front.",
        "diagram": "Op1",
        "full_name": "Neck Back to Waist Front",
        "number": "O01"
    },
    "neck_back_to_waist_side": {
        "description": "From Neck Back diagonal across back to Waist Side.",
        "diagram": "Kp5",
        "full_name": "Neck Back to Waist Side",
        "number": "K06"
    },
    "neck_circ": {
        "description": "Номер размерного признака в стандарте №13. Ленту накладывают на основание шеи так, чтобы оба края ленты плотно прилегали к ее поверхности, и замыкают спереди над яремной вырезкой. Размер читают по нижнему краю ленты.",
        "diagram": "Gp1",
        "full_name": "Обхват шеи",
        "number": "G02"
    },
    "neck_front_to_bust_f": {
        "description": "From Neck Front down to Bust Front. Requires tape to cover gap between Bustpoints.",
        "diagram": "Hp4",
        "full_name": "Neck Front to Bust Front",
        "number": "H09"
    },
    "neck_front_to_highbust_f": {
        "description": "Neck Front down to Highbust Front.",
        "diagram": "Hp4",
        "full_name": "Расстояние от точки основания шеи сбоку до линии обхвата груди первого спереди",
        "number": "H07"
    },
    "neck_front_to_shoulder_tip_f": {
        "description": "From Neck Front to Shoulder Tip.",
        "diagram": "Ip5",
        "full_name": "Neck Front to Shoulder Tip",
        "number": "I12"
    },
    "neck_front_to_waist_f": {
        "description": "From Neck Front, over tape between Breastpoints, down to Waist Front.",
        "diagram": "Hp1",
        "full_name": "Neck Front to Waist Front",
        "number": "H01"
    },
    "neck_front_to_waist_flat_f": {
        "description": "From Neck Front down between breasts to Waist Front.",
        "diagram": "Hp2",
        "full_name": "Neck Front to Waist Front flat",
        "number": "H02"
    },
    "neck_front_to_waist_side": {
        "description": "From Neck Front diagonal to Waist Side.",
        "diagram": "Kp2",
        "full_name": "Neck Front to Waist Side",
        "number": "K02"
    },
    "neck_mid_circ": {
        "description": "Circumference of Neck midsection, about halfway between jaw and torso.",
        "diagram": "Gp1",
        "full_name": "Полуобхват шеи для сорочек",
        "number": "G01"
    },
    "neck_side_to_armfold_b": {
        "description": "From Neck Side diagonal to Armfold Back.",
        "diagram": "Kp9",
        "full_name": "Neck Side to Armfold Back",
        "number": "K11"
    },
    "neck_side_to_armfold_f": {
        "description": "Расстояние от шейной точки до уровня заднего угла подмышечной впадины спереди",
        "diagram": "Kp6",
        "full_name": "Высота проймы косая",
        "number": "K08"
    },
    "neck_side_to_armpit_b": {
        "description": "From Neck Side diagonal across back to Highbust Side (Armpit).",
        "diagram": "Kp10",
        "full_name": "Neck Side to Highbust Side, back",
        "number": "K12"
    },
    "neck_side_to_armpit_f": {
        "description": "From Neck Side diagonal across front to Highbust Side (Armpit).",
        "diagram": "Kp7",
        "full_name": "Neck Side to Highbust Side, front",
        "number": "K09"
    },
    "neck_side_to_bust_b": {
        "description": "From Neck Side straight down back to Bust level.",
        "diagram": "Hp8",
        "full_name": "Neck Side to Bust level, back",
        "number": "H27"
    },
    "neck_side_to_bust_f": {
        "description": "From Neck Side straight down front to Bust level.",
        "diagram": "Hp5",
        "full_name": "Neck Side to Bust level, front",
        "number": "H14"
    },
    "neck_side_to_bust_side_b": {
        "description": "Neck Side diagonal across back to Bust Side.",
        "diagram": "Kp11",
        "full_name": "Neck Side to Bust Side, back",
        "number": "K13"
    },
    "neck_side_to_bust_side_f": {
        "description": "Neck Side diagonal across front to Bust Side.",
        "diagram": "Kp8",
        "full_name": "Neck Side to Bust Side, front",
        "number": "K10"
    },
    "neck_side_to_highbust_b": {
        "description": "From Neck Side straight down back to Highbust level.",
        "diagram": "Hp8",
        "full_name": "Neck Side to Highbust level, back",
        "number": "H28"
    },
    "neck_side_to_highbust_f": {
        "description": "From Neck Side straight down front to Highbust level.",
        "diagram": "Hp5",
        "full_name": "Neck Side to Highbust level, front",
        "number": "H15"
    },
    "neck_side_to_waist_b": {
        "description": "From Neck Side straight down back to Waist level.",
        "diagram": "Hp6",
        "full_name": "Neck Side to Waist level, back",
        "number": "H18"
    },
    "neck_side_to_waist_bustpoint_f": {
        "description": "From Neck Side over Bustpoint to Waist level, forming a straight line.",
        "diagram": "Hp3",
        "full_name": "Neck Side to Waist level, through Bustpoint",
        "number": "H06"
    },
    "neck_side_to_waist_f": {
        "description": "Номер размерного признака в стандарте №43. Измеряют параллельно позвоночнику от линии талии сзади до точки основания шеи сбоку.",
        "diagram": "Hp3",
        "full_name": "Расстояние от линии талии сзади до точки основания шеи сбоку",
        "number": "H05"
    },
    "neck_side_to_waist_scapula_b": {
        "description": "From Neck Side across Scapula down to Waist level, forming a straight line.",
        "diagram": "Hp6",
        "full_name": "Neck Side to Waist level, through Scapula",
        "number": "H20"
    },
    "neck_side_to_waist_side_b": {
        "description": "From Neck Side diagonal across back to Waist Side.",
        "diagram": "Kp5",
        "full_name": "Neck Side to Waist Side, back",
        "number": "K07"
    },
    "neck_side_to_waist_side_f": {
        "description": "From Neck Side diagonal across front to Waist Side.",
        "diagram": "Kp2",
        "full_name": "Neck Side to Waist Side, front",
        "number": "K03"
    },
    "neck_width": {
        "description": "Номер размерного признака в стандарте №54. Измеряют между точками основания шеи сбоку.",
        "diagram": "Ip7",
        "full_name": "Поперечный диаметр шеи",
        "number": "I14"
    },
    "rib_arc_b": {
        "description": "From Rib Side to Rib side across back. ('Rib circumference' - 'Rib arc, front').",
        "diagram": "Gp4",
        "full_name": "Rib arc, back",
        "number": "G30"
    },
    "rib_arc_f": {
        "description": "From Rib Side to Rib Side, across front.",
        "diagram": "Gp2",
        "full_name": "Rib arc, front",
        "number": "G14"
    },
    "rib_arc_half_b": {
        "description": "Half of 'Rib arc, back'. ('Rib arc, back' / 2).",
        "diagram": "Gp5",
        "full_name": "Rib arc, back, half",
        "number": "G38"
    },
    "rib_arc_half_f": {
        "description": "Half of 'Rib arc, front'.   ('Rib Arc, front' / 2).",
        "diagram": "Gp3",
        "full_name": "Rib arc, front, half",
        "number": "G22"
    },
    "rib_circ": {
        "description": "Circumference around Ribs at level of the lowest rib at the side, parallel to floor.",
        "diagram": "Gp1",
        "full_name": "Rib circumference",
        "number": "G06"
    },
    "rib_to_waist_side": {
        "description": "From lowest rib at side down to Waist Side.",
        "diagram": "Hp4",
        "full_name": "Rib Side to Waist Side",
        "number": "H12"
    },
    "rise_length_b": {
        "description": "Vertical distance from Waist Back to Crotch level. ('Height: Waist Back' - 'Leg: Crotch to Floor')",
        "diagram": "Np4",
        "full_name": "Rise length, back",
        "number": "N06"
    },
    "rise_length_diag": {
        "description": "Measure from Waist Side diagonally to a string tied at the top of the leg, seated on a hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, diagonal",
        "number": "N05"
    },
    "rise_length_f": {
        "description": "Vertical Distance from Waist Front to Crotch level. ('Height: Waist Front' - 'Leg: Crotch to Floor')",
        "diagram": "Np4",
        "full_name": "Rise length, front",
        "number": "N07"
    },
    "rise_length_side": {
        "description": "Vertical distance from Waist side down to Crotch level. Use formula (Height: Waist side - Leg: Crotch to floor).",
        "diagram": "Np5",
        "full_name": "Расстояние от линии талии до подъягодичной складки",
        "number": "N08"
    },
    "rise_length_side_sitting": {
        "description": "From Waist Side around hp curve down to surface, while seated on hard surface.",
        "diagram": "Np3",
        "full_name": "Rise length, side, sitting",
        "number": "N04"
    },
    "shoulder_center_to_highbust_b": {
        "description": "From mid-Shoulder down back to Highbust level, aimed through Scapula.",
        "diagram": "Hp8",
        "full_name": "Shoulder center to Highbust level, back",
        "number": "H29"
    },
    "shoulder_center_to_highbust_f": {
        "description": "From mid-Shoulder down front to Highbust level, aimed at Bustpoint.",
        "diagram": "Hp5",
        "full_name": "Shoulder center to Highbust level, front",
        "number": "H16"
    },
    "shoulder_length": {
        "description": "Номер размерного признака в стандарте №31. Измеряют от точки основания шеи сбоку до плечевой точки.",
        "diagram": "Ip1",
        "full_name": "Длина плечевого ската",
        "number": "I01"
    },
    "shoulder_slope_neck_back_angle": {
        "description": "Angle formed by  line from Neck Back to Shoulder Tip and line from Neck Back parallel to floor.",
        "diagram": "Hp11",
        "full_name": "Shoulder Slope Angle from Neck Back",
        "number": "H38"
    },
    "shoulder_slope_neck_back_height": {
        "description": "Vertical distance between Neck Back and Shoulder Tip.",
        "diagram": "Hp11",
        "full_name": "Shoulder Slope length from Neck Back",
        "number": "H39"
    },
    "shoulder_slope_neck_side_angle": {
        "description": "Angle formed by line from Neck Side to Shoulder Tip and line from Neck Side parallel to floor.",
        "diagram": "Hp11",
        "full_name": "Shoulder Slope Angle from Neck Side",
        "number": "H36"
    },
    "shoulder_slope_neck_side_length": {
        "description": "Vertical distance between Neck Side and Shoulder Tip.",
        "diagram": "Hp11",
        "full_name": "Shoulder Slope length from Neck Side",
        "number": "H37"
    },
    "shoulder_slope_shoulder_tip_angle": {
        "description": "Angle formed by line from Neck Side to Shoulder Tip and vertical line at Shoulder Tip.",
        "diagram": "Hp12",
        "full_name": "Shoulder Slope Angle from Shoulder Tip",
        "number": "H40"
    },
    "shoulder_tip_to_armfold_b": {
        "description": "From Shoulder Tip around Armscye down to Armfold Back.",
        "diagram": "Hp8",
        "full_name": "Shoulder Tip to Armfold Back",
        "number": "H26"
    },
    "shoulder_tip_to_armfold_f": {
        "description": "From Shoulder Tip around Armscye down to Armfold Front.",
        "diagram": "Hp5",
        "full_name": "Shoulder Tip to Armfold Front",
        "number": "H13"
    },
    "shoulder_tip_to_shoulder_tip_b": {
        "description": "From Shoulder Tip to Shoulder Tip, across the back.",
        "diagram": "Ip3",
        "full_name": "Дуга плечевого пояса сзади",
        "number": "I07"
    },
    "shoulder_tip_to_shoulder_tip_f": {
        "description": "Номер размерного признака в стандарте №53. Измеряют спереди между плечевыми точками без деформации мягких тканей.",
        "diagram": "Ip1",
        "full_name": "Плечевой диаметр",
        "number": "I02"
    },
    "shoulder_tip_to_shoulder_tip_half_b": {
        "description": "Half of 'Shoulder Tip to Shoulder Tip, back'. ('Shoulder Tip to Shoulder Tip,  back' / 2).",
        "diagram": "Ip4",
        "full_name": "Shoulder Tip to Shoulder Tip, back, half",
        "number": "I10"
    },
    "shoulder_tip_to_shoulder_tip_half_f": {
        "description": "Half of' Shoulder Tip to Shoulder tip, front'. ('Shoulder Tip to Shoulder Tip, front' / 2).",
        "diagram": "Ip2",
        "full_name": "Shoulder Tip to Shoulder Tip, front, half",
        "number": "I05"
    },
    "shoulder_tip_to_waist_b_1in_offset": {
        "description": "Mark 1in (2.54cm) outward from Waist Back along Waist level. Measure from Shoulder Tip diagonal to mark.",
        "diagram": "Kp4",
        "full_name": "Shoulder Tip to Waist Back, with 1in (2.54cm) offset",
        "number": "K05"
    },
    "shoulder_tip_to_waist_back": {
        "description": "Номер размерного признака в стандарте №41. Измеряют кратчайшее расстояние от отметки уровня талии на позвоночнике до плечевой точки.",
        "diagram": "Kp3",
        "full_name": "Высота плеча косая",
        "number": "K04"
    },
    "shoulder_tip_to_waist_front": {
        "description": "Измеряют от выступающей точки грудной железы до плечевой точки.",
        "diagram": "Kp1",
        "full_name": "Высота плеча косая спереди",
        "number": "K01"
    },
    "shoulder_tip_to_waist_side_b": {
        "description": "From Shoulder Tip, curving around Armscye Back, then down to Waist Side.",
        "diagram": "Hp6",
        "full_name": "Shoulder Tip to Waist Side, back",
        "number": "H17"
    },
    "shoulder_tip_to_waist_side_f": {
        "description": "From Shoulder Tip, curving around Armscye Front, then down to Waist Side.",
        "diagram": "Hp3",
        "full_name": "Shoulder Tip to Waist Side, front",
        "number": "H04"
    },
    "waist_arc_b": {
        "description": "From Waist Side to Waist Side across back. ('Waist circumference' - 'Waist arc, front').",
        "diagram": "Gp4",
        "full_name": "Waist arc, back",
        "number": "G31"
    },
    "waist_arc_f": {
        "description": "Ленту накладывают горизонтально вокруг туловища на уровне линии талии и замыкают спереди. Мерка записывается в половинном размере.",
        "diagram": "Gp2",
        "full_name": "Полуобхват талии",
        "number": "G15"
    },
    "waist_arc_half_b": {
        "description": "Half of 'Waist arc, back'. ('Waist  arc, back' / 2).",
        "diagram": "Gp5",
        "full_name": "Waist arc, back, half",
        "number": "G39"
    },
    "waist_arc_half_f": {
        "description": "Half of 'Waist arc, front'. ('Waist arc, front' / 2).",
        "diagram": "Gp3",
        "full_name": "Waist arc, front, half",
        "number": "G23"
    },
    "waist_circ": {
        "description": "Номер размерного признака в стандарте №18. Ленту накладывают горизонтально вокруг туловища на уровне линии талии и замыкают спереди.",
        "diagram": "Gp1",
        "full_name": "Обхват талии",
        "number": "G07"
    },
    "waist_natural_arc_b": {
        "description": "From Side to Side at Natural Waist level, across the back. Calculate as ( Natural Waist circumference  - Natural Waist arc (front) ).",
        "diagram": "Op5",
        "full_name": "Natural Waist arc, back",
        "number": "O05"
    },
    "waist_natural_arc_f": {
        "description": "From Side to Side at the Natural Waist level, across the front.",
        "diagram": "Op4",
        "full_name": "Natural Waist arc, front",
        "number": "O04"
    },
    "waist_natural_circ": {
        "description": "Torso circumference at men's natural side Abdominal Obliques indentation, if Oblique indentation isn't found then just below the Navel level.",
        "diagram": "Op3",
        "full_name": "Natural Waist circumference",
        "number": "O03"
    },
    "waist_to_highhip_b": {
        "description": "From Waist Back down to Highhip Back.",
        "diagram": "Hp10",
        "full_name": "Waist Back to Highhip Back",
        "number": "H33"
    },
    "waist_to_highhip_f": {
        "description": "Размерный признак в стандарте №78. Измеряют по горизонтали расстояние от вертикальной плоскости, касательной к выступающим точкам лопаток, до линейки, приложенной горизонтально к продольным мышцам спины на уровне линии талии.",
        "diagram": "Hp9",
        "full_name": "Глубина талии первая",
        "number": "H30"
    },
    "waist_to_highhip_side": {
        "description": "From Waist Side to Highhip Side.",
        "diagram": "Hp9",
        "full_name": "Waist Side to Highhip Side",
        "number": "H32"
    },
    "waist_to_hip_b": {
        "description": "From Waist Back down to Hip Back. Requires tape to cover the gap between buttocks.",
        "diagram": "Hp10",
        "full_name": "От талии до линии бедра сзади",
        "number": "H34"
    },
    "waist_to_hip_f": {
        "description": "Размерный признак в стандарте №79. Измеряют по горизонтали расстояние от вертикальной плоскости, касательной к ягодичным точкам, до линейки, приложенной горизонтально к продольным мышцам спины на уровне линии талии.",
        "diagram": "Hp9",
        "full_name": "Глубина талии вторая",
        "number": "H31"
    },
    "waist_to_hip_side": {
        "description": "От талии сбоку до бедра сбоку.",
        "diagram": "Hp10",
        "full_name": "От талии сбоку до бедра сбоку",
        "number": "H35"
    },
    "waist_to_natural_waist_b": {
        "description": "Length from Waist Back to Natural Waist Back.",
        "diagram": "Op7",
        "full_name": "Waist Back to Natural Waist Back",
        "number": "O07"
    },
    "waist_to_natural_waist_f": {
        "description": "Length from Waist Front to Natural Waist Front.",
        "diagram": "Op6",
        "full_name": "Waist Front to Natural Waist Front",
        "number": "O06"
    },
    "waist_to_waist_halter": {
        "description": "Номер размерного признака в стандарте №44. Измеряют сзади параллельно позвоночнику от линии талии к точке основания шеи сбоку, касаясь этой точки внутренним краем ленты. Спереди лента должна проходить через сосковую точку, далее вниз до линии талии.",
        "diagram": "Op2",
        "full_name": "Длина дуги верхней части туловища через точку основания шеи сбоку",
        "number": "O02"
    },
    "width_abdomen_to_hip": {
        "description": "Horizontal distance from the greatest abdomen prominence to the greatest hip prominence.",
        "diagram": "Bp2",
        "full_name": "Width: Abdomen to Hip",
        "number": "B05"
    },
    "width_bust": {
        "description": "Horizontal distance from Bust Side to Bust Side.",
        "diagram": "Bp1",
        "full_name": "Width: Bust",
        "number": "B02"
    },
    "width_hip": {
        "description": "Horizontal distance from Hip Side to Hip Side.",
        "diagram": "Bp1",
        "full_name": "Width: Hip",
        "number": "B04"
    },
    "width_shoulder": {
        "description": "Horizontal distance from Shoulder Tip to Shoulder Tip.",
        "diagram": "Bp1",
        "full_name": "Width: Shoulder",
        "number": "B01"
    },
    "width_waist": {
        "description": "Horizontal distance from Waist Side to Waist Side.",
        "diagram": "Bp1",
        "full_name": "Width: Waist",
        "number": "B03"
    }
}
